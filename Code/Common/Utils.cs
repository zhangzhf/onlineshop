﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;
using System.Web;

namespace Common
{
    public class Utils
    {

        public static string ApplicationBase()
        {
            String strBase = HttpRuntime.AppDomainAppVirtualPath;
            strBase = strBase.EndsWith("/") ? strBase.Substring(0, strBase.Length - 1) : strBase;
            return strBase;
        }

        /// <summary>
        /// 新版ResolveURL
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        public static string ResolveUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' ||
                relativeUrl[0] == '\\') return relativeUrl;

            int idxOfScheme =
              relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        /// <summary>
        /// 输出页面内容，避免页面闪动,刷新父页面
        /// </summary>
        public static void ResponseCloseDialogScript()
        {
            HttpContext.Current.Response.Write("<script>parent.location.href=parent.location.href; </script>");
            HttpContext.Current.Response.End();
        }

        /// <summary>
        /// 刷新父窗口
        /// </summary>
        public static void ResponseParentReloadScript()
        {
            HttpContext.Current.Response.Write("<script>parent.Reload();</script>");
            HttpContext.Current.Response.End();
        }

        public static void ResponseParentCallback(String callback, String result)
        {
            HttpContext.Current.Response.Write(String.Format("<script>parent.CloseDialog(); parent.{0}({1});</script>", callback, result));
            HttpContext.Current.Response.End();
        }

        public static String GetBrandname()
        {
            return System.Configuration.ConfigurationManager.AppSettings["Brandname"];
        }
        public static String GetCopyright()
        {
            return System.Configuration.ConfigurationManager.AppSettings["Copyright"];
        }
        public static String GetMallname()
        {
            return System.Configuration.ConfigurationManager.AppSettings["MallName"];
        }
        /// <summary>
        /// 将字节数转换为可读字符串
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string ConvertByteToUnit(long size)
        {
            String strSize = String.Empty;

            Double dblSize = Convert.ToDouble(size);

            if (dblSize < 1024)
            {
                strSize = dblSize.ToString() + " B";
            }
            else if (dblSize < 1024D * 1024D)
            {
                strSize = Math.Round(dblSize / 1024D, 2).ToString() + " KB";
            }
            else if (dblSize < 1024D * 1024D * 1024D)
            {
                strSize = Math.Round(dblSize / 1024D / 1024D, 2).ToString() + " MB";
            }
            else
            {
                strSize = Math.Round(dblSize / 1024D / 1024D / 1024D, 2).ToString() + " GB";
            }

            return strSize;
        }

        /// <summary>
        /// 获取当前登陆用户信息
        /// </summary>
        /// <returns></returns>
        public static model.member.UserInfo LoginUser
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated || !String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
                {
                    String userId = HttpContext.Current.User.Identity.Name;

                    if (HttpContext.Current.Session["UserInfo_" + userId] != null)
                    {
                        return (model.member.UserInfo)HttpContext.Current.Session["UserInfo_" + userId];
                    }
                    else
                    {
                        model.member.UserInfo u = OR.DAL.GetModelByKey<model.member.UserInfo>(Int32.Parse(HttpContext.Current.User.Identity.Name));
                        HttpContext.Current.Session["UserInfo_" + u.UserId] = u;
                        return u;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 当前用户是否以登录
        /// </summary>
        public static bool IsAuthenticated
        {
            get
            {
                return HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }

        #region MD5

        private static System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

        /// <summary>
        /// 转换到MD5
        /// </summary>
        /// <param name="str">加密前字符串</param>
        /// <returns>加密后字符串</returns>
        public static string to_md5(string str)
        {
            byte[] bIn = Encoding.ASCII.GetBytes(str);

            byte[] bOut = md5.ComputeHash(bIn);

            String bResult = Convert.ToBase64String(bOut);

            return bResult;
        }

        #endregion


        /// <summary>
        /// 进行实体类的序列化操作，主要用户是记录当前登陆用户信息
        /// </summary>
        public class SerializeObj
        {
            public SerializeObj()
            { }

            /// <summary>
            /// 序列化 对象到字符串
            /// </summary>
            /// <param name="obj">泛型对象</param>
            /// <returns>序列化后的字符串</returns>
            public static string Serialize<T>(T obj)
            {
                try
                {
                    IFormatter formatter = new BinaryFormatter();
                    MemoryStream stream = new MemoryStream();
                    formatter.Serialize(stream, obj);
                    stream.Position = 0;
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    stream.Flush();
                    stream.Close();
                    return Convert.ToBase64String(buffer);
                }
                catch (Exception ex)
                {
                    throw new Exception("序列化失败,原因:" + ex.Message);
                }
            }

            /// <summary>
            /// 反序列化 字符串到对象
            /// </summary>
            /// <param name="obj">泛型对象</param>
            /// <param name="str">要转换为对象的字符串</param>
            /// <returns>反序列化出来的对象</returns>
            public static T Desrialize<T>(string str) where T : OR.Model.Entity, new()
            {
                T obj = new T();
                try
                {
                    IFormatter formatter = new BinaryFormatter();
                    byte[] buffer = Convert.FromBase64String(str);
                    MemoryStream stream = new MemoryStream(buffer);
                    obj = (T)formatter.Deserialize(stream);
                    stream.Flush();
                    stream.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("反序列化失败,原因:" + ex.Message);
                }
                return obj;
            }
        }
    }
}
