﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Common
{
    /// <summary>
    ///MenuUtils 的摘要说明
    /// </summary>
    public class MenuUtils
    {
        private static XmlDocument _doc = null;

        public static XmlDocument GetMenuDocument()
        {
            if (!Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings["UseMenuCache"]) || _doc == null)
            {
                _doc = new XmlDocument();
                _doc.Load(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Menus.xml"));
            }

            return _doc;
        }
    }
}