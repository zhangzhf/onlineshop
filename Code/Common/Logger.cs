﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Common
{

    public enum LogLevel
    {
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5
    }

    public class Logger
    {
        private static LogLevel _logLevel = LogLevel.Info;

        private static bool hasLoad = false;

        /// <summary>
        /// 记录当前登录用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        public static void debug(String logContent)
        {
            debug(logContent, Common.Utils.LoginUser);
        }

        /// <summary>
        /// 记录指定用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        /// <param name="user"></param>
        public static void debug(String logContent, model.member.UserInfo user)
        {
            if (GetLogLevel() >= LogLevel.Debug)
            {
                SaveLog(LogLevel.Debug, logContent, user);
            }
        }

        /// <summary>
        /// 记录当前登录用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        public static void info(String logContent)
        {
            info(logContent, Common.Utils.LoginUser);
        }

        /// <summary>
        /// 记录指定用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        /// <param name="user"></param>
        public static void info(String logContent, model.member.UserInfo user)
        {
            if (GetLogLevel() >= LogLevel.Info)
            {
                SaveLog(LogLevel.Info, logContent, user);
            }
        }

        /// <summary>
        /// 记录当前登录用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        public static void warn(String logContent)
        {
            warn(logContent, Common.Utils.LoginUser);
        }

        /// <summary>
        /// 记录指定用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        /// <param name="user"></param>
        public static void warn(String logContent, model.member.UserInfo user)
        {
            if (GetLogLevel() >= LogLevel.Warn)
            {
                SaveLog(LogLevel.Warn, logContent, user);
            }
        }

        /// <summary>
        /// 记录当前登录用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        public static void error(String logContent)
        {
            error(logContent, Common.Utils.LoginUser);
        }

        /// <summary>
        /// 记录指定用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        /// <param name="user"></param>
        public static void error(String logContent, model.member.UserInfo user)
        {
            if (GetLogLevel() >= LogLevel.Error)
            {
                SaveLog(LogLevel.Error, logContent, user);
            }
        }

        /// <summary>
        /// 记录当前登录用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        public static void fatal(String logContent)
        {
            fatal(logContent, Common.Utils.LoginUser);
        }

        /// <summary>
        /// 记录指定用户活动日志
        /// </summary>
        /// <param name="logContent"></param>
        /// <param name="user"></param>
        public static void fatal(String logContent, model.member.UserInfo user)
        {
            if (GetLogLevel() >= LogLevel.Fatal)
            {
                SaveLog(LogLevel.Fatal, logContent, user);
            }
        }


        /// <summary>
        /// 获取当期配置的日志级别
        /// </summary>
        /// <returns></returns>
        internal static LogLevel GetLogLevel()
        {
            if (!hasLoad)
            {
                String strLevel = System.Configuration.ConfigurationManager.AppSettings["LogLevel"];

                if (!String.IsNullOrEmpty(strLevel))
                {
                    int i = 0;
                    if (Int32.TryParse(strLevel, out i))
                    {
                        if (i >= 1 && i <= 5)
                        {
                            _logLevel = (LogLevel)i;
                        }
                    }
                }
                else
                {
                    _logLevel = LogLevel.Info;
                }

                hasLoad = true;
            }
            return _logLevel;
        }

        /// <summary>
        /// 将日志存入数据库
        /// </summary>
        /// <param name="level"></param>
        /// <param name="logContent"></param>
        /// <param name="user"></param>
        internal static void SaveLog(LogLevel level, String logContent, model.member.UserInfo user)
        {
            model.log.SysLog log = new model.log.SysLog();
            log.Created = DateTime.Now;
            log.LogContent = logContent;
            log.LogLevel = (int)level;
            log.UserId = user.UserId;
            log.UserName = user.RealName;
            log.RemoteIP = HttpContext.Current.Request.ServerVariables["Remote_Addr"];

            OR.DAL.Add<model.log.SysLog>(log, false);
        }
    }
}
