﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopHeader.ascx.cs" Inherits="Common_TopHeader" %>

<div class="header">
    <div class="w">
        <div class="fav">
            <b></b>
            <a href="###" rel="nofollow">收藏<%=Common.Utils.GetMallname() %></a>
        </div>
        <div class="user">
            <ul class="welcome">

                <li class="for1">

                    <% if (Common.Utils.IsAuthenticated)
                       { %>

                    <%= Common.Utils.LoginUser.NickName %>, 欢迎来到<%=Common.Utils.GetMallname() %>

                    <a href="<%= Common.Utils.ResolveUrl("~/login.aspx") %>">[退出]</a>

                    <%}
                       else
                       { %>
                   欢迎来到<%=Common.Utils.GetMallname() %>, <a href="<%= Common.Utils.ResolveUrl("~/login.aspx") %>">[登录]</a>
                    <%} %>
                </li>
                <li class="for2">|</li>
                <li class="for1">
                    <a href="<%= Common.Utils.ResolveUrl("~/space/Default.aspx") %>">我的订单</a>
                </li>
            </ul>
        </div>
    </div>
</div>