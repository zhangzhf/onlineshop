﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageFooter.ascx.cs" Inherits="Common_PageFooter" %>
<div class="footer">
    <p>
        
        <asp:HyperLink ID="hyAbout" runat="server" NavigateUrl="~/info/About.aspx">关于我们</asp:HyperLink>
        &nbsp;|&nbsp;
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/info/Contact.aspx">联系我们</asp:HyperLink>
        &nbsp;|&nbsp;
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/info/HR.aspx">人才招聘</asp:HyperLink>
        &nbsp;|&nbsp;商家入驻&nbsp;|&nbsp;广告服务&nbsp;|&nbsp;友情链接
    </p>
    <p>
        Copyright © 2014  北京礼尚合一科技有限公司 版权所有
    </p>
    <p>
        京ICP备14028575号 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;京公网安备11010802015242
    </p>
</div>
