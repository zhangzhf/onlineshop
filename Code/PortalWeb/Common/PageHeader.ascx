﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageHeader.ascx.cs" Inherits="Common_PageHeader" %>
<%@ Register Src="~/Common/TopHeader.ascx" TagPrefix="uc1" TagName="TopHeader" %>

<uc1:TopHeader runat="server" ID="TopHeader" />
<div class="logo">
    <a href="javascript:gotoHome();">
        <div class="logoPic"></div>
    </a>
    <div class="search">
        <input type="text" id="txtKeyword" class="input" /><div id="btnSearch" class="input">搜索</div>
    </div>
    <script type="text/javascript">

        $(document).ready(function ()
        {
            $("#btnMyCart").click(function ()
            {
                window.location.href = globalSetting.base + "/space/PreCheck.aspx";
            });
        });
        function gotoHome()
        {
            window.location.href = globalSetting.base;
        }
    </script>
    <div class="myCart">
        <div class="flatButton" id="btnMyCart">
            去购物车结算
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/arrow_down.png" />
        </div>
    </div>
    <div class="myAccount">
        <div class="flatButton">
            我的账户
            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/arrow_down.png" />
        </div>
    </div>
</div>

<div class="menu">
    <div class="menuNav">
        <div class="top"><span id="showPopMenu">全部商品分类</span></div>
        <div class="popMenu" id="popNavMenu" style="display: none;">
            <div class="container">
                <div class="sp"></div>
            </div>
        </div>
    </div>
    <ul class="menuTop">
        <li class="selected"><a href="<%= Common.Utils.ResolveUrl("~/index.aspx") %>">首页</a></li>
    </ul>
</div>
