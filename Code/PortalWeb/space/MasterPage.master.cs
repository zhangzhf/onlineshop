﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class space_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.Page.Title = "我的" + Common.Utils.GetMallname();

            LoadMenu();
        }
    }


    #region 对菜单内容进行初始化

    /// <summary>
    /// 初始化菜单树
    /// </summary>
    protected void LoadMenu()
    {
        XmlDocument doc = Common.MenuUtils.GetMenuDocument();

        XmlNodeList menus = doc.SelectNodes("/root/folder");

        String sessionURL = Session["selectedURL"] == null ? "" : Session["selectedURL"].ToString();
        String currUrl = Request.Url.AbsolutePath.ToLower();
        bool hasSelectedMenu = false;

        foreach (XmlNode folder in menus)
        {
            TreeNode nodeFolder = new TreeNode(folder.Attributes["name"].Value, folder.Attributes["id"].Value);
            nodeFolder.SelectAction = TreeNodeSelectAction.Expand;
            nodeFolder.Expanded = true;

            XmlNodeList subMenus = folder.SelectNodes("menu");

            foreach (XmlNode item in subMenus)
            {
                TreeNode nodeMenu = new TreeNode(item.Attributes["name"].Value, item.Attributes["id"].Value);
                nodeMenu.SelectAction = TreeNodeSelectAction.Select;
                nodeMenu.Expanded = false;
                nodeMenu.NavigateUrl = item.Attributes["url"].Value;
                nodeFolder.ChildNodes.Add(nodeMenu);

                // 给菜单加上选中的样式，标识当前哪个是活动菜单
                String currPage = this.Page.ResolveUrl(item.Attributes["url"].Value);

                // 先比较session中的
                if (!hasSelectedMenu && currPage.ToLower().Equals(sessionURL))
                {
                    nodeMenu.Selected = true;
                }

                // 如果找到了合适的，以此为准，并记录下来
                if (!hasSelectedMenu && currPage.ToLower().Equals(currUrl))
                {
                    nodeMenu.Selected = true;
                    hasSelectedMenu = true;
                    Session["selectedURL"] = currUrl;
                }
            }

            if (nodeFolder.ChildNodes.Count > 0)
            {
                this.treeMenu.Nodes.Add(nodeFolder);
            }

            ShowExpendNode(nodeFolder);
        }
    }

    protected void TreeView1_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
        Session["Expand_" + e.Node.Value] = "true";
    }

    protected void TreeView1_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
    {
        Session["Expand_" + e.Node.Value] = "false";
    }

    protected void ShowExpendNode(TreeNode node)
    {
        if (Session["Expand_" + node.Value] != null)
        {
            node.Expanded = (Session["Expand_" + node.Value] == null ? true : Boolean.Parse(Session["Expand_" + node.Value].ToString()));
        }
    }

    #endregion
}
