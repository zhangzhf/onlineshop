﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class space_PreCheck : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<model.view.Carts> items = OR.DAL.GetModelList<model.view.Carts>("UserId=@user And Status=1 Order By CartTime Desc",
                OR.Param.NamedParam("@user", Common.Utils.LoginUser.UserId));

            this.Repeater1.DataSource = items;
            this.Repeater1.DataBind();
        }
    }
}