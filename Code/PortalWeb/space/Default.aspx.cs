﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class space_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<model.order.OrderInfo> orders = OR.DAL.GetModelList<model.order.OrderInfo>("UserId=@id Order By OrderId Desc",
                OR.Param.NamedParam("@id", Common.Utils.LoginUser.UserId));

            if (orders.Count == 0)
            {
                this.panelNoData.Visible = true;
            }
            else
            {
                this.Repeater1.DataSource = orders;
                this.Repeater1.DataBind();
            }
        }
    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            model.order.OrderInfo order = e.Item.DataItem as model.order.OrderInfo;

            Literal ltPictures = e.Item.FindControl("ltPictures") as Literal;

            StringBuilder sb = new StringBuilder();

            List<model.order.OrderDetailItem> items = OR.DAL.GetModelList<model.order.OrderDetailItem>("OrderId=" + order.OrderId.ToString());

            foreach (model.order.OrderDetailItem item in items)
            {
                sb.AppendFormat("<a href='../item.aspx?id={0}'><img src='{1}' /></a> ",
                    item.ItemId,
                   Common.Utils.ResolveUrl(model.res.FileInfoImpl.GetPictureThumb(model.res.FileInfoImpl.GetModelByKey(item.SnapshotPicId), model.res.ThumbLevel.S)));
            }

            ltPictures.Text = sb.ToString();
        }
    }
}