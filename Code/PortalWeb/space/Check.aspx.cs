﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class space_Check : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            List<model.view.Carts> items = OR.DAL.GetModelList<model.view.Carts>("UserId=@user And Status=1 Order By CartTime Desc",
                OR.Param.NamedParam("@user", Common.Utils.LoginUser.UserId));

            this.Repeater1.DataSource = items;
            this.Repeater1.DataBind();

            double price = 0.0;
            int totalCount = 0;
            foreach (model.view.Carts item in items)
            {
                price += item.RetailPrice * item.ItemCnt;
                totalCount += item.ItemCnt;
            }

            this.ltTotalPrice1.Text = price.ToString();
            this.ltTotalPrice2.Text = price.ToString();
            this.ltTotalPrice3.Text = price.ToString();
            this.ltTotalCount.Text = totalCount.ToString();

            List<model.member.AddressInfo> addresses = OR.DAL.GetModelList<model.member.AddressInfo>("UserId=@user Order By AddressId",
                OR.Param.NamedParam("@user", Common.Utils.LoginUser.UserId));

            if (addresses.Count == 0)
            {
                model.member.AddressInfo address = new model.member.AddressInfo()
                {
                    Address = Common.Utils.LoginUser.Address,
                    CityCode = 110000,
                    Created = DateTime.Now,
                    LastUsed = 1,
                    PhoneNum = Common.Utils.LoginUser.Cellphone,
                    Recipient = Common.Utils.LoginUser.RealName,
                    Status = (int)model.Status.Normal,
                    Updated = DateTime.Now,
                    UserId = Common.Utils.LoginUser.UserId,
                    ZIP = Common.Utils.LoginUser.ZIP,
                    Email = Common.Utils.LoginUser.Email
                };

                address = OR.DAL.Add<model.member.AddressInfo>(address, true);
                addresses.Add(address);
            }

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "address", "address_callback(" + Newtonsoft.Json.JsonConvert.SerializeObject(addresses[0]) + ");", true);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        // 记录订单基本信息
        model.order.OrderInfo order = new model.order.OrderInfo();
        order.Created = DateTime.Now;
        order.Memo = "";
        order.Status = (int)model.Status.Normal;
        order.SubmitTime = DateTime.Now;
        order.TotalPrice = 0.0;
        order.Updated = DateTime.Now;
        order.UserId = Common.Utils.LoginUser.UserId;

        order = OR.DAL.Add<model.order.OrderInfo>(order, true);

        // 记录订单商品明细信息
        List<model.view.Carts> items = OR.DAL.GetModelList<model.view.Carts>("UserId=@user And Status=1 Order By CartTime Desc",
                OR.Param.NamedParam("@user", Common.Utils.LoginUser.UserId));

        double p = 0.0;

        foreach (model.view.Carts item in items)
        {
            model.order.OrderDetailItem i = new model.order.OrderDetailItem();
            i.Discount = 0;
            i.ItemCount = item.ItemCnt;
            i.ItemId = item.ItemId;
            i.ItemPrice = item.MarketPrice * item.ItemCnt;
            i.ItemTitle = item.ItemName;
            i.OrderId = order.OrderId;
            i.SnapshotPicId = item.SnapshotPicId;
            i.UnitPrice = item.MarketPrice;

            OR.DAL.Add<model.order.OrderDetailItem>(i, false);

            p += i.ItemPrice;
        }


        // 记录订单价格信息
        order.TotalPrice = p;
        OR.DAL.Update<model.order.OrderInfo>(order);

        // 记录订单付款信息
        model.order.Payment payment = new model.order.Payment();
        payment.Created = DateTime.Now;
        payment.Memo = "";
        payment.OrderId = order.OrderId;
        payment.PaymentMethod = (int)model.order.PaymentMethod.支票;
        payment.PaymentNO = "";
        payment.PaymentStatus = (int)model.order.PaymentStatus.等待付款;
        payment.Status = (int)model.Status.Normal;
        payment.Updated = DateTime.Now;

        OR.DAL.Add<model.order.Payment>(payment, false);

        // 记录发票信息
        model.order.Invoice invoice = new model.order.Invoice();
        invoice.InvoiceDetail = (int)model.order.InvoiceDetail.明细;
        invoice.InvoiceStatus = (int)model.Status.Normal;
        invoice.InvoiceTime = DateTime.Now;
        invoice.InvoiceTitleContent = model.member.OrgInfoImpl.GetModelByKey(Common.Utils.LoginUser.OrgId).OrgName;
        invoice.InvoiceTitleType = (int)model.order.InvoiceTitleType.单位;
        invoice.InvoiceType = (int)model.order.InvoiceType.普通发票;
        invoice.OrderId = order.OrderId;

        OR.DAL.Add<model.order.Invoice>(invoice, false);

        // 记录订单收件人信息
        model.member.AddressInfo add1 = OR.DAL.GetModelByKey<model.member.AddressInfo>(Convert.ToInt32(this.txtAddressId.Text));

        model.order.AddressInfo address = new model.order.AddressInfo();
        address.Address = add1.Address;
        address.CityName = model.dict.CityCodeImpl.GetModelByKey(add1.CityCode).CityName;
        address.Email = add1.Email;
        address.OrderId = order.OrderId;
        address.PhoneNum = add1.PhoneNum;
        address.Recipient = add1.Recipient;
        OR.DAL.Add<model.order.AddressInfo>(address, false);

        // 下单后清理购物车里的东西
        String sql = "Delete From Order_Cart Where UserId=@user";
        OR.SQLHelper.ExecuteSql(sql, OR.Param.NamedParam("@user", Common.Utils.LoginUser.UserId));

        // 跳转到确认页面
        Response.Redirect("./CheckOrder.aspx?id=" + order.OrderId);
    }
}