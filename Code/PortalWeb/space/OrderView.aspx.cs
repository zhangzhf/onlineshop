﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class space_OrderView : System.Web.UI.Page
{
    protected String orderId = String.Empty;
    protected model.order.OrderInfo order = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        orderId = Request.QueryString["id"];

        if (!IsPostBack)
        {
            order = OR.DAL.GetModelByKey<model.order.OrderInfo>(Convert.ToInt32(orderId));

            List<model.order.OrderDetailItem> items = OR.DAL.GetModelList<model.order.OrderDetailItem>("OrderId=@id",
                OR.Param.NamedParam("@id", order.OrderId));

            this.Repeater1.DataSource = items;
            this.Repeater1.DataBind();

            double price = 0.0;
            int totalCount = 0;

            foreach (model.order.OrderDetailItem item in items)
            {
                price += item.ItemPrice;
                totalCount += item.ItemCount;
            }

            this.ltTotalPrice1.Text = price.ToString();
            this.ltTotalPrice2.Text = price.ToString();
            this.ltTotalPrice3.Text = price.ToString();
            this.ltTotalCount.Text = totalCount.ToString();

            model.order.AddressInfo address = OR.DAL.GetModel<model.order.AddressInfo>("OrderId=@id",
                OR.Param.NamedParam("@id", order.OrderId));

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "address", "address_callback(" + Newtonsoft.Json.JsonConvert.SerializeObject(address) + ");", true);
        }
    }
}