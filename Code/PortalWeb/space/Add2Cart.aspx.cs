﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class space_Add2Cart : System.Web.UI.Page
{
    protected String itemId = String.Empty;
    protected String count = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        itemId = Request.QueryString["itemId"];
        count = Request.QueryString["count"];

        model.order.Cart cart = new model.order.Cart();
        cart.CartTime = DateTime.Now;
        cart.ItemCnt = Convert.ToInt32(count);
        cart.ItemId = Convert.ToInt32(itemId);
        cart.UserId = Common.Utils.LoginUser.UserId;

        OR.DAL.Add<model.order.Cart>(cart, false);

        Response.Redirect("./AddToCartConfirm.aspx");

    }
}