﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Item : System.Web.UI.Page
{
    protected model.goods.ItemInfoFull item = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = String.Format("{0}", Common.Utils.GetMallname());

        String itemId = Request.QueryString["id"];

        if (String.IsNullOrEmpty(itemId))
        {
            return;
        }

        item = OR.DAL.GetModelByKey<model.goods.ItemInfoFull>(Convert.ToInt32(itemId));

        if (item == null)
        {
            return;
        }

        model.goods.Category p = null; // parent
        model.goods.Category s = null; // sub
        s = model.goods.CategoryImpl.GetModelByKey(item.CategoryId);
        p = model.goods.CategoryImpl.GetModelByKey(s.ParentId);

        this.ltParent.Text = p.CategoryName;
        this.ltBoard.Text = s.CategoryName;
        this.ltCurrent.Text = item.ItemName;

        this.ltBrandName.Text = item.BrandName;
        this.ltDisplayTitle.Text = item.DisplayTitle;
        this.ltItemId.Text = item.ItemId.ToString();
        this.ltItemModel.Text = item.ItemModel;
        this.ltItemName.Text = item.ItemName;
        this.ltItemNo2.Text = item.ItemId.ToString();
        this.ltMarketPrice.Text = item.MarketPrice.ToString();
        this.ltRetailPrice.Text = item.RetailPrice.ToString();
        this.ltSubTitle.Text = item.SubTitle;
        this.ltContent.Text = item.Description;

        BuildCategoryInfoTable();

        LoadItemPic();
    }

    protected void BuildCategoryInfoTable()
    {
        List<model.goods.InfoKeys> infoKeys = model.goods.InfoKeysImpl.RetrieveCategoryInfoKeys(item.CategoryId, true);

        foreach (model.goods.InfoKeys key in infoKeys)
        {
            AddTabInfoAttr(key);
        }
    }

    protected void AddTabInfoAttr(model.goods.InfoKeys infoKey)
    {
        model.goods.InfoTypeCode t = (model.goods.InfoTypeCode)Enum.Parse(typeof(model.goods.InfoTypeCode), infoKey.InfoType);

        Dictionary<String, model.goods.ItemKeyValue> values = model.goods.ItemKeyValueImpl.RetrieveItemKeyValue(item.ItemId);

        Table table = GetInfoTable(t);

        if (table != null)
        {
            TableCell cell1 = new TableCell();
            cell1.Text = infoKey.KeyName;

            TableCell cell2 = new TableCell();

            Label label = new Label();
            label.ID = String.Format("lbl_{0}_{1}", infoKey.InfoType, infoKey.InfoKey);

            if (values.ContainsKey(String.Format("{0}_{1}", infoKey.InfoType, infoKey.InfoKey)))
            {
                model.goods.ItemKeyValue value = values[String.Format("{0}_{1}", infoKey.InfoType, infoKey.InfoKey)];

                label.Text = value.InfoValue;
            }

            cell2.Controls.Add(label);

            TableRow row = new TableRow();
            row.Cells.Add(cell1);
            row.Cells.Add(cell2);

            table.Rows.Add(row);
        }
    }

    protected Table GetInfoTable(model.goods.InfoTypeCode code)
    {
        switch (code)
        {
            case model.goods.InfoTypeCode.BasicInfo:
                return this.ItemBasicInfo;
            case model.goods.InfoTypeCode.SpecInfo:
                return this.ItemSpecInfo;
            case model.goods.InfoTypeCode.Package:
                return this.ItemPackage;
            case model.goods.InfoTypeCode.Services:
                return this.ItemServices;
            default:
                return null;
        }
    }

    protected void LoadItemPic()
    {
        List<model.res.FileInfo> pics = model.res.FileInfoImpl.GetBizResFiles(model.res.BizModule.Goods_Photo, item.ItemId.ToString());

        this.Repeater1.DataSource = pics;
        this.Repeater1.DataBind();

        this.Repeater2.DataSource = pics;
        this.Repeater2.DataBind();
    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            model.res.FileInfo pic = e.Item.DataItem as model.res.FileInfo;

            Image img = e.Item.FindControl("Image1") as Image;

            img.ImageUrl = model.res.FileInfoImpl.GetPictureThumb(pic, model.res.ThumbLevel.M);
        }
    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            model.res.FileInfo pic = e.Item.DataItem as model.res.FileInfo;

            Image img = e.Item.FindControl("Image1") as Image;

            img.ImageUrl = model.res.FileInfoImpl.GetPictureThumb(pic, model.res.ThumbLevel.S);
        }
    }
}