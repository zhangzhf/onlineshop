﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<%@ Register Src="Common/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="Common/PageFooter.ascx" TagName="PageFooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="css/layout.css" rel="stylesheet" />
    <link href="css/scrollpic.css" rel="stylesheet" />

    <script type="text/javascript">
        var globalSetting = {
            base: '<%=Common.Utils.ApplicationBase() %>'
        };
    </script>
    <script src="js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="js/Common.js"></script>
    <script src="js/scrollpic.js"></script>

    <script type="text/javascript">

        $(document).ready(function ()
        {
            InitPopMenu(true);
        });

    </script>
</head>
<body>

    <uc1:PageHeader ID="PageHeader1" runat="server" />

    <div class="banner">
        <div id="focus">
            <ul>
                <asp:Repeater ID="rpBanner" runat="server">
                    <ItemTemplate>
                        <li>
                            <a href="###">
                                <img src='<%# model.res.FileInfoImpl.GetResolvedFileURL(Convert.ToInt32(Eval("FileId").ToString())) %>' alt="" />
                            </a>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>

    <div id="productsList" class="productsList">
        <ul class="floor">
        </ul>
    </div>

    <uc2:PageFooter ID="PageFooter1" runat="server" />

</body>
</html>
