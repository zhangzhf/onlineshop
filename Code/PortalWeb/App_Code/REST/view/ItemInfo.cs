﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REST.view
{
    /// <summary>
    /// ItemInfo 的摘要说明
    /// </summary>
    public class ItemInfo
    {
        public int ItemId { get; set; }
        public int CategoryId { get; set; }
        public String DisplayTitle { get; set; }
        public String SubTitle { get; set; }
        public double RetailPrice { get; set; }
        public double GroupOnPrice { get; set; }

        public String url { get; set; }
    }
}