﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REST.view
{
    /// <summary>
    /// MenuItem 的摘要说明
    /// </summary>
    public class MenuItem
    {
        public int MenuId { get; set; }
        public String MenuName { get; set; }
        public String URL { get; set; }

        public List<MenuItem> subMenu;
    }
}