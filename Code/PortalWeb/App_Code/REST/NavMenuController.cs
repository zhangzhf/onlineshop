﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// NavMenuController 的摘要说明
/// </summary>
public class NavMenuController : ApiController
{

    #region GetMenuItem 获取菜单列表

    [HttpGet]
    [ActionName("GetMenuItem")]
    public List<REST.view.MenuItem> GetMenuItem()
    {
        List<REST.view.MenuItem> result = new List<REST.view.MenuItem>();

        System.Data.DataTable dt = model.goods.CategoryImpl.GetMenuItem();

        System.Data.DataRow[] rows = dt.Select("ParentId=0");

        for (int i = 0; i < rows.Length; i++)
        {
            REST.view.MenuItem item = new REST.view.MenuItem() { MenuId = Convert.ToInt32(rows[i]["CategoryId"].ToString()), MenuName = rows[i]["CategoryName"].ToString(), URL = "" };

            result.Add(item);

            BuildSubItem(item, Convert.ToInt32(rows[i]["CategoryId"].ToString()), dt);
        }

        return result;
    }

    protected void BuildSubItem(REST.view.MenuItem result, int parentId, System.Data.DataTable source)
    {
        System.Data.DataRow[] rows = source.Select("ParentId=" + parentId.ToString());

        for (int i = 0; i < rows.Length; i++)
        {
            REST.view.MenuItem item = new REST.view.MenuItem() { MenuId = Convert.ToInt32(rows[i]["CategoryId"].ToString()), MenuName = rows[i]["CategoryName"].ToString(), URL = "" };

            if (result.subMenu == null)
            {
                result.subMenu = new List<REST.view.MenuItem>();
            }

            result.subMenu.Add(item);

            BuildSubItem(item, Convert.ToInt32(rows[i]["CategoryId"].ToString()), source);
        }

    }

    #endregion

    #region GetProductItems 获取指定分类下产品列表

    [HttpGet]
    [ActionName("GetProductItems")]
    public List<REST.view.ItemInfo> GetProductItems([FromUri]String ids, [FromUri] String Top)
    {
        Int32 n = 0;

        /* 为避免攻击，将所有输入的字符串重新整理一下，只保留合法的内容 */
        String[] id = ids.Split(',');

        String newIds = String.Empty;

        for (int i = 0; i < id.Length; i++)
        {
            if (!String.IsNullOrEmpty(id[0]) && Int32.TryParse(id[0], out n))
            {
                newIds += id[i] + ",";
            }
        }

        newIds = newIds.Trim(new char[] { ' ', ',' });

        Int32 nTop = 0;
        String strTop = "";

        if (Int32.TryParse(Top, out nTop))
        {
            // 输入的top是个数字,这时最大只能5
            if (nTop > 5)
            {
                nTop = 5;
            }
            else if (nTop < 1)
            {
                nTop = 1;
            }

            strTop = nTop.ToString();
        }
        else
        {
            Top = Top.Trim(new char[] { ' ' });

            if (Top.EndsWith("%"))
            {
                Top = Top.Trim(new char[] { '%' });

                if (Int32.TryParse(Top, out n) && n <= 100 && n > 0)
                {
                    strTop = Top + " percent";
                }
            }
        }

        if (!String.IsNullOrEmpty(newIds) && !String.IsNullOrEmpty(strTop))
        {
            return GetProducts(newIds, strTop);
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// 该方法是处理过的输入参数，不需要再次验证
    /// </summary>
    /// <param name="ids"></param>
    /// <param name="top"></param>
    protected List<REST.view.ItemInfo> GetProducts(String ids, String top)
    {
        List<REST.view.ItemInfo> result = new List<REST.view.ItemInfo>();

        String sql = "select top " + top + " * From [dbo].[Goods_ItemInfo] where CategoryId in (" + ids + ") And status=1";

        List<model.goods.ItemInfo> items = OR.DAL.DataTableToModel<model.goods.ItemInfo>(OR.SQLHelper.Query(sql).Tables[0]);

        foreach (model.goods.ItemInfo item in items)
        {
            REST.view.ItemInfo i = new REST.view.ItemInfo()
            {
                CategoryId = item.CategoryId,
                DisplayTitle = item.DisplayTitle,
                GroupOnPrice = item.GroupOnPrice,
                ItemId = item.ItemId,
                RetailPrice = item.RetailPrice,
                SubTitle = item.SubTitle,
                url = Common.Utils.ResolveUrl(model.res.FileInfoImpl.GetFileUrlById(item.SnapshotPicId, model.res.ThumbLevel.M))
            };

            result.Add(i);
        }

        return result;
    }

    #endregion
}