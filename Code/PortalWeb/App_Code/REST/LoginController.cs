﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace REST.controller
{
    /// <summary>
    /// HelloController 的摘要说明
    /// </summary>
    public class HelloController : ApiController
    {
        [HttpGet]
        [ActionName("Sayto")]
        public REST.view.Hello GetCity([FromUri]String name)
        {
            REST.view.Hello hello = new REST.view.Hello() { Name = name, Time = DateTime.Now };

            return hello;
        }
    }
}