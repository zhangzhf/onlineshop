﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Board : System.Web.UI.Page
{
    protected String strCategoryId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = String.Format("{0}", Common.Utils.GetMallname());

        strCategoryId = Request.QueryString["id"];

        model.goods.Category p = null; // parent
        model.goods.Category s = null; // sub, current

        model.goods.Category t = model.goods.CategoryImpl.GetModelByKey(Convert.ToInt32(strCategoryId));

        if (t.ParentId == 0)
        {
            // 当前是一级目录
            p = t;
        }
        else
        {
            //当前是二级目录
            p = model.goods.CategoryImpl.GetModelByKey(t.ParentId);
            s = t;
        }

        List<model.goods.Category> categories = OR.DAL.GetModelList<model.goods.Category>("ParentId=@id And Status=@status Order By OrderId",
            OR.Param.NamedParam("@id", p.CategoryId), OR.Param.NamedParam("@status", (int)model.Status.Normal));

        this.Repeater2.DataSource = categories;
        this.Repeater2.DataBind();

        if (categories.Count > 0 && t.ParentId == 0)  // 当前选择的是某父节点
        {
            s = categories[0];
        }
        if (p != null)
        {
            this.ltParent.Text = p.CategoryName;
        }
        if (s != null)
        {
            this.ltCurrent.Text = s.CategoryName;
        }

        List<model.goods.ItemInfo> items = OR.DAL.GetModelList<model.goods.ItemInfo>("CategoryId=@id And Status=@status",
            OR.Param.NamedParam("@id", s.CategoryId), OR.Param.NamedParam("@status", (int)model.Status.Normal));

        this.Repeater1.DataSource = items;
        this.Repeater1.DataBind();
    }
}