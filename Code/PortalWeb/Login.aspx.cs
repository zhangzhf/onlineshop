﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = String.Format("登录{0}", Common.Utils.GetMallname());

        if (!IsPostBack)
        {
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
        }

    }

    protected void loginsubmit_Click(object sender, EventArgs e)
    {
        String userAccount = this.nloginname.Text;
        String userPassword = this.nloginpwd.Text;

        model.member.UserInfo user = OR.DAL.GetModel<model.member.UserInfo>("UserAccount=@UserAccount AND Status=@status",
            new System.Data.SqlClient.SqlParameter("@UserAccount", userAccount),
            new System.Data.SqlClient.SqlParameter("@status", model.Status.Normal));

        if (user != null)
        {
            String strPassword = Common.Utils.to_md5(userPassword);

            if (strPassword.Equals(user.Password))
            {
                Common.Logger.info("用户登录", user);

                System.Web.Security.FormsAuthentication.RedirectFromLoginPage(user.UserId.ToString(), false);

                return;
            }
        }

        return;
    }
}