﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sysadmin_AddressSelect : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<model.member.AddressInfo> addresses = OR.DAL.GetModelList<model.member.AddressInfo>("UserId=@user Order By AddressId",
            OR.Param.NamedParam("@user", Common.Utils.LoginUser.UserId));

        if (addresses.Count == 0)
        {
            model.member.AddressInfo address = new model.member.AddressInfo()
            {
                Address = Common.Utils.LoginUser.Address,
                CityCode = 110000,
                Created = DateTime.Now,
                LastUsed = 1,
                PhoneNum = Common.Utils.LoginUser.Cellphone,
                Recipient = Common.Utils.LoginUser.RealName,
                Status = (int)model.Status.Normal,
                Updated = DateTime.Now,
                UserId = Common.Utils.LoginUser.UserId,
                ZIP = Common.Utils.LoginUser.ZIP,
                Email = Common.Utils.LoginUser.Email
            };

            address = OR.DAL.Add<model.member.AddressInfo>(address, true);
            addresses.Add(address);
        }

        this.GridView1.DataSource = addresses;
        this.GridView1.DataBind();
    }
}