﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sysadmin_AddressEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        model.member.AddressInfo address = new model.member.AddressInfo()
        {
            Address = this.txtAddress.Text,
            CityCode = Convert.ToInt32(this.ddpCity.SelectedValue),
            Created = DateTime.Now,
            LastUsed = 0,
            PhoneNum = this.txtCellPhone.Text,
            Recipient = this.txtRecipent.Text,
            Status = (int)model.Status.Normal,
            Updated = DateTime.Now,
            UserId = Common.Utils.LoginUser.UserId,
            Email = this.txtEmail.Text,
        };

        address = OR.DAL.Add<model.member.AddressInfo>(address, true);

        Response.Redirect("./AddressSelect.aspx");
    }
}