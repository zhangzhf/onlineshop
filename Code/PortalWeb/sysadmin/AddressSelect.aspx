﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddressSelect.aspx.cs" Inherits="sysadmin_AddressSelect" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>选择地址</title>
    <link href="../css/layout.css" rel="stylesheet" />
    <style type="text/css">
        html {
            overflow: auto;
        }

        .form-btn {
            width: 100%;
            text-align: right;
            height: 40px;
            padding-top: 20px;
        }

        #GridView1 {
            line-height: 180%;
        }
    </style>
    <script src="../js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function ()
        {
            $("input[name='address']:eq(0)").attr("checked", 'checked');
        });

        function selectAddress()
        {
            var id = $("input[name='address']:checked").attr("value");
            parent.address_callback($.parseJSON($("#span_" + id).text()));
            parent.CloseDialog();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false"  ShowHeader="false" BorderWidth="0" CellPadding="5" GridLines="None">
            <Columns>
                <asp:TemplateField ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <input type="radio" name="address" value="<%# Eval("AddressId") %>" />
                        <span style="display: none;" id="span_<%# Eval("AddressId") %>">
                            <%# Newtonsoft.Json.JsonConvert.SerializeObject( Container.DataItem)%>
                        </span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <%# Eval("Recipient") %> &nbsp;<%# Eval("PhoneNum") %>&nbsp;<%# Eval("Email") %>
                        <br />
                        <%# model.dict.CityCodeImpl.GetModelByKey(Convert.ToInt32( Eval("CityCode").ToString())).CityName %>
                        <%# Eval("Address") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle Height="20px" />
        </asp:GridView>

        <div class="form-btn">
            <a href="javascript:selectAddress()" class="btn primary">保存收货人信息</a>
            <a href="./AddressEdit.aspx" class="btn secondary">添加收货人信息</a>
        </div>

    </form>
</body>
</html>
