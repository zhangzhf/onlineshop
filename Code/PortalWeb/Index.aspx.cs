﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Index : System.Web.UI.Page
{
    protected String strBase = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = String.Format("{0}", Common.Utils.GetMallname());

        DataTable dt = OR.SQLHelper.Query("Select * From Portal_Banner Where Status=@status Order By OrderId, Created Desc",
            OR.Param.NamedParam("@status", (int)model.Status.Normal)).Tables[0];

        List<model.portal.Banner> banners = OR.DAL.GetModelList<model.portal.Banner>("Status=@status Order By OrderId", OR.Param.NamedParam("@status", (int)model.Status.Normal));

        this.rpBanner.DataSource = banners;
        this.rpBanner.DataBind();
    }
}