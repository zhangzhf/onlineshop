﻿<%@ Application Language="C#" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        REST.config.WebApiConfig.Register(System.Web.Http.GlobalConfiguration.Configuration);

        System.Web.Http.GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
    }
    
</script>
