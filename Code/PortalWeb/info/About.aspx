﻿<%@ Page Title="" Language="C#" MasterPageFile="~/info/master.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="info_About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        p {
            line-height: 200%;
            font-size: 16px;
            padding: 5px 50px;
        }
    </style>
    <br />
    <h2>公司简介</h2>
    <p>
        北京礼尚合一科技有限公司是基于有机产品的高端商务礼品及员工福利提供商。公司通过不断筛选玉龙混杂的有机产品生产商，遴选出优质的产品供应商，旨在为企业在日常商务活动中提供更好的商务礼品。我们立志于有机事业，通过一点一滴的长期努力，为更多的人提供健康、安全的食品；我们以一种预见性和负责任的态度来推进有机农业，保护当前人类和子孙后代的健康和福利，同时在健康农业、生态文明、公平和谐各个方面做出努力；我们支持更多的人分享健康的生活。
    </p>
    <h2>使命</h2>
    <p>
        万里挑一的优选，专心致志的筹划只希望为您的商业活动锦上添花。
    </p>
    <h2>愿景</h2>
    <p>
        传递健康从有机产品开始，我们愿为第一棒负责。
    </p>

</asp:Content>

