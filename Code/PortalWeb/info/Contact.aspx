﻿<%@ Page Title="" Language="C#" MasterPageFile="~/info/master.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="info_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        p {
            line-height: 200%;
            font-size: 16px;
            padding: 5px 50px;
        }
    </style>
    <br />
    <h2>联系我们</h2>
    <p>
        北京礼尚合一科技有限公司
    </p>
    <p>办公地址：北京市海淀区挂甲屯5号</p>
    <p>　　邮编：100091</p>
    <p>　　电话：(010) 57186577 传真：(010) 57732166</p>
    <p>　　邮箱：service@presentone.cn</p>
    <p>　　网址：http://presentone.cn</p>

</asp:Content>

