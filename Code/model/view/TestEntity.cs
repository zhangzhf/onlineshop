﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.view
{
    public class TestEntity:OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int ID { get; set; }
        public DateTime? Created { get; set; }
        public String UserName { get; set; }
    }
}
