﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.view
{
    [OR.Model.Table("View_Carts")]
    public class Carts : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int CartId { get; set; }
        public int UserId { get; set; }
        public int ItemId { get; set; }
        public DateTime CartTime { get; set; }
        public int ItemCnt { get; set; }

        public int CategoryId { get; set; }
        public String ItemName { get; set; }
        public String DisplayTitle { get; set; }
        public String SubTitle { get; set; }
        public double MarketPrice { get; set; }
        public double RetailPrice { get; set; }
        public int Status { get; set; }
        public int SnapshotPicId { get; set; }

        public String BrandName { get; set; }
        public String ItemModel { get; set; }
    }
}
