﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.portal
{
    [OR.Model.Table("Portal_Banner")]
    public class Banner : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int BannerId { get; set; }
        public int FileId { get; set; }
        public String Comment { get; set; }
        public int OrderId { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
