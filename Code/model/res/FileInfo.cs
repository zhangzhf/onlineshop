﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace model.res
{
    [OR.Model.Table("Res_FileInfo")]
    public class FileInfo : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int FileId { get; set; }
        public String BizModule { get; set; }
        public String BizKey { get; set; }
        public int BizOrder { get; set; }
        public int IsCached { get; set; }
        public String Title { get; set; }
        public String FileName { get; set; }

        public String Folder { get; set; }
        public DateTime UploadTime { get; set; }
        public String FileGUID { get; set; }
        public String FileExt { get; set; }
        public long FileSize { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public String Comment { get; set; }
    }


    /// <summary>
    /// 资源文件类型列表
    /// </summary>
    public enum BizModule
    {
        /// <summary>
        /// 商品介绍图片
        /// </summary>
        Goods_Photo
    }

    public enum ThumbLevel
    {
        /// <summary>
        /// 小一点的图，50*50
        /// </summary>
        S,
        /// <summary>
        /// 大一点的图，350*350
        /// </summary>
        M,
        /// <summary>
        /// 最大的图，1000*1000
        /// </summary>
        B,
        /// <summary>
        /// 原图，或者将其统一为指定的尺寸，比如600*600
        /// </summary>
        O
    }

    /// <summary>
    /// 该类除了支持缓存外，还可通过别的方式获取一些通用的处理接口供重用。
    /// 针对文件资源类型的 FileInfo ，以后放到Redis里，高速缓存起来。
    /// </summary>
    public class FileInfoImpl : OR.Model.CachedEntity<FileInfo>
    {

        /// <summary>
        /// 获取完整的客户端路径
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public static String GetResolvedFileURL(int fileId)
        {
            String url = GetFileURL(fileId);
            return ResolveUrl(url);
        }

        /// <summary>
        /// 获取完整的客户端路径
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static String GetResolvedFileURL(FileInfo file)
        {
            String url = GetFileURL(file);
            return ResolveUrl(url);
        }

        /// <summary>
        /// 获取该文件的完整路径。正式文件的路径
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public static String GetFileURL(int fileId)
        {
            FileInfo file = GetModelByKey(fileId);
            return GetFileURL(file);
        }

        public static List<FileInfo> GetBizResFiles(model.res.BizModule module, String bizKey)
        {
            List<FileInfo> files = OR.DAL.GetModelList<FileInfo>("BizModule=@biz And BizKey=@key Order By BizOrder, FileId",
                OR.Param.NamedParam("@biz", module.ToString()),
                OR.Param.NamedParam("@key", bizKey));

            return files;
        }

        //Dictionary<  >

        /// <summary>
        /// 获取资源文件的第一个，一般用来获取商品的封面图片。首先从缓存里找，其次从数据库里找。
        /// </summary>
        /// <param name="module"></param>
        /// <param name="bizKey"></param>
        /// <returns></returns>
        public static FileInfo GetBizCoverFile(model.res.BizModule module, String bizKey)
        {
            String sql = "Select Top 1 * From Member_Organization Where BizModule=@biz And BizKey=@key Order By BizOrder, FileId";

            System.Data.DataTable t = OR.SQLHelper.Query(sql,
                OR.Param.NamedParam("@biz", module),
                OR.Param.NamedParam("@key", bizKey)).Tables[0];

            if (t == null || t.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                FileInfo f = OR.DAL.DataRowToModel<FileInfo>(t.Rows[0]);
                return f;
            }
        }

        /// <summary>
        /// 获取该文件的完整路径。正式文件的路径
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static String GetFileURL(FileInfo file)
        {
            String url = "~/Upload/Common/" + file.Folder + "/" + file.FileGUID + "." + file.FileExt;
            return url;
        }

        /// <summary>
        /// 获取图片文件的缩略图路径
        /// </summary>
        /// <param name="file"></param>
        /// <param name="level">缩略图的级别，当前只支持 S, M 两个级别</param>
        /// <returns></returns>
        public static String GetPictureThumb(FileInfo file, ThumbLevel level)
        {
            String url = String.Empty;

            if (file == null)
            {
                url = "~/images/blank_" + level.ToString() + ".jpg";
            }
            else
            {
                if (level == ThumbLevel.O)
                {
                    url = GetFileURL(file);
                }
                else
                {
                    url = "~/Upload/Common/" + file.Folder + "/" + file.FileGUID + "_" + level.ToString() + ".jpg";
                }
            }
            return url;
        }

        /// <summary>
        /// 图片不大于指定的Boundary范围的最大尺寸。返回二维数组，0：width，1：height。待完善
        /// </summary>
        /// <param name="file"></param>
        /// <param name="boundWidth"></param>
        /// <param name="boundHeight"></param>
        /// <returns></returns>
        public static Size GetImageBound(FileInfo file, int boundWidth, int boundHeight)
        {
            if (file.Width > boundWidth || file.Height > boundHeight)
            {
                double respectTarget = (double)boundWidth / (double)boundHeight;

                double respectSource = (double)file.Width / (double)file.Height;

                return new Size(boundWidth, boundHeight);
            }
            else
            {
                return new Size(file.Width, file.Height);
            }
        }

        /// <summary>
        /// 新版ResolveURL
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        public static string ResolveUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' ||
                relativeUrl[0] == '\\') return relativeUrl;

            int idxOfScheme =
              relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        /// <summary>
        /// 删除指定编号的资源文件，包括数据库里和磁盘上
        /// </summary>
        /// <param name="fileId"></param>
        public static void Delete(int fileId)
        {
            FileInfo f = OR.DAL.GetModelByKey<FileInfo>(fileId);

            if (f == null)
            {
                return;
            }
            else
            {
                OR.DAL.Delete<FileInfo>(fileId);

                String oFileO = GetPictureThumb(f, ThumbLevel.B);
                DeleteFile(oFileO);

                String oFileM = GetPictureThumb(f, ThumbLevel.M);
                DeleteFile(oFileM);

                String oFileS = GetPictureThumb(f, ThumbLevel.S);
                DeleteFile(oFileS);
            }
        }

        /// <summary>
        /// 此文件是web路径，需要将其转为磁盘路径才能开始删除动作
        /// </summary>
        /// <param name="filePath"></param>
        public static void DeleteFile(String filePath)
        {
            try
            {
                String diskFullPath = System.Web.HttpContext.Current.Server.MapPath(filePath);

                if (System.IO.File.Exists(diskFullPath))
                {
                    System.IO.File.Delete(diskFullPath);
                }
            }
            catch (Exception)
            {

            }
        }

        public static String GetFileUrlById(int fileId, ThumbLevel level)
        {
            if (fileId == 0)
            {
                return "~/images/blank_" + level.ToString() + ".jpg";
            }
            else
            {
                FileInfo f = GetModelByKey(fileId);
                return GetPictureThumb(f, level);
            }
        }
    }
}
