﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.log
{
    [OR.Model.Table("Log_SysLog")]
    public class SysLog : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int LogID { get; set; }
        public DateTime Created { get; set; }
        public int LogLevel { get; set; }
        public int UserId { get; set; }
        public String UserName { get; set; }
        public String LogContent { get; set; }
        public String RemoteIP { get; set; }

    }
}
