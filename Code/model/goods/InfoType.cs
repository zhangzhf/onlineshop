﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace model.goods
{
    [OR.Model.Table("Goods_InfoType")]
    public class InfoType : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Manually)]
        public String TypeCode { get; set; }
        public String TypeName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
    }

    /// <summary>
    /// 商品信息分类
    /// </summary>
    public enum InfoTypeCode
    {
        /// <summary>
        /// 商品介绍
        /// </summary>
        [Description("商品介绍")]
        BasicInfo,
        /// <summary>
        /// 规格参数
        /// </summary>
        [Description("规格参数")]
        SpecInfo,
        /// <summary>
        /// 包装清单
        /// </summary>
        [Description("包装清单")]
        Package,
        /// <summary>
        /// 售后保障
        /// </summary>
        [Description("售后保障")]
        Services
    }
}
