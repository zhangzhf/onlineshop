﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.goods
{
    [OR.Model.Table("Goods_ItemInfo")]
    public class ItemInfo : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int ItemId { get; set; }
        public int CategoryId { get; set; }
        public String ItemName { get; set; }
        public String DisplayTitle { get; set; }
        public String SubTitle { get; set; }
        public double MarketPrice { get; set; }
        public double RetailPrice { get; set; }
        public double GroupOnPrice { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
        public int SnapshotPicId { get; set; }

        public String MFR { get; set; }
        public String BrandName { get; set; }
        public String ItemModel { get; set; }
        public String RMK { get; set; }
    }

    [OR.Model.Table("Goods_ItemInfo")]
    public class ItemInfoFull : ItemInfo
    {
        public String Description { get; set; }
    }
}
