﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.goods
{
    [OR.Model.Table("Goods_ItemKeyValue")]
    public class ItemKeyValue : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int InfoId { get; set; }
        public int ItemId { get; set; }
        public String InfoType { get; set; }
        public String InfoKey { get; set; }
        public String InfoValue { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
    }

    public class ItemKeyValueImpl
    {
        /// <summary>
        /// 根据 ItemId 获取该条目的所有扩展信息内容
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static Dictionary<String, ItemKeyValue> RetrieveItemKeyValue(int itemId)
        {
            List<ItemKeyValue> values = OR.DAL.GetModelList<ItemKeyValue>("ItemId=@id",
                OR.Param.NamedParam("@id", itemId));

            Dictionary<String, ItemKeyValue> d = new Dictionary<string, ItemKeyValue>();

            foreach (ItemKeyValue value in values)
            {
                d.Add(String.Format("{0}_{1}", value.InfoType, value.InfoKey), value);
            }

            return d;
        }
    }
}
