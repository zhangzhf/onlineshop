﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.goods
{
    [OR.Model.Table("Goods_InfoKeys")]
    public class InfoKeys : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Manually)]
        public int CategoryId { get; set; }
        public String InfoType { get; set; }
        public String InfoKey { get; set; }
        public String KeyName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
        public int OrderId { get; set; }
    }

    public class InfoKeysImpl : OR.Model.CachedEntity<InfoKeys>
    {
        /// <summary>
        /// 该方法从基础表里获取该分类下的所有基础信息字典表，然后逐个遍历建立属性信息表
        /// 
        /// 读取方法：首先读取公共的各属性方法，其次读取私有的个分类方法，按InfoType将其组合，统一进行排序，逐个进行显示
        /// 
        /// </summary>
        public static List<model.goods.InfoKeys> RetrieveCategoryInfoKeys(int CategoryId, bool withCommon)
        {
            List<model.goods.InfoKeys> infoKeys = OR.DAL.GetModelList<model.goods.InfoKeys>("CategoryId in (@basic, @id) And Status=@status Order By InfoType, OrderId, Created ",
                OR.Param.NamedParam("@basic", withCommon ? 0 : -1),
                OR.Param.NamedParam("@id", CategoryId),
                OR.Param.NamedParam("@status", (int)model.Status.Normal));

            return infoKeys;
        }
    }
}
