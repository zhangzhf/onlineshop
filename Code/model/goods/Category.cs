﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.goods
{
    [OR.Model.Table("Goods_Category")]
    public class Category : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int CategoryId { get; set; }
        public String CategoryName { get; set; }
        public int ParentId { get; set; }
        public int OrderId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
    }

    public class CategoryImpl : OR.Model.CachedEntity<Category>
    {
        /// <summary>
        /// 返回DataTable类型的目录列表，用于构建界面菜单
        /// </summary>
        /// <returns></returns>
        public static DataTable GetMenuItem()
        {
            DataTable dt = OR.SQLHelper.Query("Select * From Goods_Category where Status<>@status And CategoryId>0 Order By OrderId",
                OR.Param.NamedParam("@status", (int)model.Status.Delete)).Tables[0];

            return dt;
        }
    }
}
