﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace model.goods
{
    [OR.Model.Table("Goods_ItemPriceTrace")]
    public class ItemPriceTrace : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int TraceId { get; set; }
        public int ItemId { get; set; }
        public double MarketPrice { get; set; }
        public double RetailPrice { get; set; }
        public double GroupOnPrice { get; set; }
        public DateTime EffectiveStartTime { get; set; }
        public DateTime EffectiveEndTime { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
    }

    /// <summary>
    /// 价格状态
    /// </summary>
    public enum PriceStatus : int
    {
        /// <summary>
        /// 价格已过期，曾经正式使用过，作为历史记录
        /// </summary>
        [Description("已过期")]
        Expired = 3,
        /// <summary>
        /// 当前正在生效的条目
        /// </summary>
        [Description("已生效")]
        Active = 2,
        /// <summary>
        /// 该条目尚未生效，处于备用状态
        /// </summary>
        [Description("尚未生效")]
        Inactive = 1,
        /// <summary>
        /// 该条目已被删除
        /// </summary>
        [Description("已删除")]
        Delete = 0
    }


    public class ItemPriceTraceImpl
    {

        /// <summary>
        /// 返回价格列表
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="onlyValidPrice">是否返回无效价格列表，在完整历史时需要，画价格曲线时不需要</param>
        /// <returns></returns>
        public static List<ItemPriceTrace> GetItemPriceHistory(int itemId, bool onlyValidPrice)
        {
            List<ItemPriceTrace> result = OR.DAL.GetModelList<ItemPriceTrace>("ItemId=@id And Status>=@status Order By Status, EffectiveStartTime Desc, Created ",
                OR.Param.NamedParam("@id", itemId),
                OR.Param.NamedParam("@status", onlyValidPrice ? (int)PriceStatus.Active : (int)PriceStatus.Inactive));

            return result;
        }
    }
}
