﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.member
{
    [OR.Model.Table("Member_Organization")]
    public class OrgInfo : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int OrgId { get; set; }

        public String OrgName { get; set; }
        public int CityCode { get; set; }
        public String Address { get; set; }
        public String ZIP { get; set; }
        public String Memo { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
    }

    public class OrgInfoImpl : OR.Model.CachedEntity<OrgInfo> { }
}
