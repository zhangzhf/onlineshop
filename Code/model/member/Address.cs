﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.member
{
    [OR.Model.Table("Member_Address")]
    public class AddressInfo : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int AddressId { get; set; }
        public int UserId { get; set; }
        public int CityCode { get; set; }
        public String Address { get; set; }
        public String ZIP { get; set; }
        public String Email { get; set; }
        public String Recipient { get; set; }
        public String PhoneNum { get; set; }
        public int LastUsed { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
    }
}
