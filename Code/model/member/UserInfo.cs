﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.member
{
    [Serializable]
    [OR.Model.Table("Member_UserInfo")]
    public class UserInfo : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int UserId { get; set; }
        public int OrgId { get; set; }

        public String UserAccount { get; set; }
        public String Password { get; set; }

        public String RealName { get; set; }
        public String NickName { get; set; }

        public String Cellphone { get; set; }
        public String LandlinePhone { get; set; }
        public String Fax { get; set; }
        public String Email { get; set; }
        public String QQ { get; set; }
        public String Title { get; set; }
        public String WeChat { get; set; }

        public int CityCode { get; set; }
        public String Address { get; set; }
        public String ZIP { get; set; }
        public String Memo { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
    }

    public class UserInfoImpl : OR.Model.CachedEntity<UserInfo> { }
}
