﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.member
{
    public class Preference : OR.Model.Entity
    {
        public int UserId { get; set; }
        public String PreferKey { get; set; }
        public String PreferValue { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
    }
}
