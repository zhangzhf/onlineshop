﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.dict
{
    [OR.Model.Table("Dict_CityCode")]
    public class CityCode : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Manually)]
        public int Code { get; set; }
        public String CityName { get; set; }
        public int ParentCode { get; set; }
    }

    public class CityCodeImpl : OR.Model.CachedEntity<CityCode> { }
}
