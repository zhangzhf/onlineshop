﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.order
{
    [OR.Model.Table("Order_OrderInfo")]
    public class OrderInfo : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public double TotalPrice { get; set; }
        public DateTime SubmitTime { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
        public String Memo { get; set; }
    }


    /// <summary>
    /// 订单状态，该状态只负责订单状态，不记录付款状态。付款状态由另一个字段记录
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// 已下单
        /// </summary>
        已下单 = 1,
        /// <summary>
        /// 已发货
        /// </summary>
        已发货 = 2,
        /// <summary>
        /// 已确认收货
        /// </summary>
        已确认收货 = 3,
        /// <summary>
        /// 已取消订单
        /// </summary>
        已取消订单 = 4
    }
}
