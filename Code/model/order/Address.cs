﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.order
{
    [OR.Model.Table("Order_Address")]
    public class AddressInfo : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Manually)]
        public int OrderId { get; set; }
        public String CityName { get; set; }
        public String Address { get; set; }
        public String Email { get; set; }
        public String Recipient { get; set; }
        public String PhoneNum { get; set; }
    }
}
