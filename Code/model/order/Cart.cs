﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.order
{
    [OR.Model.Table("Order_Cart")]
    public class Cart: OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int CartId { get; set; }
        public int UserId { get; set; }
        public int ItemId { get; set; }
        public DateTime CartTime { get; set; }
        public int ItemCnt { get; set; }
    }
}
