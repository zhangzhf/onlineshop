﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.order
{
    [OR.Model.Table("Order_OrderDetailItem")]
    public class OrderDetailItem:OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int DetailId { get; set; }
        public int OrderId { get; set; }
        public int ItemId { get; set; }
        public String ItemTitle { get; set; }
        public double UnitPrice { get; set; }
        public int ItemCount { get; set; }
        public double Discount { get; set; }
        public double ItemPrice { get; set; }
        public int SnapshotPicId { get; set; }
    }
}
