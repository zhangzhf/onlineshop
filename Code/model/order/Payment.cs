﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.order
{
    [OR.Model.Table("Order_Payment")]
    public class Payment : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int PaymentId { get; set; }
        public int OrderId { get; set; }
        public int PaymentMethod { get; set; }
        public int PaymentStatus { get; set; }
        public String Memo { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public int Status { get; set; }
        public String PaymentNO { get; set; }
    }

    /// <summary>
    /// 付款方式
    /// </summary>
    public enum PaymentMethod
    {
        /// <summary>
        /// 在线支付
        /// </summary>
        在线支付 = 1,
        /// <summary>
        /// 支票
        /// </summary>
        支票 = 2,
        /// <summary>
        /// 银行转账
        /// </summary>
        银行转账 = 3,
        /// <summary>
        /// 现金
        /// </summary>
        现金 = 4,
        /// <summary>
        /// 
        /// </summary>
        POS刷卡 = 5
    }
    /// <summary>
    /// 订单的付款状态
    /// </summary>
    public enum PaymentStatus
    {
        /// <summary>
        /// 等待付款
        /// </summary>
        等待付款 = 1,
        /// <summary>
        /// 已付款
        /// </summary>
        已付款 = 2,
        /// <summary>
        /// 已退款
        /// </summary>
        已退款 = 3
    }

}
