﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model.order
{
    [OR.Model.Table("Order_Invoice")]
    public class Invoice : OR.Model.Entity
    {
        [OR.Model.ID(OR.Model.GenerationType.Identity)]
        public int InvocieId { get; set; }
        public int OrderId { get; set; }
        public int InvoiceType { get; set; }
        public int InvoiceTitleType { get; set; }
        public String InvoiceTitleContent { get; set; }
        public int InvoiceDetail { get; set; }
        public int InvoiceStatus { get; set; }
        public DateTime InvoiceTime { get; set; }
    }

    /// <summary>
    /// 是否要发票
    /// </summary>
    public enum InvoiceTitleType
    {
        无需发票 = 0,
        单位 = 1,
        个人 = 2
    }

    /// <summary>
    /// 发票类型
    /// </summary>
    public enum InvoiceType
    {
        普通发票 = 1,
        增值税票 = 2
    }

    /// <summary>
    /// 发票内容类型
    /// </summary>
    public enum InvoiceDetail
    {
        明细 = 1,
        办公用品 = 2
    }
}
