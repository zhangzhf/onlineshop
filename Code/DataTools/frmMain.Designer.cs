﻿namespace DataTools
{
    partial class txtMainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImportCityCode = new System.Windows.Forms.Button();
            this.btnThumb = new System.Windows.Forms.Button();
            this.btnBackupDB = new System.Windows.Forms.Button();
            this.btnRestoreDB = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnImportCityCode
            // 
            this.btnImportCityCode.Location = new System.Drawing.Point(12, 12);
            this.btnImportCityCode.Name = "btnImportCityCode";
            this.btnImportCityCode.Size = new System.Drawing.Size(107, 36);
            this.btnImportCityCode.TabIndex = 0;
            this.btnImportCityCode.Text = "导入城市编号";
            this.btnImportCityCode.UseVisualStyleBackColor = true;
            this.btnImportCityCode.Click += new System.EventHandler(this.btnImportCityCode_Click);
            // 
            // btnThumb
            // 
            this.btnThumb.Location = new System.Drawing.Point(12, 55);
            this.btnThumb.Name = "btnThumb";
            this.btnThumb.Size = new System.Drawing.Size(107, 34);
            this.btnThumb.TabIndex = 1;
            this.btnThumb.Text = "处理图片缩略图";
            this.btnThumb.UseVisualStyleBackColor = true;
            this.btnThumb.Click += new System.EventHandler(this.btnThumb_Click);
            // 
            // btnBackupDB
            // 
            this.btnBackupDB.Location = new System.Drawing.Point(13, 96);
            this.btnBackupDB.Name = "btnBackupDB";
            this.btnBackupDB.Size = new System.Drawing.Size(106, 38);
            this.btnBackupDB.TabIndex = 2;
            this.btnBackupDB.Text = "备份数据库";
            this.btnBackupDB.UseVisualStyleBackColor = true;
            this.btnBackupDB.Click += new System.EventHandler(this.btnBackupDB_Click);
            // 
            // btnRestoreDB
            // 
            this.btnRestoreDB.Location = new System.Drawing.Point(13, 140);
            this.btnRestoreDB.Name = "btnRestoreDB";
            this.btnRestoreDB.Size = new System.Drawing.Size(106, 38);
            this.btnRestoreDB.TabIndex = 3;
            this.btnRestoreDB.Text = "恢复数据库";
            this.btnRestoreDB.UseVisualStyleBackColor = true;
            this.btnRestoreDB.Click += new System.EventHandler(this.btnRestoreDB_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 185);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 32);
            this.button1.TabIndex = 4;
            this.button1.Text = "处理黑边";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 365);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRestoreDB);
            this.Controls.Add(this.btnBackupDB);
            this.Controls.Add(this.btnThumb);
            this.Controls.Add(this.btnImportCityCode);
            this.Name = "txtMainForm";
            this.Text = "数据管理";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnImportCityCode;
        private System.Windows.Forms.Button btnThumb;
        private System.Windows.Forms.Button btnBackupDB;
        private System.Windows.Forms.Button btnRestoreDB;
        private System.Windows.Forms.Button button1;
    }
}

