﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataTools
{
    public partial class txtMainForm : Form
    {
        public txtMainForm()
        {
            InitializeComponent();
        }

        private void btnImportCityCode_Click(object sender, EventArgs e)
        {
            String[] content = File.ReadAllLines("CityCode.txt");

            for (int i = 0; i < content.Length; i++)
            {
                String[] info = content[i].Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                model.dict.CityCode city = new model.dict.CityCode();

                city.CityName = info[1];
                city.Code = Int32.Parse(info[0]);
                city.ParentCode = GetCodeParent(Int32.Parse(info[0]));

                OR.DAL.Add<model.dict.CityCode>(city, false);
            }
        }


        private int GetCodeParent(int code)
        {
            if (code % 100 == 0)
            {
                return GetCodeParent(code / 100) * 100;
            }
            else
            {
                return (code / 100) * 100;
            }
        }

        private void btnThumb_Click(object sender, EventArgs e)
        {
            System.Drawing.Image o = System.Drawing.Image.FromFile("D:\\01.jpg");

            //MakeThumbnail(o, "D:\\01_M.jpg", 500, 500);
        }

        private void btnBackupDB_Click(object sender, EventArgs e)
        {
            DirectoryInfo appDir = new DirectoryInfo(System.IO.Directory.GetCurrentDirectory());

            DirectoryInfo releaseDir = appDir.Parent;

            if (File.Exists(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak")))
            {
                File.Delete(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak"));
            }

            if (File.Exists(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.zip")))
            {
                File.Delete(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.zip"));
            }

            String cmd = String.Format("BACKUP DATABASE [OnlineShop] TO DISK = N'{0}' WITH NOFORMAT, NOINIT,  NAME = N'OnlineShop-Full Database Backup', SKIP, NOREWIND, NOUNLOAD, STATS = 10",
                Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak"));

            OR.SQLHelper.ExecuteSql(cmd);

            ZipFile z = ZipFile.Create(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.zip"));
            z.NameTransform = new ZipNameTransform(Path.Combine(releaseDir.FullName, "Database"));

            z.BeginUpdate();
            z.Add(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak"));
            z.CommitUpdate();
            z.Close();

            if (File.Exists(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak")))
            {
                File.Delete(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak"));
            }

            MessageBox.Show("数据库备份完成");
        }

        private void btnRestoreDB_Click(object sender, EventArgs e)
        {
            DirectoryInfo appDir = new DirectoryInfo(System.IO.Directory.GetCurrentDirectory());

            DirectoryInfo releaseDir = appDir.Parent;

            if (File.Exists(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak")))
            {
                File.Delete(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak"));
            }

            if (!File.Exists(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.zip")))
            {
                MessageBox.Show("对不起，找不到数据库备份文件");
                return;
            }

            ZipInputStream s = new ZipInputStream(File.OpenRead(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.zip")));

            try
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {

                    if (theEntry.Name.Equals("OnlineShop.bak"))
                    {

                        FileStream streamWriter = File.Create(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak"));

                        int size = 1024 * 100;
                        byte[] data = new byte[1024 * 100];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }

                        streamWriter.Close();
                        break;
                    }
                }
            }
            catch (Exception eu)
            {
                throw eu;
            }
            finally
            {
                s.Close();
            }

            String cmd = String.Format("RESTORE DATABASE [OnlineShop1] FROM DISK = N'{0}\\OnlineShop.bak' "
                    + "WITH FILE = 1, MOVE N'OnlineShop_dat' TO N'{0}\\OnlineShop1.mdf" + "', MOVE N'OnlineShop_Log' TO N'{0}\\OnlineShop1.ldf" + "', "
                    + "KEEP_REPLICATION,  NOUNLOAD,  STATS = 10",
                Path.Combine(releaseDir.FullName, "Database"));

            OR.SQLHelper.ExecuteSql(cmd);

            if (File.Exists(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak")))
            {
                File.Delete(Path.Combine(releaseDir.FullName, "Database", "OnlineShop.bak"));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String folder = @"D:\Data\Upload\Common";

            DirectoryInfo dir = new DirectoryInfo(folder);

            process(dir);
        }

        private void process(DirectoryInfo dir)
        {
            DirectoryInfo[] dirs = dir.GetDirectories();

            for (int i = 0; i < dirs.Length; i++)
            {
                process(dirs[i]);
            }

            FileInfo[] files = dir.GetFiles();

            String[] exts = new String[]{".jpg",".png",".bmp"};

            for (int i = 0; i < files.Length; i++)
            {
                FileInfo file = files[i];

                if (exts.Contains(file.Extension.ToLower()))
                {
                    if (!(file.Name.ToLower().EndsWith("_s.jpg") || file.Name.ToLower().EndsWith("_m.jpg") || file.Name.ToLower().EndsWith("_b.jpg")))
                    {
                        // 原图，要处理一下
                        String folder = file.Directory.FullName;
                        String guid = file.Name.Split('.')[0];
                        

                        System.Drawing.Image imgsrc = System.Drawing.Image.FromFile(file.FullName);

                        MakeThumbnail(imgsrc, folder + "\\" + guid + "_" + model.res.ThumbLevel.S.ToString() + ".jpg", 50, 50);
                        MakeThumbnail(imgsrc, folder + "\\" + guid + "_" + model.res.ThumbLevel.M.ToString() + ".jpg", 350, 350);
                        MakeThumbnail(imgsrc, folder + "\\" + guid + "_" + model.res.ThumbLevel.B.ToString() + ".jpg", 1000, 1000);

                    }
                }
            }
        }


        /// <summary>
        /// 生成缩略图.每个照片需要生成两幅缩略图，一个是图标型的，120*90，一个是中等尺寸的，用于相册浏览400*300，再保留一个原图，用于浏览看大图
        /// </summary>
        /// <param name="originalImage"></param>
        /// <param name="fileName"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public static void MakeThumbnail(System.Drawing.Image originalImage, String fileName, int width, int height)
        {

            int towidth = width;
            int toheight = height;

            if (originalImage.Width > width || originalImage.Height > height)
            {
                // 宽的比例比较大，按照宽的比例来处理图片
                if ((double)originalImage.Width / (double)originalImage.Height > (double)width / (double)height)
                {
                    towidth = width;
                    toheight = originalImage.Height * width / originalImage.Width;
                }
                else
                {
                    towidth = originalImage.Width * height / originalImage.Height;
                    toheight = height;
                }
            }

            //新建一个bmp图片
            System.Drawing.Image bitmap = new System.Drawing.Bitmap(width, height);

            //新建一个画板
            Graphics g = System.Drawing.Graphics.FromImage(bitmap);

            //设置高质量插值法
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

            //设置高质量,低速度呈现平滑程度
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            //清空画布并以白色背景色填充
            g.Clear(Color.White);

            //在指定位置并且按指定大小绘制原图片的指定部分
            g.DrawImage(originalImage, new Rectangle((width - towidth) / 2, (height - toheight) / 2, towidth, toheight),
                new Rectangle(0, 0, originalImage.Width, originalImage.Height),
                GraphicsUnit.Pixel);

            try
            {
                //以jpg格式保存缩略图
                bitmap.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                bitmap.Dispose();
                g.Dispose();
            }
        }
    }
}
