﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_OrgInfo : System.Web.UI.Page
{
    protected String strOrgId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strOrgId = Request.QueryString["OrgId"];

        if (!IsPostBack)
        {
            model.member.OrgInfo u = OR.DAL.GetModelByKey<model.member.OrgInfo>(Convert.ToInt32(strOrgId));

            // 如果按ID找不到用户，则返回列表，避免直接输入URL找不到用户
            if (u == null)
            {
                Response.Redirect("./OrgAdmin.aspx");
                return;
            }
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        model.member.OrgInfo u = OR.DAL.GetModelByKey<model.member.OrgInfo>(Convert.ToInt32(strOrgId));
        u.Updated = DateTime.Now;
        u.Status = (int)model.Status.Delete;

        OR.DAL.Update<model.member.OrgInfo>(u);

        Common.Logger.info("逻辑删除单位：" + u.ToString(true));

        Response.Redirect("./OrgAdmin.aspx");
    }
}