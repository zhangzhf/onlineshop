﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_UserAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        master_MasterPage master = (master_MasterPage)this.Master;
        master.PageTitle = "用户帐号管理";

        if (!IsPostBack)
        {
            InitUI();

            ViewState["_SQL"] = String.Format(" Status<>{0} Order by UserAccount", ((int)model.Status.Delete).ToString());

            GetList();
        }
    }

    protected void InitUI()
    {
        this.ddpUserStatus.Items.Add(new ListItem("全部", ""));

        this.ddpUserStatus.Items.Add(new ListItem(EnumUtils.GetDescription(model.Status.Normal), ((int)model.Status.Normal).ToString()));
        this.ddpUserStatus.Items.Add(new ListItem(EnumUtils.GetDescription(model.Status.Locked), ((int)model.Status.Locked).ToString()));
    }

    protected void GetList()
    {
        int _RowCount = 0;

        List<model.member.UserInfo> userList = OR.DAL.GetModelList<model.member.UserInfo>(ViewState["_SQL"].ToString(), this.AspNetPager.CurrentPageIndex - 1, ref _RowCount);

        this.AspNetPager.RecordCount = _RowCount;

        this.GridView1.DataSource = userList;
        this.GridView1.DataBind();
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        String strCondition = String.Empty;

        // 用户状态
        if (this.ddpUserStatus.SelectedIndex != 0)
        {
            strCondition = String.Format(" Status={0}", this.ddpUserStatus.SelectedValue);
        }
        else
        {
            strCondition = String.Format(" Status<>{0}", ((int)model.Status.Delete).ToString());
        }

        // 关键词
        if (!String.IsNullOrEmpty(this.txtKeyword.Text.Trim()))
        {
            strCondition += String.Format(" AND (UserName like '%{0}%' OR UserAccount like '%{0}%' )", this.txtKeyword.Text.Trim());
        }

        strCondition += " Order by UserAccount";

        ViewState["_SQL"] = strCondition;

        this.AspNetPager.CurrentPageIndex = 1;

        GetList();
    }

    protected void AspNetPager_PageChanged(object sender, EventArgs e)
    {
        GetList();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            model.member.UserInfo user = e.Row.DataItem as model.member.UserInfo;

            HyperLink hyEditUser = e.Row.FindControl("hyEditUser") as HyperLink;
            hyEditUser.NavigateUrl = String.Format("javascript:EditUser('{0}')", user.UserId);

            Label txtOrgname = e.Row.FindControl("txtOrgname") as Label;
            txtOrgname.Text = model.member.OrgInfoImpl.GetModelByKey(user.OrgId).OrgName;
        }
    }
}