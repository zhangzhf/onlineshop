﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_UserInfo : System.Web.UI.Page
{
    protected String strUserId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strUserId = Request.QueryString["UserId"];

        if (!IsPostBack)
        {
            model.member.UserInfo u = OR.DAL.GetModelByKey<model.member.UserInfo>(Convert.ToInt32(strUserId));

            // 如果按ID找不到用户，则返回列表，避免直接输入URL找不到用户
            if (u == null)
            {
                Response.Redirect("./UserAdmin.aspx");
                return;
            }

            // 如果是当前用户，则不允许删除
            if (u.UserId == Common.Utils.LoginUser.UserId)
            {
                this.btnDelete.Visible = false;
            }
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        model.member.UserInfo u = OR.DAL.GetModelByKey<model.member.UserInfo>(Convert.ToInt32(strUserId));
        u.Updated = DateTime.Now;
        u.Status = (int)model.Status.Delete;

        OR.DAL.Update<model.member.UserInfo>(u);

        Common.Logger.info("逻辑删除用户：" + u.ToString(true));

        Response.Redirect("./UserAdmin.aspx");
    }
}