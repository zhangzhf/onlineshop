﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_OrgAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["_SQL"] = " Status<>" + ((int)model.Status.Delete).ToString() + " Order by OrgName";

            GetUnitList();
        }
    }


    protected void GetUnitList()
    {
        List<model.member.OrgInfo> userList = OR.DAL.GetModelList<model.member.OrgInfo>(ViewState["_SQL"].ToString());

        this.GridView1.DataSource = userList;
        this.GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            model.member.OrgInfo org = e.Row.DataItem as model.member.OrgInfo;

            HyperLink hyEditOrg = e.Row.FindControl("hyEditOrg") as HyperLink;
            hyEditOrg.NavigateUrl = String.Format("javascript:EditOrg('{0}')", org.OrgId);
        }
    }
}