﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_UserEdit : System.Web.UI.Page
{
    private String strUserId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strUserId = Request.QueryString["UserId"];

        if (!IsPostBack)
        {

            InitUI();

            if (!String.IsNullOrEmpty(strUserId))
            {
                LoadUserInfo();
            }
        }
    }

    protected void InitUI()
    {
        List<model.member.OrgInfo> orgs = OR.DAL.GetModelList<model.member.OrgInfo>("Status<>@status",
            new System.Data.SqlClient.SqlParameter("@status", (int)model.Status.Delete));

        this.ddpOrgInfo.DataSource = orgs;
        this.ddpOrgInfo.DataBind();
    }

    protected void LoadUserInfo()
    {
        model.member.UserInfo user = OR.DAL.GetModelByKey<model.member.UserInfo>(Convert.ToInt32(strUserId));

        this.txtUserAccount.Text = user.UserAccount;
        this.txtUserAccount.Enabled = false;

        this.txtPassword1.Text = "";
        this.txtPassword2.Text = "";

        this.txtAddress.Text = user.Address;
        this.txtCellPhone.Text = user.Cellphone;
        this.txtEmail.Text = user.Email;
        this.txtFax.Text = user.Fax;
        this.txtLandlinePhone.Text = user.LandlinePhone;
        this.txtMemo.Text = user.Memo;
        this.txtNickName.Text = user.NickName;
        this.txtQQ.Text = user.QQ;
        this.txtRealName.Text = user.RealName;
        this.txtUserAccount.Text = user.UserAccount;
        this.txtWeChat.Text = user.WeChat;
        this.txtZIP.Text = user.ZIP;

        this.chkStatus.Checked = (user.Status == 1 ? true : false);

        if (user.OrgId != 0)
        {
            this.ddpOrgInfo.SelectedValue = user.OrgId.ToString();
        }

        RequiredFieldValidator2.Enabled = false;
    }

    /// <summary>
    /// 这里在一起处理了插入和更新的代码。通过判断是否有传入参数来区分两者的状态
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(strUserId))
        {
            model.member.UserInfo user = OR.DAL.GetModel<model.member.UserInfo>("UserAccount=@account",
                new System.Data.SqlClient.SqlParameter("@account", this.txtUserAccount.Text));

            if (user != null)
            {
                this.txtMsg.Text = "该帐号已被占用";
                return;
            }

            // 添加新用户
            user = new model.member.UserInfo();
            
            user.UserAccount = this.txtUserAccount.Text;
            user.Password = this.txtPassword1.Text;
            user.NickName = this.txtNickName.Text;
            user.RealName = this.txtRealName.Text;

            user.Created = DateTime.Now;
            user.Updated = DateTime.Now;
            user.Status = (int)(chkStatus.Checked ? model.Status.Normal : model.Status.Locked);

            user.OrgId = Convert.ToInt32(this.ddpOrgInfo.SelectedValue);

            user.CityCode = 0;
            user.Address = this.txtAddress.Text;
            user.ZIP = this.txtZIP.Text;
            
            user.Cellphone = this.txtCellPhone.Text;
            user.Email = this.txtEmail.Text;
            user.Fax = this.txtFax.Text;
            user.LandlinePhone = this.txtLandlinePhone.Text;
            user.QQ = this.txtQQ.Text;
            user.WeChat = this.txtWeChat.Text;
            user.Memo = this.txtMemo.Text;
            
            user = OR.DAL.Add<model.member.UserInfo>(user, true);

            Common.Logger.info("添加用户：" + user.ToString(true));
        }
        else
        {
            // 更新内容
            model.member.UserInfo user = OR.DAL.GetModelByKey<model.member.UserInfo>(strUserId);

            if (user == null)
            {
                this.txtMsg.Text = "对不起，找不到该用户";
                return;
            }

            if (!String.IsNullOrEmpty(this.txtPassword1.Text))
            {
                user.Password = Common.Utils.to_md5(this.txtPassword1.Text);
            }
            user.Updated = DateTime.Now;
            user.Status = (int)(chkStatus.Checked ? model.Status.Normal : model.Status.Locked);


            user.NickName = this.txtNickName.Text;
            user.RealName = this.txtRealName.Text;

            user.OrgId = Convert.ToInt32(this.ddpOrgInfo.SelectedValue);

            user.CityCode = 0;
            user.Address = this.txtAddress.Text;
            user.ZIP = this.txtZIP.Text;

            user.Cellphone = this.txtCellPhone.Text;
            user.Email = this.txtEmail.Text;
            user.Fax = this.txtFax.Text;
            user.LandlinePhone = this.txtLandlinePhone.Text;
            user.QQ = this.txtQQ.Text;
            user.WeChat = this.txtWeChat.Text;
            user.Memo = this.txtMemo.Text;

            OR.DAL.Update<model.member.UserInfo>(user);

            Common.Logger.info("更新用户：" + user.ToString(true));
        }

        Common.Utils.ResponseCloseDialogScript();
    }
}