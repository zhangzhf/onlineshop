﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/PopupPage.master" AutoEventWireup="true" CodeFile="OrgEdit.aspx.cs" Inherits="Member_OrgEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script type="text/javascript">

        $(document).ready(function () {
            $("#btnCancel").click(function () {
                parent.CloseDialog();
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ToolBar" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="tableInfo">
        <thead>
            <tr>
                <th colspan="2">请填写单位信息
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>单位名称：
                </td>
                <td>
                    <asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="请输入单位名称"
                        ControlToValidate="txtOrgName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>地址：
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>邮编：
                </td>
                <td>
                    <asp:TextBox ID="txtZIP" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>备注：
                </td>
                <td>
                    <asp:TextBox ID="txtMemo" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>单位状态：
                </td>
                <td>
                    <asp:CheckBox ID="chkStatus" runat="server" Checked="true" />
                    启用
                </td>
            </tr>
        </tbody>
    </table>
    <div class="divFooter">
        <asp:Button ID="btnSubmit" runat="server" Text="保存" OnClick="btnSubmit_Click" />
        <input id="btnCancel" type="button" value="取消" />
        <br />
    </div>
    <asp:Label ID="txtMsg" runat="server" Text=""></asp:Label>
</asp:Content>

