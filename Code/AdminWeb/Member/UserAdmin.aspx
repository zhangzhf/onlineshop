﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserAdmin.aspx.cs" Inherits="Member_UserAdmin" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {

            $("#btnAddUser").click(function () {

                document.getElementById("dialogContent").src = "./UserEdit.aspx";

                $("#dialog").dialog({ title: '添加用户', resizable: false, width: 600, height: 600, modal: true });
                $('#dialog').dialog('open');

            });

        });

        function CloseDialog() {
            $('#dialog').dialog('close');
        }

        function EditUser(userId) {
            document.getElementById("dialogContent").src = "./UserEdit.aspx?UserId=" + userId;

            $("#dialog").dialog({ title: '编辑用户', resizable: false, width: 600, height: 600, modal: true });
            $('#dialog').dialog('open');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
    用户管理
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
    用户状态：
    <asp:DropDownList ID="ddpUserStatus" runat="server">
    </asp:DropDownList>
    关键字：
    <asp:TextBox ID="txtKeyword" runat="server"></asp:TextBox>
    <asp:Button ID="btnQuery" runat="server" Text="查询" OnClick="btnQuery_Click" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
    <input id="btnAddUser" type="button" value="添加用户" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:HyperLinkField HeaderText="用户帐号" DataTextField="UserAccount" DataNavigateUrlFields="UserId" DataNavigateUrlFormatString="./UserInfo.aspx?UserId={0}" />
            <asp:BoundField HeaderText="用户姓名" DataField="RealName" />
            <asp:TemplateField HeaderText="所在单位">
                <ItemTemplate>
                    <asp:Label ID="txtOrgname" runat="server" Text=""></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="电话" DataField="CellPhone" />
            <asp:BoundField HeaderText="Email" DataField="Email" />
            <asp:BoundField HeaderText="创建时间" DataField="Created" DataFormatString="{0:yyy-MM-dd HH:mm}" />
            <asp:TemplateField HeaderText="用户状态">
                <ItemTemplate>
                    <%# EnumUtils.GetDescription((model.Status)Convert.ToInt32( Eval("Status").ToString()) )%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="操作">
                <ItemTemplate>
                    <asp:HyperLink ID="hyEditUser" runat="server">编辑</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <div class="divPager">
        <webdiyer:AspNetPager ID="AspNetPager" runat="server" OnPageChanged="AspNetPager_PageChanged">
        </webdiyer:AspNetPager>
    </div>
    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src=""></iframe>
    </div>
</asp:Content>
