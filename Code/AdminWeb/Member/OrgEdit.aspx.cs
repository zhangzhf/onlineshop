﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_OrgEdit : System.Web.UI.Page
{
    private String strOrgId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strOrgId = Request.QueryString["OrgId"];

        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(strOrgId))
            {
                LoadOrgInfo();
            }
        }
    }

    protected void LoadOrgInfo()
    {
        model.member.OrgInfo org = OR.DAL.GetModelByKey<model.member.OrgInfo>(Convert.ToInt32(strOrgId));

        this.txtOrgName.Text = org.OrgName;
        this.txtAddress.Text = org.Address;
        this.txtMemo.Text = org.Memo;
        this.txtZIP.Text = org.ZIP;

        this.chkStatus.Checked = (org.Status == 1 ? true : false);

    }

    /// <summary>
    /// 这里在一起处理了插入和更新的代码。通过判断是否有传入参数来区分两者的状态
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(strOrgId))
        {
            model.member.OrgInfo org = OR.DAL.GetModel<model.member.OrgInfo>("OrgName=@orgname",
                new System.Data.SqlClient.SqlParameter("@orgname", this.txtOrgName.Text));

            if (org != null)
            {
                this.txtMsg.Text = "该单位名称已被占用";
                return;
            }

            // 添加新用户
            org = new model.member.OrgInfo();

            org.OrgName = this.txtOrgName.Text;

            org.Created = DateTime.Now;
            org.Updated = DateTime.Now;
            org.Status = (int)(chkStatus.Checked ? model.Status.Normal : model.Status.Locked);

            org.CityCode = 0;
            org.Address = this.txtAddress.Text;
            org.ZIP = this.txtZIP.Text;

            org.Memo = this.txtMemo.Text;

            org = OR.DAL.Add<model.member.OrgInfo>(org, true);

            Common.Logger.info("添加单位：" + org.ToString(true));
        }
        else
        {
            // 更新内容
            model.member.OrgInfo org = OR.DAL.GetModelByKey<model.member.OrgInfo>(strOrgId);

            if (org == null)
            {
                this.txtMsg.Text = "对不起，找不到该单位";
                return;
            }

            org.Updated = DateTime.Now;
            org.Status = (int)(chkStatus.Checked ? model.Status.Normal : model.Status.Locked);

            org.CityCode = 0;
            org.Address = this.txtAddress.Text;
            org.ZIP = this.txtZIP.Text;

            org.Memo = this.txtMemo.Text;

            OR.DAL.Update<model.member.OrgInfo>(org);

            Common.Logger.info("更新单位：" + org.ToString(true));
        }

        Common.Utils.ResponseCloseDialogScript();
    }
}