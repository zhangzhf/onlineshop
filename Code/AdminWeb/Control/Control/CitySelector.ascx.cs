﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_Control_CitySelector : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void SetCity(int code)
    {

    }

    public void LoadCity()
    {
        List<model.dict.CityCode> cities = OR.DAL.GetModelList<model.dict.CityCode>("ParentCode=0 Order By Code");

        this.ddpCity1.DataSource = cities;
        this.ddpCity1.DataBind();

        this.ddpCity1.SelectedIndex = 0;

        ddpCity1_SelectedIndexChanged(null, null);
    }

    protected void ddpCity1_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<model.dict.CityCode> cities = OR.DAL.GetModelList<model.dict.CityCode>("ParentCode=" + this.ddpCity1.SelectedValue + " Order By Code");

        this.ddpCity2.DataSource = cities;
        this.ddpCity2.DataBind();

        this.ddpCity2.SelectedIndex = 0;

        ddpCity2_SelectedIndexChanged(null, null);
    }

    protected void ddpCity2_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<model.dict.CityCode> cities = OR.DAL.GetModelList<model.dict.CityCode>("ParentCode=" + this.ddpCity2.SelectedValue + " Order By Code");

        this.ddpCity3.DataSource = cities;
        this.ddpCity3.DataBind();
    }
}