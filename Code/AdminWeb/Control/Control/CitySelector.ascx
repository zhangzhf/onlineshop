﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CitySelector.ascx.cs" Inherits="Control_Control_CitySelector" %>

<asp:DropDownList ID="ddpCity1" runat="server" DataTextField="CityName" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddpCity1_SelectedIndexChanged"></asp:DropDownList>
<asp:DropDownList ID="ddpCity2" runat="server" DataTextField="CityName" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddpCity2_SelectedIndexChanged"></asp:DropDownList>
<asp:DropDownList ID="ddpCity3" runat="server" DataTextField="CityName" DataValueField="Code"></asp:DropDownList>
