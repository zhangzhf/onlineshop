﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterPage.master" AutoEventWireup="true" CodeFile="BannerAdmin.aspx.cs" Inherits="Portal_BannerAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script language="javascript" type="text/javascript">

        $(document).ready(function ()
        {
            $("#btnAdd").click(function ()
            {
                CommonUploadFile("上传Banner图片", "UploadBanner_Callback");
            });
        });

        function UploadBanner_Callback(file)
        {
            $("#<%=txtNewFileId.ClientID%>").val(file.FileId);
            $("#<%=btnSubmit.ClientID%>").click();
        }

        function CloseDialog()
        {
            $('#dialog').dialog('close');
        }

        function EditBanner(bid)
        {
            document.getElementById("dialogContent").src = "./BannerEdit.aspx?bid=" + bid;

            $("#dialog").dialog({ title: '编辑图片', resizable: false, width: 600, height: 300, modal: true });
            $('#dialog').dialog('open');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" Runat="Server">
    <input type="button" id="btnAdd" value="新增" />
    <asp:TextBox ID="txtNewFileId" runat="server" CssClass="nodisplay"></asp:TextBox>
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="nodisplay" OnClick="btnSubmit_Click" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand">
        <Columns>
            <asp:BoundField HeaderText="顺序" DataField="OrderId" HeaderStyle-Width="50px" />
            <asp:TemplateField HeaderText="图片">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="状态" HeaderStyle-Width="50px">
                <ItemTemplate>
                    <%# EnumUtils.GetDescription((model.Status)Convert.ToInt32( Eval("Status").ToString()) )%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="上传时间" DataField="Created" DataFormatString="{0:yyy-MM-dd HH:mm}" HeaderStyle-Width="150px" />
            <asp:TemplateField HeaderText="操作" HeaderStyle-Width="100px">
                <ItemTemplate>
                    <asp:HyperLink ID="btnEdit" runat="server">编辑</asp:HyperLink>
                    <asp:LinkButton ID="btnDelete" runat="server" OnClientClick="javascript:confirm('确认删除所选图片?')">删除</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent"></iframe>
    </div>

</asp:Content>

