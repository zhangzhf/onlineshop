﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Portal_BannerAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetList();
        }
    }

    protected void GetList()
    {
        String sql = " Status<>@delete Order by OrderId, Created Desc";

        List<model.portal.Banner> banners = OR.DAL.GetModelList<model.portal.Banner>(sql,
            OR.Param.NamedParam("@delete", (int)model.Status.Delete));

        this.GridView1.DataSource = banners;
        this.GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            model.portal.Banner banner = e.Row.DataItem as model.portal.Banner;

            model.res.FileInfo pic = model.res.FileInfoImpl.GetModelByKey(banner.FileId);
            System.Drawing.Size size = model.res.FileInfoImpl.GetImageBound(pic, 800, 300);

            Image image = e.Row.FindControl("Image1") as Image;
            image.ImageUrl = model.res.FileInfoImpl.GetPictureThumb(pic, model.res.ThumbLevel.O);
            image.Width = size.Width;
            image.Height = size.Height;

            HyperLink btnEdit = e.Row.FindControl("btnEdit") as HyperLink;
            btnEdit.NavigateUrl = String.Format("javascript:EditBanner('{0}')", banner.BannerId);

            LinkButton btnDelete = e.Row.FindControl("btnDelete") as LinkButton;
            btnDelete.CommandName = "Del";
            btnDelete.CommandArgument = banner.BannerId.ToString();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        model.portal.Banner banner = new model.portal.Banner();

        banner.Created = DateTime.Now;
        banner.FileId = Convert.ToInt32(this.txtNewFileId.Text);
        banner.Status = (int)model.Status.Normal;
        banner.OrderId = 0;
        banner.Comment = "";
        banner.Updated = DateTime.Now;

        banner = OR.DAL.Add<model.portal.Banner>(banner, true);

        Common.Logger.info("新增 Banner: " + banner.ToString());

        GetList();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Del")
        {
            model.portal.Banner banner = OR.DAL.GetModelByKey<model.portal.Banner>(Convert.ToInt32(e.CommandArgument.ToString()));
            banner.Status = (int)model.Status.Delete;

            OR.DAL.Update<model.portal.Banner>(banner);

            Common.Logger.info("逻辑删除 Banner: " + banner.ToString());

            GetList();
        }
    }
}