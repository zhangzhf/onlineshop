﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class master_MasterPage : System.Web.UI.MasterPage
{
    #region 页面基本功能

    private bool _setTitle = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.txtLogo.Text = Common.Utils.GetBrandname();

            if (String.IsNullOrEmpty(Page.Title) && !_setTitle)
            {
                Page.Title = Common.Utils.GetBrandname(); ;
            }

            model.member.UserInfo user = Common.Utils.LoginUser;
            this.labUserName.Text = user.NickName;

            LoadMenuTree();
        }
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        Common.Logger.info("用户退出登录");

        Session.RemoveAll();

        System.Web.Security.FormsAuthentication.SignOut();
        System.Web.Security.FormsAuthentication.RedirectToLoginPage();
    }

    /// <summary>
    /// 设置页面标题
    /// </summary>
    public String PageTitle
    {
        set
        {
            Page.Title = String.Format("{0}-{1}", Common.Utils.GetBrandname(), value);
            _setTitle = true;
        }
    }

    #endregion

    #region 对菜单内容进行初始化

    /// <summary>
    /// 初始化菜单树
    /// </summary>
    protected void LoadMenuTree()
    {

        TreeNode root = new TreeNode("管理中心");
        root.SelectAction = TreeNodeSelectAction.Expand;
        root.ImageUrl = "~/images/comp2.gif";
        root.NavigateUrl = "~/Admin/Default.aspx";
        root.Expanded = true;
        this.treeMenu.Nodes.Add(root);

        LoadMenu(root);
    }

    protected void LoadMenu(TreeNode root)
    {
        XmlDocument doc = Common.MenuUtils.GetMenuDocument();

        XmlNodeList menus = doc.SelectNodes("/root/folder");

        String sessionURL = Session["selectedURL"] == null ? "" : Session["selectedURL"].ToString();
        String currUrl = Request.Url.AbsolutePath.ToLower();
        bool hasSelectedMenu = false;

        foreach (XmlNode folder in menus)
        {
            TreeNode nodeFolder = new TreeNode(folder.Attributes["name"].Value, folder.Attributes["id"].Value);
            nodeFolder.SelectAction = TreeNodeSelectAction.Expand;
            nodeFolder.Expanded = true;
            nodeFolder.ImageUrl = folder.Attributes["icon"] == null ? "~/images/folder.gif" : folder.Attributes["icon"].Value;

            XmlNodeList subMenus = folder.SelectNodes("menu");

            foreach (XmlNode item in subMenus)
            {
                TreeNode nodeMenu = new TreeNode(item.Attributes["name"].Value, item.Attributes["id"].Value);
                nodeMenu.SelectAction = TreeNodeSelectAction.Select;
                nodeMenu.Expanded = false;
                nodeMenu.NavigateUrl = item.Attributes["url"].Value;
                nodeMenu.ImageUrl = item.Attributes["icon"] == null ? "~/images/page.png" : item.Attributes["icon"].Value;
                nodeFolder.ChildNodes.Add(nodeMenu);

                // 给菜单加上选中的样式，标识当前哪个是活动菜单
                String currPage = this.Page.ResolveUrl(item.Attributes["url"].Value);

                // 先比较session中的
                if (!hasSelectedMenu && currPage.ToLower().Equals(sessionURL))
                {
                    nodeMenu.Selected = true;
                }

                // 如果找到了合适的，以此为准，并记录下来
                if (!hasSelectedMenu && currPage.ToLower().Equals(currUrl))
                {
                    nodeMenu.Selected = true;
                    hasSelectedMenu = true;
                    Session["selectedURL"] = currUrl;
                }
            }

            if (nodeFolder.ChildNodes.Count > 0)
            {
                root.ChildNodes.Add(nodeFolder);
            }

            ShowExpendNode(nodeFolder);
        }
    }

    protected void TreeView1_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
        Session["Expand_" + e.Node.Value] = "true";
    }

    protected void TreeView1_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
    {
        Session["Expand_" + e.Node.Value] = "false";
    }

    protected void ShowExpendNode(TreeNode node)
    {
        if (Session["Expand_" + node.Value] != null)
        {
            node.Expanded = (Session["Expand_" + node.Value] == null ? true : Boolean.Parse(Session["Expand_" + node.Value].ToString()));
        }
    }

    #endregion
}
