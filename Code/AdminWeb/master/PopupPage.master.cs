﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class master_PopupPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// 设置或者获取工具栏区是否显示。默认不显示
    /// </summary>
    public bool ShowToolbar
    {
        get
        {
            return this.panelToolbar.Visible;
        }
        set
        {
            this.panelToolbar.Visible = value;
        }
    }
}
