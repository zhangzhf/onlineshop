﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// CityController 的摘要说明
/// </summary>
public class CityController : ApiController
{
    [HttpGet]
    [ActionName("GetCity")]
    public List<model.dict.CityCode> GetCity([FromUri]int rootCode)
    {
        List<model.dict.CityCode> cities = OR.DAL.GetModelList<model.dict.CityCode>("ParentCode=@code",
            new System.Data.SqlClient.SqlParameter("@code", rootCode));

        return cities;
    }
}