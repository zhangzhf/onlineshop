﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// CategoryUtils 的摘要说明
/// </summary>
public class CategoryUtils
{
    public static List<model.goods.Category> GetCategory()
    {
        String sql = @"SELECT a.* FROM Goods_Category a"
            + "  left join Goods_Category b ON a.parentId = b.CategoryId AND b.categoryId<>0 "
            + "  WHERE a.CategoryId > 0 And a.[Status]<>" + ((int)model.Status.Delete).ToString()
            + "  ORDER BY ISNULL(b.OrderId, a.OrderId),a.ParentId, a.OrderId,a.Created";

        List<model.goods.Category> categories = OR.DAL.DataTableToModel<model.goods.Category>(OR.SQLHelper.Query(sql).Tables[0]);

        return categories;
    }
}