﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_AttrEdit : System.Web.UI.Page
{
    private String categoryId = String.Empty;
    private String infoType = String.Empty;
    private String infoKey = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        categoryId = Request.QueryString["categoryId"];
        infoType = Request.QueryString["infoType"];
        infoKey = Request.QueryString["infoKey"];

        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(infoKey))
            {
                LoadAttrInfo();
            }
        }
    }

    protected void LoadAttrInfo()
    {
        model.goods.InfoKeys key = OR.DAL.GetModel<model.goods.InfoKeys>("CategoryId=@id And InfoType=@type And InfoKey=@key",
            OR.Param.NamedParam("@id", Convert.ToInt32(categoryId)),
            OR.Param.NamedParam("@type", infoType),
            OR.Param.NamedParam("@key", infoKey));

        if (key == null)
        {
            return;
        }

        this.txtInfoKey.Text = key.InfoKey;
        this.txtKeyName.Text = key.KeyName;
        this.txtOrderId.Text = key.OrderId.ToString();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(infoKey))
        {
            model.goods.InfoKeys key = new model.goods.InfoKeys();

            key.CategoryId = Convert.ToInt32(categoryId);
            key.Created = DateTime.Now;
            key.InfoKey = this.txtInfoKey.Text.Trim();
            key.InfoType = infoType;
            key.KeyName = this.txtKeyName.Text.Trim();
            key.OrderId = Convert.ToInt32(this.txtOrderId.Text);
            key.Status = (int)(chkStatus.Checked ? model.Status.Normal : model.Status.Locked);
            key.Updated = DateTime.Now;

            key = OR.DAL.Add<model.goods.InfoKeys>(key, true);

            Common.Logger.info("添加属性信息：" + key.ToString(true));
        }
        else
        {
            model.goods.InfoKeys key = OR.DAL.GetModel<model.goods.InfoKeys>("CategoryId=@id And InfoType=@type And InfoKey=@key",
                OR.Param.NamedParam("@id", Convert.ToInt32(categoryId)),
                OR.Param.NamedParam("@type", infoType),
                OR.Param.NamedParam("@key", infoKey));

            key.InfoKey = this.txtInfoKey.Text.Trim();
            key.KeyName = this.txtKeyName.Text.Trim();
            key.OrderId = Convert.ToInt32(this.txtOrderId.Text);
            key.Status = (int)(chkStatus.Checked ? model.Status.Normal : model.Status.Locked);
            key.Updated = DateTime.Now;

            key = OR.DAL.Add<model.goods.InfoKeys>(key, true);

            Common.Logger.info("更新属性信息：" + key.ToString(true));
        }

        Common.Utils.ResponseCloseDialogScript();
    }
}