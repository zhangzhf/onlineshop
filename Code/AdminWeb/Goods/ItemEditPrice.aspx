﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterPage.master" AutoEventWireup="true" CodeFile="ItemEditPrice.aspx.cs" Inherits="Goods_ItemEditPrice" %>

<%@ Register Src="../Common/InnerTabs.ascx" TagName="InnerTabs" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">

        $(document).ready(function ()
        {

            $("#btnAdd").click(function ()
            {
                document.getElementById("dialogContent").src = "./ItemEditPriceAdd.aspx?ItemId=<%=strItemId%>";

                $("#dialog").dialog({ title: '添加产品价格', resizable: false, width: 600, height: 300, modal: true });
                $('#dialog').dialog('open');

            });
        });

        function CloseDialog()
        {
            $('#dialog').dialog('close');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
    商品价格管理
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
    <input type="button" id="btnAdd" value="添加价格" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:InnerTabs ID="InnerTabs1" runat="server" />
    <!--
    列表列出产品的价格曲线。
    价格状态有以下：
    已过期
    当前生效
    尚未发布

    价格以发布生效时间进行排序

    发布动作时，同时修改两条记录状态，同时修改发布时间。
    然后，同时将生效的价格发布到基础信息中。
    -->
    <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand">
        <Columns>
            <asp:BoundField HeaderText="价格编号" DataField="TraceId" />
            <asp:TemplateField HeaderText="市场价格">
                <ItemTemplate>
                    <asp:Label ID="txtPriceMarket" runat="server" Text=""></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="零售价格">
                <ItemTemplate>
                    <asp:Label ID="txtPriceRetail" runat="server" Text=""></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="团购价格">
                <ItemTemplate>
                    <asp:Label ID="txtPriceGroupOn" runat="server" Text=""></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="价格生效时间">
                <ItemTemplate>
                    <asp:Label ID="txtStartTime" runat="server" Text=""></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="价格失效时间">
                <ItemTemplate>
                    <asp:Label ID="txtEndTime" runat="server" Text=""></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="价格状态">
                <ItemTemplate>
                    <%# EnumUtils.GetDescription((model.goods.PriceStatus)Convert.ToInt32( Eval("Status").ToString()) )%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="操作">
                <ItemTemplate>
                    <asp:LinkButton ID="btnSetActive" runat="server">发布</asp:LinkButton>
                    <asp:LinkButton ID="btnDelete" runat="server">删除</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src=""></iframe>
    </div>
</asp:Content>

