﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_CategoryAttrAdmin : System.Web.UI.Page
{
    protected String strCategoryId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strCategoryId = Request.QueryString["CategoryId"];

        if (!IsPostBack)
        {
            LoadInfoType();

            ddpInfoType_SelectedIndexChanged(null, null);
        }
    }

    public void LoadInfoType()
    {
        foreach (model.goods.InfoTypeCode type in Enum.GetValues(typeof(model.goods.InfoTypeCode)))
        {
            this.ddpInfoType.Items.Add(new ListItem(EnumUtils.GetDescription(type), type.ToString()));
        }
    }

    protected void ddpInfoType_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<model.goods.InfoKeys> keys = OR.DAL.GetModelList<model.goods.InfoKeys>("CategoryId=@id AND InfoType=@type And Status<>@status Order By OrderId, Created",
            OR.Param.NamedParam("@id", Convert.ToInt32(strCategoryId)),
            OR.Param.NamedParam("@type", this.ddpInfoType.SelectedValue),
            OR.Param.NamedParam("@status", (int)model.Status.Delete));

        this.GridView1.DataSource = keys;
        this.GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            model.goods.InfoKeys key = e.Row.DataItem as model.goods.InfoKeys;

            HyperLink hyEdit = e.Row.FindControl("hyEdit") as HyperLink;
            hyEdit.NavigateUrl = String.Format("javascript:EditAttr('{0}','{1}','{2}')",
                key.CategoryId, key.InfoType, key.InfoKey);

            LinkButton btnDelete = e.Row.FindControl("btnDelete") as LinkButton;
            btnDelete.CommandArgument = String.Format("{0}^{1}", key.InfoType, key.InfoKey);
            btnDelete.CommandName = "del";
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            String[] arg = e.CommandArgument.ToString().Split('^');

            String sql = "Update Goods_InfoKeys Set Status=@status where CategoryId=@categoryId And InfoType=@type And InfoKey=@key";
            OR.SQLHelper.ExecuteSql(sql,
                OR.Param.NamedParam("@status", (int)model.Status.Delete),
                OR.Param.NamedParam("@categoryId", Convert.ToInt32(strCategoryId)),
                OR.Param.NamedParam("@type", arg[0]),
                OR.Param.NamedParam("@key", arg[1]));

            ddpInfoType_SelectedIndexChanged(null, null);
        }
    }
}