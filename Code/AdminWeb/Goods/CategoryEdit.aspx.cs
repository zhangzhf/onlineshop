﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_CategoryEdit : System.Web.UI.Page
{
    private String strCateId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strCateId = Request.QueryString["CategoryId"];

        if (!IsPostBack)
        {
            InitUI();

            if (!String.IsNullOrEmpty(strCateId))
            {
                LoadCateInfo();
            }

            ddpParentCate_SelectedIndexChanged(null, null);
        }
    }

    protected void InitUI()
    {
        List<model.goods.Category> category = OR.DAL.GetModelList<model.goods.Category>("ParentId=0 Order By CategoryName");
        category.Insert(0, new model.goods.Category() { CategoryId = 0, CategoryName = "一级目录" });

        this.ddpParentCate.DataSource = category;
        this.ddpParentCate.DataBind();

        //// 找到当前最大的顺序号，默认值
        //int order = Convert.ToInt32(OR.SQLHelper.GetSingle("Select ISNULL(Max(OrderId),0)+1 OrderId From InfoPub_Board"));
        //this.txtOrderId.Text = order.ToString();
    }

    protected void LoadCateInfo()
    {
        model.goods.Category cate = OR.DAL.GetModelByKey<model.goods.Category>(Convert.ToInt32(strCateId));

        this.txtCategoryName.Text = cate.CategoryName;
        this.ddpParentCate.SelectedValue = cate.ParentId.ToString();
        this.txtOrderId.Text = cate.OrderId.ToString();

        this.chkStatus.Checked = (cate.Status == 1 ? true : false);
    }

    /// <summary>
    /// 这里在一起处理了插入和更新的代码。通过判断是否有传入参数来区分两者的状态
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(strCateId))
        {
            model.goods.Category category = OR.DAL.GetModel<model.goods.Category>("CategoryName=@name",
                new System.Data.SqlClient.SqlParameter("@name", this.txtCategoryName.Text));

            if (category != null)
            {
                this.txtMsg.Text = "该分类名称已被占用";
                return;
            }

            // 添加新分类
            category = new model.goods.Category();

            category.CategoryName = this.txtCategoryName.Text;
            category.ParentId = Convert.ToInt32(this.ddpParentCate.SelectedValue);
            category.OrderId = Convert.ToInt32(this.txtOrderId.Text);

            category.Created = DateTime.Now;
            category.Updated = DateTime.Now;
            category.Status = (int)(chkStatus.Checked ? model.Status.Normal : model.Status.Locked);

            category = OR.DAL.Add<model.goods.Category>(category, true);

            Common.Logger.info("添加分类：" + category.ToString(true));
        }
        else
        {
            // 更新内容
            model.goods.Category category = OR.DAL.GetModelByKey<model.goods.Category>(Convert.ToInt32(strCateId));

            if (category == null)
            {
                this.txtMsg.Text = "对不起，找不到该分类";
                return;
            }

            category.CategoryName = this.txtCategoryName.Text;
            category.ParentId = Convert.ToInt32(this.ddpParentCate.SelectedValue);
            category.OrderId = Convert.ToInt32(this.txtOrderId.Text);

            category.Updated = DateTime.Now;
            category.Status = (int)(chkStatus.Checked ? model.Status.Normal : model.Status.Locked);

            OR.DAL.Update<model.goods.Category>(category);

            Common.Logger.info("更新分类：" + category.ToString(true));
        }

        Common.Utils.ResponseCloseDialogScript();
    }

    protected void ddpParentCate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(strCateId) 
            && this.ddpParentCate.SelectedValue == model.goods.CategoryImpl.GetModelByKey(Convert.ToInt32(strCateId)).ParentId.ToString())
        {
            this.txtOrderId.Text = model.goods.CategoryImpl.GetModelByKey(Convert.ToInt32(strCateId)).OrderId.ToString();
        }
        else
        {
            // 新栏目，每次都找最大的
            int order = Convert.ToInt32(OR.SQLHelper.GetSingle("Select ISNULL(Max(OrderId),0)+1 OrderId From Goods_Category Where ParentId=@pid",
                new System.Data.SqlClient.SqlParameter("@pid", Convert.ToInt32(this.ddpParentCate.SelectedValue))));
            this.txtOrderId.Text = order.ToString();
        }

    }
}