﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_ItemEdit : System.Web.UI.Page
{
    protected String strCategoryId = String.Empty;
    protected String strItemId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strCategoryId = Request.QueryString["CategoryId"];
        strItemId = Request.QueryString["ItemId"];

        this.InnerTabs1.AddTabs("基本信息", String.Format("~/Goods/ItemEditInfo.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));

        if (!String.IsNullOrEmpty(strItemId))
        {
            this.InnerTabs1.AddTabs("商品信息", String.Format("~/Goods/ItemEditExt.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
            this.InnerTabs1.AddTabs("商品照片", String.Format("~/Goods/ItemEditPicture.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
            this.InnerTabs1.AddTabs("商品价格", String.Format("~/Goods/ItemEditPrice.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
        }

        this.InnerTabs1.SetSelected(0);

        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(strItemId))
            {
                LoadItemInfo();
            }
        }
    }

    protected void LoadItemInfo()
    {
        model.goods.ItemInfoFull item = OR.DAL.GetModelByKey<model.goods.ItemInfoFull>(Convert.ToInt32(strItemId));

        if (item == null)
        {
            return;
        }

        this.txtBrandName.Text = item.BrandName;
        this.txtDisplayTitle.Text = item.DisplayTitle;
        this.txtItemName.Text = item.ItemName;
        this.txtMFR.Text = item.MFR;
        this.txtModel.Text = item.ItemModel;
        this.txtRMK.Text = item.RMK;
        this.txtSubTitle.Text = item.SubTitle;
        this.chkStatus.Checked = (item.Status == (int)model.Status.Normal ? true : false);
        this.txtDescript.Text = item.Description;

        this.lblBrandName.Text = item.BrandName;
        this.lblDisplayTitle.Text = item.DisplayTitle;
        this.lblItemName.Text = item.ItemName;
        this.lblMFR.Text = item.MFR;
        this.lblModel.Text = item.ItemModel;
        this.lblRMK.Text = item.RMK;
        this.lblSubTitle.Text = item.SubTitle;
        this.lblStatus.Text = EnumUtils.GetDescription((model.Status)item.Status);
        this.lblDescript.Text = item.Description;

        this.lblMarketPrice.Text = item.MarketPrice.ToString() + " 元";
        this.lblRetailPrice.Text = item.RetailPrice.ToString() + " 元";
        this.lblGroupOnPrice.Text = item.GroupOnPrice.ToString() + " 元";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        model.goods.ItemInfoFull item = null;

        if (!String.IsNullOrEmpty(strItemId))
        {
            item = OR.DAL.GetModelByKey<model.goods.ItemInfoFull>(Convert.ToInt32(strItemId));
        }

        if (String.IsNullOrEmpty(strItemId) || item == null)
        {
            item = new model.goods.ItemInfoFull();

            item.BrandName = this.txtBrandName.Text;
            item.DisplayTitle = this.txtDisplayTitle.Text;
            item.ItemName = this.txtItemName.Text;
            item.MFR = this.txtMFR.Text;
            item.ItemModel = this.txtModel.Text;
            item.RMK = this.txtRMK.Text;
            item.SubTitle = this.txtSubTitle.Text;
            item.Status = this.chkStatus.Checked ? 1 : 0;
            item.CategoryId = Convert.ToInt32(strCategoryId);
            item.Description = this.txtDescript.Text;

            item.Created = DateTime.Now;
            item.Updated = DateTime.Now;

            item = OR.DAL.Add<model.goods.ItemInfoFull>(item, true);

            Common.Logger.info("添加商品：" + item.ToString(true));
        }
        else
        {
            item.BrandName = this.txtBrandName.Text;
            item.DisplayTitle = this.txtDisplayTitle.Text;
            item.ItemName = this.txtItemName.Text;
            item.MFR = this.txtMFR.Text;
            item.ItemModel = this.txtModel.Text;
            item.RMK = this.txtRMK.Text;
            item.SubTitle = this.txtSubTitle.Text;
            item.Status = this.chkStatus.Checked ? 1 : 0;
            item.Description = this.txtDescript.Text;

            item.Updated = DateTime.Now;

            OR.DAL.Update<model.goods.ItemInfoFull>(item);

            Common.Logger.info("更新商品：" + item.ToString(true));
        }

        Response.Redirect(String.Format("./ItemEditInfo.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
    }
}