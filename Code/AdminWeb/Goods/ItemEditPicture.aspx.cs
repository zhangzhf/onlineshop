﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_ItemEditPicture : System.Web.UI.Page
{
    protected String strCategoryId = String.Empty;
    protected String strItemId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strCategoryId = Request.QueryString["CategoryId"];
        strItemId = Request.QueryString["ItemId"];

        this.InnerTabs1.AddTabs("基本信息", String.Format("~/Goods/ItemEditInfo.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));

        if (!String.IsNullOrEmpty(strItemId))
        {
            this.InnerTabs1.AddTabs("商品信息", String.Format("~/Goods/ItemEditExt.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
            this.InnerTabs1.AddTabs("商品照片", String.Format("~/Goods/ItemEditPicture.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
            this.InnerTabs1.AddTabs("商品价格", String.Format("~/Goods/ItemEditPrice.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
        }

        this.InnerTabs1.SetSelected(2);

        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(strItemId))
            {
                LoadItemInfo();
            }
        }
    }

    model.goods.ItemInfo item = null;

    protected void LoadItemInfo()
    {
        item = OR.DAL.GetModelByKey<model.goods.ItemInfo>(Convert.ToInt32(strItemId));

        List<model.res.FileInfo> files = model.res.FileInfoImpl.GetBizResFiles(model.res.BizModule.Goods_Photo, strItemId);
        this.Repeater1.DataSource = files;
        this.Repeater1.DataBind();
    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            model.res.FileInfo file = e.Item.DataItem as model.res.FileInfo;

            Image imgPhoto = e.Item.FindControl("imgPhoto") as Image;
            imgPhoto.ImageUrl = model.res.FileInfoImpl.GetPictureThumb(file, model.res.ThumbLevel.M);

            HyperLink linkPicture = e.Item.FindControl("linkPicture") as HyperLink;
            linkPicture.NavigateUrl = model.res.FileInfoImpl.GetFileURL(file);

            LinkButton btnDelete = e.Item.FindControl("btnDelete") as LinkButton;
            btnDelete.CommandName = "del";
            btnDelete.CommandArgument = file.FileId.ToString();

            LinkButton btnSetCover = e.Item.FindControl("btnSetCover") as LinkButton;

            if (item.SnapshotPicId == file.FileId)
            {
                btnSetCover.Visible = false;
            }
            else
            {
                btnSetCover.CommandName = "cover";
                btnSetCover.CommandArgument = file.FileId.ToString();
            }
        }
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            model.res.FileInfoImpl.Delete(Convert.ToInt32(e.CommandArgument.ToString()));
        }
        else if (e.CommandName == "cover")
        {
            model.goods.ItemInfo item = OR.DAL.GetModelByKey<model.goods.ItemInfo>(Convert.ToInt32(strItemId));

            item.SnapshotPicId = Convert.ToInt32(e.CommandArgument.ToString());
            item.Updated = DateTime.Now;
            OR.DAL.Update<model.goods.ItemInfo>(item);
        }

        LoadItemInfo();
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        String txtFileId = this.txtFileId.Text;

        List<model.res.FileInfo> result = model.res.FileInfoImpl.GetBizResFiles(model.res.BizModule.Goods_Photo, strItemId);

        if (result.Count > 0)
        {
            model.goods.ItemInfo item = OR.DAL.GetModelByKey<model.goods.ItemInfo>(Convert.ToInt32(strItemId));

            if (item.SnapshotPicId == 0)
            {
                item.SnapshotPicId = Convert.ToInt32(txtFileId);
                item.Updated = DateTime.Now;
                OR.DAL.Update<model.goods.ItemInfo>(item);
            }
        }

        LoadItemInfo();
    }
}