﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterPage.master" AutoEventWireup="true" CodeFile="ItemEditExt.aspx.cs" Inherits="Goods_ItemEditExt" %>

<%@ Register Src="../Common/InnerTabs.ascx" TagName="InnerTabs" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(document).ready(function ()
        {
            $(".write").hide();

            $("#btnEdit").click(function ()
            {
                $(".read").hide();
                $(".write").show();
            });

            $("#btnCancel").click(function ()
            {
                $(".write").hide();
                $(".read").show();
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <uc1:InnerTabs ID="InnerTabs1" runat="server" />

    <asp:Table ID="ItemBasicInfo" runat="server" CssClass="tableInfo width-p100">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>商品介绍</asp:TableHeaderCell>
            <asp:TableHeaderCell HorizontalAlign="Right">
                <input type="button" id="btnEdit" value="编辑" class="read" />

                <asp:Button ID="btnSubmit" runat="server" Text="提交" CssClass="write" OnClick="btnSubmit_Click" />
                <input type="button" id="btnCancel" value="取消" class="write" />

            </asp:TableHeaderCell>
        </asp:TableHeaderRow>
    </asp:Table>

    <asp:Table ID="ItemPackage" runat="server" CssClass="tableInfo width-p100">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell ColumnSpan="2">包装清单</asp:TableHeaderCell>
        </asp:TableHeaderRow>
    </asp:Table>

    <asp:Table ID="ItemServices" runat="server" CssClass="tableInfo width-p100">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell ColumnSpan="2">售后保障</asp:TableHeaderCell>
        </asp:TableHeaderRow>
    </asp:Table>

    <asp:Table ID="ItemSpecInfo" runat="server" CssClass="tableInfo width-p100">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell ColumnSpan="2">规格参数</asp:TableHeaderCell>
        </asp:TableHeaderRow>
    </asp:Table>

</asp:Content>

