﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_ItemEditExt : System.Web.UI.Page
{
    protected String strCategoryId = String.Empty;
    protected String strItemId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strCategoryId = Request.QueryString["CategoryId"];
        strItemId = Request.QueryString["ItemId"];

        this.InnerTabs1.AddTabs("基本信息", String.Format("~/Goods/ItemEditInfo.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));

        if (!String.IsNullOrEmpty(strItemId))
        {
            this.InnerTabs1.AddTabs("商品信息", String.Format("~/Goods/ItemEditExt.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
            this.InnerTabs1.AddTabs("商品照片", String.Format("~/Goods/ItemEditPicture.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
            this.InnerTabs1.AddTabs("商品价格", String.Format("~/Goods/ItemEditPrice.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
        }

        this.InnerTabs1.SetSelected(1);

        BuildCategoryInfoTable();

        if (!IsPostBack)
        {

        }
    }

    protected void BuildCategoryInfoTable()
    {
        List<model.goods.InfoKeys> infoKeys = model.goods.InfoKeysImpl.RetrieveCategoryInfoKeys(Convert.ToInt32(strCategoryId), true);

        foreach (model.goods.InfoKeys key in infoKeys)
        {
            AddTabInfoAttr(key);
        }
    }

    protected void AddTabInfoAttr(model.goods.InfoKeys infoKey)
    {
        model.goods.InfoTypeCode t = (model.goods.InfoTypeCode)Enum.Parse(typeof(model.goods.InfoTypeCode), infoKey.InfoType);

        Dictionary<String, model.goods.ItemKeyValue> values = model.goods.ItemKeyValueImpl.RetrieveItemKeyValue(Convert.ToInt32(strItemId));

        Table table = GetInfoTable(t);

        if (table != null)
        {
            TableCell cell1 = new TableCell();
            cell1.Text = infoKey.KeyName;

            TableCell cell2 = new TableCell();

            Label label = new Label();
            label.ID = String.Format("lbl_{0}_{1}", infoKey.InfoType, infoKey.InfoKey);
            label.CssClass = "read";

            TextBox text = new TextBox();
            text.ID = String.Format("txt_{0}_{1}", infoKey.InfoType, infoKey.InfoKey);
            text.CssClass = "write";

            if (values.ContainsKey(String.Format("{0}_{1}", infoKey.InfoType, infoKey.InfoKey)))
            {
                model.goods.ItemKeyValue value = values[String.Format("{0}_{1}", infoKey.InfoType, infoKey.InfoKey)];

                label.Text = value.InfoValue;
                text.Text = value.InfoValue;
            }

            cell2.Controls.Add(label);
            cell2.Controls.Add(text);

            TableRow row = new TableRow();
            row.Cells.Add(cell1);
            row.Cells.Add(cell2);

            table.Rows.Add(row);
        }
    }

    protected Table GetInfoTable(model.goods.InfoTypeCode code)
    {
        switch (code)
        {
            case model.goods.InfoTypeCode.BasicInfo:
                return this.ItemBasicInfo;
            case model.goods.InfoTypeCode.SpecInfo:
                return this.ItemSpecInfo;
            case model.goods.InfoTypeCode.Package:
                return this.ItemPackage;
            case model.goods.InfoTypeCode.Services:
                return this.ItemServices;
            default:
                return null;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SaveTableInfo(model.goods.InfoTypeCode.BasicInfo);
        SaveTableInfo(model.goods.InfoTypeCode.Package);
        SaveTableInfo(model.goods.InfoTypeCode.Services);
        SaveTableInfo(model.goods.InfoTypeCode.SpecInfo);

        Response.Redirect(String.Format("./ItemEditExt.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
    }

    protected void SaveTableInfo(model.goods.InfoTypeCode code)
    {
        List<model.goods.InfoKeys> infoKeys = model.goods.InfoKeysImpl.RetrieveCategoryInfoKeys(Convert.ToInt32(strCategoryId), true);

        Table table = GetInfoTable(code);

        foreach (model.goods.InfoKeys infoKey in infoKeys)
        {
            String id = String.Format("txt_{0}_{1}", infoKey.InfoType, infoKey.InfoKey);

            TextBox txtInput = table.FindControl(id) as TextBox;

            if (txtInput == null)
            {
                continue;
            }

            model.goods.ItemKeyValue value = OR.DAL.GetModel<model.goods.ItemKeyValue>("ItemId=@id And InfoType=@type And InfoKey=@key",
                OR.Param.NamedParam("@id", Convert.ToInt32(strItemId)),
                OR.Param.NamedParam("@type", infoKey.InfoType.ToString()),
                OR.Param.NamedParam("@key", infoKey.InfoKey));

            if (value == null)
            {
                value = new model.goods.ItemKeyValue();
                value.Created = DateTime.Now;
                value.InfoKey = infoKey.InfoKey;
                value.InfoType = infoKey.InfoType;
                value.InfoValue = txtInput.Text;
                value.ItemId = Convert.ToInt32(strItemId);
                value.Status = (int)model.Status.Normal;
                value.Updated = DateTime.Now;

                value = OR.DAL.Add<model.goods.ItemKeyValue>(value, true);
            }
            else
            {
                value.InfoValue = txtInput.Text;
                value.Status = (int)model.Status.Normal;
                value.Updated = DateTime.Now;

                OR.DAL.Update<model.goods.ItemKeyValue>(value);
            }
        }
    }
}