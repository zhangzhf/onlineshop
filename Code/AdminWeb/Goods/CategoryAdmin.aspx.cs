﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_CategoryAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetList();
        }
    }

    protected void GetList()
    {
        List<model.goods.Category> categories = CategoryUtils.GetCategory();

        this.GridView1.DataSource = categories;
        this.GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            model.goods.Category cate = e.Row.DataItem as model.goods.Category;

            HyperLink hyEditCate = e.Row.FindControl("hyEditCate") as HyperLink;
            hyEditCate.NavigateUrl = String.Format("javascript:EditCate('{0}')", cate.CategoryId.ToString());

            if (cate.ParentId == 0)
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(0, 163, 207);
                e.Row.ForeColor = System.Drawing.Color.White;
            }
            else
            {
                e.Row.Cells[1].Style.Add(HtmlTextWriterStyle.PaddingLeft, "20px");
                e.Row.BackColor = System.Drawing.Color.White;
            }
        }
    }
}