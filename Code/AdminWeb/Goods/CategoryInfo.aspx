﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterPage.master" AutoEventWireup="true" CodeFile="CategoryInfo.aspx.cs" Inherits="Goods_CategoryInfo" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function ()
        {
            $("#btnAddItem").click(function ()
            {
                window.location.href = "./ItemEditInfo.aspx?CategoryId=<%=strCategoryId%>";
            });

        });

        function EditItem(itemId)
        {
            window.location.href = "./ItemEditInfo.aspx?CategoryId=<%=strCategoryId%>&ItemId=" + itemId;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
    分类商品管理
    <asp:Label ID="txtCurrCategory" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">

    <input type="button" id="btnAddItem" value="添加商品" />
    &nbsp;&nbsp;
    <asp:Button ID="btnPrivateAttrAdmin" runat="server" Text="私有属性管理" OnClick="btnPrivateAttrAdmin_Click" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
        <HeaderTemplate>
            <table class="gvTable" width="100%">
                <colgroup>
                    <col width="80px" />
                    <col width="100px" />
                    <col />
                    <col />
                    <col width="200px" />
                    <col width="100px" />
                </colgroup>
                <tbody>
                    <tr>
                        <th>商品编号</th>
                        <th>照片</th>
                        <th>产品信息</th>
                        <th>价格信息</th>
                        <th>产品状态</th>
                        <th>库存</th>
                    </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td align="center">
                    <%#Eval("ItemId")  %>
                </td>
                <td align="center">
                    <asp:Image ID="Image1" runat="server" />
                </td>
                <td>
                    <table>
                        <tr>
                            <td>商品名称</td>
                            <td>
                                <a href="./ItemEditInfo.aspx?CategoryId=<%#Eval("CategoryId") %>&ItemId=<%#Eval("ItemId") %>"><%#Eval("ItemName") %></a>
                            </td>
                        </tr>
                        <tr>
                            <td>厂家</td>
                            <td>
                                <%#Eval("MFR")  %> - <%#Eval("BrandName")  %>
                            </td>
                        </tr>
                        <tr>
                            <td>规格</td>
                            <td>
                                <%#Eval("ItemModel") %>
                            </td>
                        </tr>

                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>市场价格</td>
                            <td>
                                <%# Eval("MarketPrice") %> 元
                            </td>
                        </tr>
                        <tr>
                            <td>零售价格</td>
                            <td>
                                <%# Eval("RetailPrice") %> 元
                            </td>
                        </tr>
                        <tr>
                            <td>团购价格</td>
                            <td>
                                <%# Eval("GroupOnPrice") %> 元
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="center">
                    <table>
                        <tr>
                            <td>销售状态</td>
                            <td>
                                <%# EnumUtils.GetDescription(  (model.Status) Convert.ToInt32( Eval("Status").ToString())) %>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="center">N/A
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <div class="divPager">
        <webdiyer:AspNetPager ID="AspNetPager" runat="server" OnPageChanged="AspNetPager_PageChanged">
        </webdiyer:AspNetPager>
    </div>

</asp:Content>

