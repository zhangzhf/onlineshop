﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterPage.master" AutoEventWireup="true" CodeFile="ItemEditPicture.aspx.cs" Inherits="Goods_ItemEditPicture" %>

<%@ Register Src="../Common/InnerTabs.ascx" TagName="InnerTabs" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/colorbox.css" rel="stylesheet" />
    <script src="../js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function ()
        {
            $("#btnUploadPic").click(function ()
            {
                CommonUploadFile("上传图片", "CommonUploadFile_Callback", "<%= model.res.BizModule.Goods_Photo.ToString() %>", "<%=strItemId%>");
            });

            $(".hrefPics").colorbox({ maxWidth: "90%", maxHeight: "90%", rel: 'hrefPics' });
        });

        function CloseDialog()
        {
            $('#dialog').dialog('close');
        }

        function CommonUploadFile_Callback(ret)
        {
            $("#<%=txtFileId.ClientID%>").val(ret.FileId);
            $("#<%=btnRefresh.ClientID%>").click();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
    商品图片管理
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
    <input id="btnUploadPic" type="button" value="上传图片" /><br />
    <asp:TextBox ID="txtFileId" runat="server" CssClass="nodisplay"></asp:TextBox>
    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" CssClass="nodisplay" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:InnerTabs ID="InnerTabs1" runat="server" />

    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
        <HeaderTemplate>
            <ul class="itemPictures">
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <div class="pic">
                    <asp:HyperLink ID="linkPicture" runat="server" CssClass="hrefPics">
                        <asp:Image ID="imgPhoto" runat="server" Visible="true" />
                    </asp:HyperLink>
                </div>
                <div class="command">
                    <asp:LinkButton ID="btnSetCover" runat="server">设为封面</asp:LinkButton>
                    <asp:LinkButton ID="btnDelete" runat="server" OnClientClick="javascript:return confirm('确认删除当前所选图片？')">删除</asp:LinkButton>
                </div>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src=""></iframe>
    </div>
</asp:Content>

