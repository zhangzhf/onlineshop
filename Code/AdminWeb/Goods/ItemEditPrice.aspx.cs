﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_ItemEditPrice : System.Web.UI.Page
{
    protected String strCategoryId = String.Empty;
    protected String strItemId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strCategoryId = Request.QueryString["CategoryId"];
        strItemId = Request.QueryString["ItemId"];

        this.InnerTabs1.AddTabs("基本信息", String.Format("~/Goods/ItemEditInfo.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));

        if (!String.IsNullOrEmpty(strItemId))
        {
            this.InnerTabs1.AddTabs("商品信息", String.Format("~/Goods/ItemEditExt.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
            this.InnerTabs1.AddTabs("商品照片", String.Format("~/Goods/ItemEditPicture.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
            this.InnerTabs1.AddTabs("商品价格", String.Format("~/Goods/ItemEditPrice.aspx?CategoryId={0}&ItemId={1}", strCategoryId, strItemId));
        }

        this.InnerTabs1.SetSelected(3);

        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(strItemId))
            {
                LoadItemInfo();
            }
        }
    }

    protected void LoadItemInfo()
    {
        List<model.goods.ItemPriceTrace> prices = model.goods.ItemPriceTraceImpl.GetItemPriceHistory(Convert.ToInt32(strItemId), false);

        this.GridView1.DataSource = prices;
        this.GridView1.DataBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            model.goods.ItemPriceTrace price = e.Row.DataItem as model.goods.ItemPriceTrace;

            Label txtStartTime = e.Row.FindControl("txtStartTime") as Label;
            if (price.Status >= (int)model.goods.PriceStatus.Active)
            {
                txtStartTime.Text = price.EffectiveStartTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                txtStartTime.Text = "-";
            }

            Label txtEndTime = e.Row.FindControl("txtEndTime") as Label;
            if (price.Status >= (int)model.goods.PriceStatus.Expired)
            {
                txtEndTime.Text = price.EffectiveEndTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                txtEndTime.Text = "-";
            }

            Label txtPriceMarket = e.Row.FindControl("txtPriceMarket") as Label;
            txtPriceMarket.Text = price.MarketPrice.ToString();

            Label txtPriceRetail = e.Row.FindControl("txtPriceRetail") as Label;
            txtPriceRetail.Text = price.RetailPrice.ToString();

            Label txtPriceGroupOn = e.Row.FindControl("txtPriceGroupOn") as Label;
            txtPriceGroupOn.Text = price.GroupOnPrice.ToString();

            LinkButton btnSetActive = e.Row.FindControl("btnSetActive") as LinkButton;
            LinkButton btnDelete = e.Row.FindControl("btnDelete") as LinkButton;

            if (price.Status == (int)model.goods.PriceStatus.Inactive)
            {
                btnSetActive.Visible = true;
                btnDelete.Visible = true;

                btnSetActive.CommandName = "Act";
                btnSetActive.CommandArgument = price.TraceId.ToString();

                btnDelete.CommandName = "Del";
                btnDelete.CommandArgument = price.TraceId.ToString();
            }
            else
            {
                btnSetActive.Visible = false;
                btnDelete.Visible = false;
            }

        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Act")
        {
            model.goods.ItemPriceTrace price = OR.DAL.GetModelByKey<model.goods.ItemPriceTrace>(Convert.ToInt32(e.CommandArgument.ToString()));
            model.goods.ItemInfo item = OR.DAL.GetModelByKey<model.goods.ItemInfo>(Convert.ToInt32(strItemId));

            // 将产品当前的价格改为正式的状态
            item.RetailPrice = price.RetailPrice;
            item.MarketPrice = price.MarketPrice;
            item.GroupOnPrice = price.GroupOnPrice;
            item.Updated = DateTime.Now;
            OR.DAL.Update<model.goods.ItemInfo>(item);

            // 将前一个发布状态的改为过期状态
            model.goods.ItemPriceTrace pprice = OR.DAL.GetModel<model.goods.ItemPriceTrace>("ItemId=@id And Status=@status",
                OR.Param.NamedParam("@id", Convert.ToInt32(strItemId)),
                OR.Param.NamedParam("@status", (int)model.goods.PriceStatus.Active));

            if (pprice != null)
            {
                pprice.Status = (int)model.goods.PriceStatus.Expired;
                pprice.EffectiveEndTime = DateTime.Now;
                pprice.Updated = DateTime.Now;
                OR.DAL.Update<model.goods.ItemPriceTrace>(pprice);
            }

            //将当前的价格发布为正式状态
            price.Status = (int)model.goods.PriceStatus.Active;
            price.EffectiveStartTime = DateTime.Now;
            price.Updated = DateTime.Now;
            OR.DAL.Update<model.goods.ItemPriceTrace>(price);

            Common.Logger.info("发布价格: " + price.ToString());

        }
        else if (e.CommandName == "Del")
        {
            model.goods.ItemPriceTrace price = OR.DAL.GetModelByKey<model.goods.ItemPriceTrace>(Convert.ToInt32(e.CommandArgument.ToString()));
            price.Status = (int)model.goods.PriceStatus.Delete;
            price.Updated = DateTime.Now;

            OR.DAL.Update<model.goods.ItemPriceTrace>(price);

            Common.Logger.info("逻辑删除价格记录: " + price.ToString());
        }

        LoadItemInfo();
    }
}