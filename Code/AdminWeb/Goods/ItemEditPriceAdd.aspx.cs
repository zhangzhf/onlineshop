﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_ItemEditPriceAdd : System.Web.UI.Page
{
    private String strItemId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strItemId = Request.QueryString["ItemId"];

        if (!IsPostBack)
        {
            LoadCurrentPrice();
        }
    }

    protected void LoadCurrentPrice()
    {
        model.goods.ItemInfo item = OR.DAL.GetModelByKey<model.goods.ItemInfo>(Convert.ToInt32(strItemId));
        this.txtPriceMarket.Text = item.MarketPrice.ToString();
        this.txtPriceRetail.Text = item.RetailPrice.ToString();
        this.txtPriceGroupOn.Text = item.GroupOnPrice.ToString();
    }

    /// <summary>
    /// 这里在一起处理了插入和更新的代码。通过判断是否有传入参数来区分两者的状态
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        model.goods.ItemPriceTrace price = new model.goods.ItemPriceTrace();
        price.Created = DateTime.Now;
        price.EffectiveEndTime = DateTime.Now;
        price.EffectiveStartTime = DateTime.Now;
        price.GroupOnPrice = Convert.ToDouble(this.txtPriceGroupOn.Text);
        price.ItemId = Convert.ToInt32(strItemId);

        price.MarketPrice = Convert.ToDouble(this.txtPriceMarket.Text);
        price.RetailPrice = Convert.ToDouble(this.txtPriceRetail.Text);
        price.GroupOnPrice = Convert.ToDouble(this.txtPriceGroupOn.Text);

        price.Status = (int)model.goods.PriceStatus.Inactive;
        price.Updated = DateTime.Now;

        price = OR.DAL.Add<model.goods.ItemPriceTrace>(price, true);

        Common.Logger.info("添加新的价格记录: " + price.ToString());

        Common.Utils.ResponseCloseDialogScript();
    }
}