﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Goods_CategoryInfo : System.Web.UI.Page
{
    protected String strCategoryId = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        strCategoryId = Request.QueryString["CategoryId"];

        if (!IsPostBack)
        {
            this.txtCurrCategory.Text = String.Format("[{0} -> {1}]",
                model.goods.CategoryImpl.GetModelByKey(model.goods.CategoryImpl.GetModelByKey(Convert.ToInt32(strCategoryId)).ParentId).CategoryName,
                model.goods.CategoryImpl.GetModelByKey(Convert.ToInt32(strCategoryId)).CategoryName);

            ViewState["_SQL"] = String.Format(" Status<>{0} And CategoryId={1} Order by Created", ((int)model.Status.Delete).ToString(), strCategoryId);

            LoadItemList();
        }
    }

    protected void btnPrivateAttrAdmin_Click(object sender, EventArgs e)
    {
        Response.Redirect("./CategoryAttrAdmin.aspx?CategoryId=" + strCategoryId);
        Response.End();
    }

    protected void LoadItemList()
    {
        int _RowCount = 0;

        List<model.goods.ItemInfo> items = OR.DAL.GetModelList<model.goods.ItemInfo>(ViewState["_SQL"].ToString(), this.AspNetPager.CurrentPageIndex - 1, ref _RowCount);

        this.AspNetPager.RecordCount = _RowCount;

        this.Repeater1.DataSource = items;
        this.Repeater1.DataBind();
    }

    protected void AspNetPager_PageChanged(object sender, EventArgs e)
    {
        LoadItemList();
    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            model.goods.ItemInfo item = e.Item.DataItem as model.goods.ItemInfo;

            Image image = e.Item.FindControl("Image1") as Image;

            if (item.SnapshotPicId != 0)
            {
                model.res.FileInfo file = OR.DAL.GetModelByKey<model.res.FileInfo>(item.SnapshotPicId);

                image.ImageUrl = model.res.FileInfoImpl.GetPictureThumb(file, model.res.ThumbLevel.S);
            }
            else
            {
                image.ImageUrl = "~/images/blank_S.jpg";
            }
        }

    }
}