﻿/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://cksource.com/ckfinder/license
*/

CKFinder.customConfig = function (config)
{
    var date = new Date();
    var y = date.getFullYear();
    var m = ("" + (101 + date.getMonth())).substr(1, 2);
    var d = ("" + (100 + date.getDate())).substr(1, 2);

    config.startupPath = "Images:/" + y + "/" + m + "/" + d + "/";
    config.startupFolderExpanded = true;
};
