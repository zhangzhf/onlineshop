﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Common_UploadFile : System.Web.UI.Page
{
    String callback = String.Empty;

    String bizModule = String.Empty;
    String bizKey = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        callback = Request.QueryString["callback"];
        bizModule = Request.QueryString["BizModule"];
        bizKey = Request.QueryString["BizKey"];

        if (!IsPostBack)
        {

        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!this.FileUpload1.HasFile)
        {
            return;
        }

        // ~/upload/common/2014/07/16
        String folder = System.Web.HttpContext.Current.Server.MapPath("~/Upload/Common/" + DateTime.Now.ToString("yyyy/MM/dd/"));

        if (!System.IO.Directory.Exists(folder))
        {
            System.IO.Directory.CreateDirectory(folder);
        }

        // filename.ext
        String strFileName = this.FileUpload1.FileName.Replace("\\", "/");
        if (strFileName.IndexOf("/") != -1)
        {
            strFileName = strFileName.Substring(strFileName.LastIndexOf("/") + 1);
        }

        // guid
        String guid = Guid.NewGuid().ToString();

        // ext
        String strExtName = String.Empty;
        if (strFileName.LastIndexOf(".") != -1)
        {
            strExtName = strFileName.Substring(strFileName.LastIndexOf(".") + 1).ToUpper();
        }

        // guid.ext
        String strDiskFilename = guid + (String.IsNullOrEmpty(strExtName) ? "" : "." + strExtName);

        // folder/guid.ext
        String strFullFilePathName = System.IO.Path.Combine(folder, strDiskFilename);
        // 将其保存到磁盘上
        this.FileUpload1.SaveAs(strFullFilePathName);

        // 开始生成缩略图
        int width = 0;
        int height = 0;

        if (".jpg.bmp.png.gif".IndexOf(strExtName.ToLower()) >= 0)
        {
            System.Drawing.Image imgsrc = System.Drawing.Image.FromFile(strFullFilePathName);

            MakeThumbnail(imgsrc, folder + "\\" + guid + "_" + model.res.ThumbLevel.S.ToString() + ".jpg", 50, 50);
            MakeThumbnail(imgsrc, folder + "\\" + guid + "_" + model.res.ThumbLevel.M.ToString() + ".jpg", 350, 350);
            MakeThumbnail(imgsrc, folder + "\\" + guid + "_" + model.res.ThumbLevel.B.ToString() + ".jpg", 1000, 1000);

            width = imgsrc.Width;
            height = imgsrc.Height;
        }

        // 保存到数据库
        model.res.FileInfo file = new model.res.FileInfo();

        file.BizModule = bizModule;
        file.BizKey = bizKey;
        file.BizOrder = 0;
        file.IsCached = 0;

        file.Title = this.txtTitle.Text;
        file.Comment = this.txtMemo.Text;

        file.FileName = strFileName;
        file.FileGUID = guid;
        file.FileExt = strExtName;
        file.Folder = DateTime.Now.ToString("yyyy/MM/dd");
        file.FileSize = this.FileUpload1.FileContent.Length;
        file.UploadTime = DateTime.Now;

        file.Height = height;
        file.Width = width;

        file = OR.DAL.Add<model.res.FileInfo>(file, true);

        String result = Newtonsoft.Json.JsonConvert.SerializeObject(file);

        Common.Utils.ResponseParentCallback(callback, result);
    }

    /// <summary>
    /// 生成缩略图.每个照片需要生成两幅缩略图，一个是图标型的，120*90，一个是中等尺寸的，用于相册浏览400*300，再保留一个原图，用于浏览看大图
    /// </summary>
    /// <param name="originalImage"></param>
    /// <param name="fileName"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    public static void MakeThumbnail(System.Drawing.Image originalImage, String fileName, int width, int height)
    {

        int towidth = width;
        int toheight = height;

        if (originalImage.Width > width || originalImage.Height > height)
        {
            // 宽的比例比较大，按照宽的比例来处理图片
            if ((double)originalImage.Width / (double)originalImage.Height > (double)width / (double)height)
            {
                towidth = width;
                toheight = originalImage.Height * width / originalImage.Width;
            }
            else
            {
                towidth = originalImage.Width * height / originalImage.Height;
                toheight = height;
            }
        }

        //新建一个bmp图片
        System.Drawing.Image bitmap = new System.Drawing.Bitmap(width, height);

        //新建一个画板
        Graphics g = System.Drawing.Graphics.FromImage(bitmap);

        //设置高质量插值法
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

        //设置高质量,低速度呈现平滑程度
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

        //清空画布并以白色背景色填充
        g.Clear(Color.White);

        //在指定位置并且按指定大小绘制原图片的指定部分
        g.DrawImage(originalImage, new Rectangle((width - towidth) / 2, (height - toheight) / 2, towidth, toheight),
            new Rectangle(0, 0, originalImage.Width, originalImage.Height),
            GraphicsUnit.Pixel);

        try
        {
            //以jpg格式保存缩略图
            bitmap.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg);
        }
        catch (System.Exception e)
        {
            throw e;
        }
        finally
        {
            bitmap.Dispose();
            g.Dispose();
        }
    }
}