﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Common_InnerTabs : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void AddTabs(String text, String url)
    {
        HyperLink link = new HyperLink();
        link.Text = text;
        link.NavigateUrl = url;

        TableCell cell = new TableCell();
        cell.Controls.Add(link);

        TableCell cellSp = new TableCell();

        this.tableTabs.Rows[0].Cells.Add(cell);
        this.tableTabs.Rows[0].Cells.Add(cellSp);
    }

    public void SetSelected(Int32 index)
    {
        this.tableTabs.Rows[0].Cells[1 + index * 2].CssClass = "selected";
    }
}