﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>管理员登录</title>
    <link href="css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%; height: 100%;">
        <div id="loginPanel">
            <asp:Login ID="LoginControl" runat="server" OnAuthenticate="Login_Authenticate" OnLoggedIn="Login_LoggedIn"
                LoginButtonText="登录" PasswordLabelText="密码：" UserNameLabelText="用户名：" RememberMeText="下次记住我"
                RenderOuterTable="False">
            </asp:Login>
        </div>
    </div>
    </form>
</body>
</html>
