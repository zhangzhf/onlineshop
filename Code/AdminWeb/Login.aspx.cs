﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.LoginControl.TitleText = String.Format("LOGIN &nbsp;<span style='font-size: 18px;'>{0}</span>", Common.Utils.GetBrandname());

            String ReturnUrl = Request.QueryString["ReturnUrl"] ?? String.Empty;

            if (!String.IsNullOrEmpty(ReturnUrl))
            {
                if (ReturnUrl.EndsWith("/") || ReturnUrl.Equals(Request.ApplicationPath))
                {
                    Response.Redirect("~/Admin/Default.aspx");
                    return;
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
                return;
            }
        }
    }

    protected void Login_Authenticate(object sender, AuthenticateEventArgs e)
    {
        String userAccount = this.LoginControl.UserName;
        String userPassword = this.LoginControl.Password;

        user = OR.DAL.GetModel<model.member.UserInfo>("UserAccount=@UserAccount AND Status=@status",
            new System.Data.SqlClient.SqlParameter("@UserAccount", userAccount),
            new System.Data.SqlClient.SqlParameter("@status", model.Status.Normal));

        if (user != null)
        {
            String strPassword = Common.Utils.to_md5(userPassword);

            if (strPassword.Equals(user.Password))
            {
                Common.Logger.info("用户登录", user);

                e.Authenticated = true;
                return;
            }
        }

        e.Authenticated = false;
        return;
    }

    model.member.UserInfo user = null;

    protected void Login_LoggedIn(object sender, EventArgs e)
    {
        if (user != null)
        {
            System.Web.Security.FormsAuthentication.RedirectFromLoginPage(user.UserId.ToString(), false);
        }
    }
}