﻿<%@ page language="C#" autoeventwireup="true" inherits="sysadmin_AddressEdit, PortalWeb" enableviewstate="false" theme="Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../css/layout.css" rel="stylesheet" />
    <script src="../js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <style type="text/css">
        html {
            overflow: auto;
        }

        .form-btn {
            width: 100%;
            text-align: right;
            height: 40px;
            padding-top: 20px;
        }

        table.tableAddress {
            width: 100%;
            line-height: 24px;
        }

            table.tableAddress tr {
            }

                table.tableAddress tr td {
                    padding: 5px;
                    color: #666;
                    font-size: 13px;
                }

                    table.tableAddress tr td input[type='text'] {
                        border: solid 1px gray;
                        height: 20px;
                    }

                    table.tableAddress tr td em {
                        color: #ff6a00;
                    }

            table.tableAddress > tr > td:first-child {
                text-align: right;
            }

        .form-tip {
            color: #999;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <table class="tableAddress">
            <tr>
                <td><em>*</em> 收货人：</td>
                <td colspan="3">
                    <asp:TextBox ID="txtRecipent" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td><em>*</em> 所在地区：</td>
                <td colspan="3">
                    <asp:DropDownList ID="ddpCity" runat="server">
                        <asp:ListItem Text="北京" Value="110000"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><em>*</em> 详细地址：</td>
                <td colspan="3">
                    <asp:TextBox ID="txtAddress" runat="server" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td><em>*</em> 手机号码：</td>
                <td>
                    <asp:TextBox ID="txtCellPhone" runat="server"></asp:TextBox>
                    <em>或</em>
                </td>
                <td>固定电话：</td>
                <td>
                    <asp:TextBox ID="txtLandlinePhone" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td><em>*</em> 邮箱：</td>
                <td colspan="3">
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    <span class="form-tip">用来接收订单提醒邮件，便于您及时了解订单状态</span>
                </td>
            </tr>

        </table>

        <div class="form-btn">
            <a href="./AddressSelect.aspx" class="btn secondary">返回收货人列表</a>
            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn primary" OnClick="btnSave_Click">保存收货人信息</asp:LinkButton>
        </div>
    </form>
</body>
</html>
