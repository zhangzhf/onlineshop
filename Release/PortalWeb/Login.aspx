﻿<%@ page language="C#" autoeventwireup="true" inherits="Login, PortalWeb" enableviewstate="false" theme="Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="css/login.css" rel="stylesheet" />
    <script type="text/javascript">
        var globalSetting = {
            base: '<%=Common.Utils.ApplicationBase() %>'
        };

        function gotoHome()
        {
            window.location.href = globalSetting.base;
        }
    </script>
</head>
<body>
    <div class="w">
        <div id="logo">
            <a href="javascript:gotoHome();">
                <img width="270" height="60" alt="<%= Common.Utils.GetMallname() %>" src="./images/logo.png" /></a>
            <b></b>
        </div>
    </div>
    <form id="form1" runat="server">
        <div class="w1" id="entry">
            <div class="mc " id="bgDiv">
                <div id="entry-bg" style="width: 511px; height: 455px;">
                </div>
                <div class="form ">
                    <div class="item fore1">
                        <span>帐号</span>
                        <div class="item-ifo">
                            <asp:TextBox ID="nloginname" runat="server" CssClass="text" autocomplete="off"></asp:TextBox>
                        </div>
                    </div>

                    <div class="item fore2">
                        <span>密码</span>
                        <div class="item-ifo">
                            <asp:TextBox ID="nloginpwd" runat="server" CssClass="text" autocomplete="off" TextMode="Password"></asp:TextBox>
                        </div>
                    </div>

                    <div class="item fore3" id="o-authcode">
                        <span>验证码</span>
                        <div class="item-ifo">
                            <asp:TextBox ID="authcode" runat="server" CssClass="text text-1" Style="ime-mode: disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="item"></div>
                    <div class="item login-btn2013">
                        <asp:Button ID="loginsubmit" runat="server" CssClass="btn-img btn-entry" Text="登录" OnClick="loginsubmit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
