﻿<%@ page language="C#" autoeventwireup="true" inherits="space_PreCheck, PortalWeb" enableviewstate="false" theme="Default" %>

<%@ Register Src="~/Common/TopHeader.ascx" TagPrefix="uc1" TagName="TopHeader" %>
<%@ Register Src="~/Common/PageFooter.ascx" TagPrefix="uc1" TagName="PageFooter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>我的购物车</title>
    <link href="../css/layout.css" rel="stylesheet" />

    <script type="text/javascript">
        var globalSetting = {
            base: '<%=Common.Utils.ApplicationBase() %>'
        };
    </script>
    <script src="../js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="../js/Common.js"></script>
    <script type="text/javascript">

        $(document).ready(function ()
        {
            InitPopMenu(false);
        });

        function deleteCart(id, author)
        {
            if (confirm('确定从购物车中删除此商品？'))
            {

            }
        }

        function goToOrder()
        {
            window.location.href = "./Check.aspx";
        }

        function gotoHome()
        {
            window.location.href = globalSetting.base;
        }
    </script>
</head>
<body>

    <uc1:TopHeader runat="server" ID="TopHeader" />

    <div class="logo">
        <a href="javascript:gotoHome();">
            <div class="logoPic"></div>
        </a>
        <ul class="stepWizard step1">
            <li class="over"><b></b>1.我的购物车</li>
            <li class="none"><b></b>2.填写核对订单信息</li>
            <li class="none"><b></b>3.成功提交订单</li>
        </ul>
    </div>

    <div class="cartList">
        <h3>我的购物车</h3>
        <table class="cartTable">
            <colgroup>
                <col style="width: 100px" />
                <col style="width: 50px" />
                <col />
                <col style="width: 150px" />
                <col style="width: 120px" />
                <col style="width: 100px" />
            </colgroup>
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" />全选</th>
                    <th></th>
                    <th>商品</th>
                    <th>零售价</th>
                    <th>数量</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <input type="checkbox" />
                            </td>
                            <td>
                                <img src="<%# Common.Utils.ResolveUrl(model.res.FileInfoImpl.GetPictureThumb(model.res.FileInfoImpl.GetModelByKey(Convert.ToInt32(Eval("SnapshotPicId").ToString())), model.res.ThumbLevel.S)) %>" alt="" />
                            </td>
                            <td style="text-align: left">
                                <%# Eval("DisplayTitle") %>
                            </td>
                            <td>¥<%# Eval("RetailPrice") %>
                            </td>
                            <td class="num">
                                <div class="des"></div>
                                <div class="in">
                                    <input type="text" class="text" value="<%# Eval("ItemCnt") %>" id="pCnt" />
                                </div>
                                <div class="plug"></div>
                            </td>
                            <td>
                                <a href="javascript:deleteCart('<%# Eval("ItemId") %>', this)">删除</a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>

        <div class="cart-button clearfix">
            <a class="btn continue" href="../index.aspx" id="continue"><span class="btn-text">继续购物</span></a>
            <a class="checkout" href="javascript:goToOrder();" id="toSettlement">去结算<b></b></a>
        </div>

    </div>
    <uc1:PageFooter runat="server" ID="PageFooter" />

</body>
</html>
