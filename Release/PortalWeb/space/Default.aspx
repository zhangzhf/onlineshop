﻿<%@ page title="" language="C#" masterpagefile="~/space/MasterPage.master" autoeventwireup="true" inherits="space_Default, PortalWeb" enableviewstate="false" theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table class="OrderTable">
        <thead>
            <tr>
                <th>订单信息</th>
                <th style="width: 80px">收货人</th>
                <th style="width: 110px">应付金额</th>
                <th style="width: 100px">订单时间</th>
                <th style="width: 100px">订单状态</th>
                <th style="width: 128px">操作</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                <ItemTemplate>
                    <tr class="i">
                        <td colspan="7">订单编号：<%# Eval("OrderId") %>
                        </td>
                    </tr>
                    <tr class="c">
                        <td>
                            <asp:Literal ID="ltPictures" runat="server"></asp:Literal>
                        </td>
                        <td></td>
                        <td>￥<%#Eval("TotalPrice") %>
                        </td>
                        <td>
                            <%# Convert.ToDateTime(Eval("Created").ToString()).ToString("yyyy-MM-dd<br />HH:mm:ss") %>
                        </td>
                        <td>
                            <%# ((model.order.OrderStatus)Convert.ToInt32(Eval("Status").ToString())).ToString() %>
                        </td>
                        <td>
                            <a href="OrderView.aspx?id=<%# Eval("OrderId") %>">查看订单</a>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Panel runat="server" ID="panelNoData" Visible="false">
                <tr class="i">
                    <td colspan="7" style="text-align:center;">对不起，您当前没有订单。
                    </td>
                </tr>
            </asp:Panel>
        </tbody>
    </table>
</asp:Content>

