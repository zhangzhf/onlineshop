﻿<%@ page language="C#" autoeventwireup="true" inherits="space_AddToCartConfirm, PortalWeb" enableviewstate="false" theme="Default" %>

<%@ Register Src="~/Common/PageHeader.ascx" TagPrefix="uc1" TagName="PageHeader" %>
<%@ Register Src="~/Common/PageFooter.ascx" TagPrefix="uc1" TagName="PageFooter" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>商品已成功加入购物车</title>

    <link href="../css/layout.css" rel="stylesheet" />

    <script type="text/javascript">
        var globalSetting = {
            base: '<%=Common.Utils.ApplicationBase() %>'
        };
    </script>
    <script src="../js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="../js/Common.js"></script>
    <script type="text/javascript">

        $(document).ready(function ()
        {
            InitPopMenu(false);
        });

    </script>

</head>
<body>
    <uc1:PageHeader runat="server" ID="PageHeader" />

    <div class="cartConfirm">
        <div class="cartConfirmInfo">
            <div class="success">
                <div class="success-b">
                    <h3>商品已成功加入购物车！</h3>
                </div>

                <span id="initCart_next_go">
                    <a id="GotoShoppingCart" href="./PreCheck.aspx" class="btn-pay">去结算</a>
                    <a href="../Index.aspx" class="btn-continue">继续购物</a>
                </span>
            </div>
        </div>

        <div class="right-extra">
            <div class="m" id="mycart-detail">
                <div class="mt">
                    <h2><s></s>我的购物车</h2>
                </div>

                <div class="mc" id="cart_content">

                    <h3>您物车中的商品</h3>

                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <dl class="old">
                                <dt class="p-img"><a href="../Item.aspx?id=<%# Eval("ItemId") %>" target="_blank">
                                    <img src="<%# Common.Utils.ResolveUrl(model.res.FileInfoImpl.GetPictureThumb(model.res.FileInfoImpl.GetModelByKey(Convert.ToInt32(Eval("SnapshotPicId").ToString())), model.res.ThumbLevel.S)) %>" alt="" /></a></dt>
                                <dd class="p-info">
                                    <div class="p-name"><a href="../Item.aspx?id=<%# Eval("ItemId") %>" target="_blank"><span style="color: red"></span><%# Eval("ItemName") %></a></div>
                                    <div class="p-price"><span style="font-weight: bold; color: red"><%# Eval("MarketPrice") %></span><em>× <%# Eval("ItemCnt") %></em></div>
                                </dd>
                            </dl>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="total">
                        共<strong id="skuCount"><asp:Literal ID="ltTotalCount" runat="server"></asp:Literal></strong>件商品<br />
                        金额总计：<strong><asp:Literal ID="ltTotalPrice" runat="server"></asp:Literal></strong>
                    </div>
                    <div class="btns">
                        <a href="./PreCheck.aspx" class="btn-pay">去结算</a>
                    </div>
                </div>
                <!--my-cart end-->
            </div>

        </div>



    </div>

    <uc1:PageFooter runat="server" ID="PageFooter" />
</body>
</html>
