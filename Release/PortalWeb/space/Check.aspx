﻿<%@ page language="C#" autoeventwireup="true" inherits="space_Check, PortalWeb" enableviewstate="false" theme="Default" %>

<%@ Register Src="~/Common/TopHeader.ascx" TagPrefix="uc1" TagName="TopHeader" %>
<%@ Register Src="~/Common/PageFooter.ascx" TagPrefix="uc1" TagName="PageFooter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>订单结算页</title>
    <link href="../css/layout.css" rel="stylesheet" />
    <link href="../css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

    <script type="text/javascript">
        var globalSetting = {
            base: '<%=Common.Utils.ApplicationBase() %>'
        };
    </script>
    <script src="../js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="../js/jQuery/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>


    <script src="../js/Common.js"></script>
    <script type="text/javascript">

        $(document).ready(function ()
        {
            InitPopMenu(false);
        });

        function edit_Consignee()
        {
            document.getElementById("dialogContent").src = "../sysadmin/AddressSelect.aspx?callback=select_address";

            $("#dialog").dialog({ title: '选择收货人', resizable: false, width: 600, height: 400, modal: true });
            $('#dialog').dialog('open');
        }

        function address_callback(ret)
        {
            $("#address_name").html(ret.Recipient);
            $("#address_cellphone").html(ret.PhoneNum);
            $("#address_email").html(ret.Email);
            $("#address_address").html(ret.Address);
            $("#<%=txtAddressId.ClientID%>").val(ret.AddressId);
        }

        function gotoHome()
        {
            window.location.href = globalSetting.base;
        }
    </script>
</head>
<body>
    <uc1:TopHeader runat="server" ID="TopHeader" />

    <form id="form1" runat="server">

        <div class="logo">
            <a href="javascript:gotoHome();">
                <div class="logoPic"></div>
            </a>

            <ul class="stepWizard step2">
                <li class="over"><b></b>1.我的购物车</li>
                <li class="over"><b></b>2.填写核对订单信息</li>
                <li class="none"><b></b>3.成功提交订单</li>
            </ul>
        </div>

        <div id="checkout" class="checkOrderInfo">
            <div class="mt">
                <h2>填写并核对订单信息</h2>
            </div>
            <div id="wizard" class="checkout-steps">
                <div id="step-1" class="step step-complete">
                    <div class="step-title">
                        <div id="save-consignee-tip" class="step-right">
                        </div>
                        <strong id="consigneeTitleDiv">收货人信息</strong>
                        <span class="step-action" id="consignee_edit_action"><a href="javascript:edit_Consignee()">[修改]</a></span>
                    </div>
                    <div class="step-content">
                        <div id="consignee" class="sbox-wrap">
                            <div class="sbox">
                                <div class="s-content">
                                    <p>
                                        <span id="address_name"></span>&nbsp; 
                                    <span id="address_cellphone"></span>&nbsp; 
                                    <span id="address_email"></span>
                                        <br />
                                        <span id="address_address"></span>
                                        <asp:TextBox ID="txtAddressId" runat="server" Style="display: none" Text=""></asp:TextBox>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--@end div#consignee-->
                    </div>
                </div>
                <div id="step-2" class="step step-complete">
                    <a name="payAndShipFocus"></a>
                    <div class="step-title">
                        <div id="save-payAndShip-tip" class="step-right">
                        </div>
                        <strong>支付及配送方式</strong>
                        <span class="step-action" id="payment-ship_edit_action"><a href="#none" onclick="edit_Payment(false)">[修改]</a></span>
                    </div>
                    <div class="step-content">
                        <div id="payment-ship" class="sbox-wrap">
                            <div class="sbox">
                                <div class="s-content payment-info">
                                    <div class="payment-selected">
                                        支票支付
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="step-3" class="step step-complete">
                    <a name="invoiceFocus"></a>
                    <div class="step-title">
                        <div id="save-invoice-tip" class="step-right">
                        </div>
                        <strong>发票信息</strong>
                        <span class="step-action" id="part-invoice_edit_action"><a href="#none" onclick="edit_Invoice()">[修改]</a></span>
                    </div>
                    <div class="step-content">
                        <div id="part-invoice" class="sbox-wrap">
                            <div class="sbox">
                                <div class="invoice">
                                    <div class="invoice-content">
                                        普通发票（电子）&nbsp;
                                        个人&nbsp;
                                        明细
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="step-4" class="step step-complete">
                    <div class="step-title"><a href="./PreCheck.aspx" id="cartRetureUrl" class="return-edit">返回修改购物车</a><strong>商品清单</strong></div>
                    <div class="step-content">
                        <div id="part-order" class="sbox-wrap">
                            <div class="sbox">
                                <div id="order-cart">
                                    <div class="order-review">
                                        <!--商品清单展示-->
                                        <span id="span-skulist">
                                            <table class="review-thead">
                                                <tbody>
                                                    <tr>
                                                        <td class="fore1">商品</td>
                                                        <td class="fore2">单价</td>
                                                        <td class="fore3">数量</td>
                                                        <td class="fore4">库存状态</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!--**********商品清单内容列表开始************-->
                                            <div class="review-body">

                                                <asp:Repeater ID="Repeater1" runat="server">
                                                    <ItemTemplate>
                                                        <!---单品开始--->
                                                        <!-- 此处置空是必须的  -->
                                                        <div class="review-tbody">
                                                            <table class="order-table">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="fore1">
                                                                            <div class="p-goods">
                                                                                <div class="p-img">
                                                                                    <a href="../Item.aspx?id=<%#Eval("ItemId") %>" target="_blank">
                                                                                        <img alt="" src="<%# Common.Utils.ResolveUrl(model.res.FileInfoImpl.GetPictureThumb(model.res.FileInfoImpl.GetModelByKey(Convert.ToInt32(Eval("SnapshotPicId").ToString())), model.res.ThumbLevel.S)) %>"></a>
                                                                                </div>
                                                                                <div class="p-detail">
                                                                                    <div class="p-name">
                                                                                        <a href="../Item.aspx?id=<%#Eval("ItemId") %>" target="_blank"><%# Eval("DisplayTitle") %></a>
                                                                                    </div>
                                                                                    <div class="p-more">
                                                                                        商品编号：<%# Eval("ItemId") %><br />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="fore2 p-price"><strong>￥<%#Eval("RetailPrice") %></strong>
                                                                        </td>
                                                                        <td class="fore3">x <%#Eval("ItemCnt") %></td>
                                                                        <td class="fore4 p-inventory">有货</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!---单品结束--->
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <!--**********商品清单内容列表结束************-->
                                        </span>
                                        <div class="order-summary">
                                            <div class="statistic fr">
                                                <div class="list">
                                                    <span><em id="span-skuNum">
                                                        <asp:Literal ID="ltTotalCount" runat="server"></asp:Literal></em> 件商品，总商品金额：</span><em class="price" id="warePriceId">￥<asp:Literal ID="ltTotalPrice3" runat="server"></asp:Literal></em>
                                                </div>
                                                <div class="list"><span>应付总额：</span><em id="sumPayPriceId" class="price"> ￥<asp:Literal ID="ltTotalPrice2" runat="server"></asp:Literal></em></div>
                                            </div>
                                        </div>
                                        <!--@end div.order-summary-->
                                    </div>
                                </div>
                                <!--@end div#order-cart-->
                            </div>
                        </div>
                        <!--@end div#part-order-->
                        <div id="checkout-floatbar" class="checkout-buttons group">
                            <div class="sticky-placeholder" style="display: block;">
                                <div class="sticky-wrap">
                                    <div class="inner">
                                        <asp:Button ID="btnSubmit" runat="server" Text="" CssClass="checkout-submit" OnClick="btnSubmit_Click" />
                                        <span class="total">应付总额：<strong id="payPriceId">￥<asp:Literal ID="ltTotalPrice1" runat="server"></asp:Literal></strong>元 
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <uc1:PageFooter runat="server" ID="PageFooter" />

    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src="###"></iframe>
    </div>

</body>

</html>
