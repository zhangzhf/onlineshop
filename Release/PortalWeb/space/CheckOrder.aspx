﻿<%@ page language="C#" autoeventwireup="true" inherits="space_CheckOrder, PortalWeb" enableviewstate="false" theme="Default" %>

<%@ Register Src="~/Common/PageHeader.ascx" TagPrefix="uc1" TagName="PageHeader" %>
<%@ Register Src="~/Common/PageFooter.ascx" TagPrefix="uc1" TagName="PageFooter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>已成功下单</title>

    <link href="../css/layout.css" rel="stylesheet" />

    <script type="text/javascript">
        var globalSetting = {
            base: '<%=Common.Utils.ApplicationBase() %>'
        };
    </script>
    <script src="../js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>

    <script src="../js/Common.js"></script>
    <script type="text/javascript">

        $(document).ready(function ()
        {
            InitPopMenu(false);
        });

    </script>
</head>
<body>
    <uc1:PageHeader runat="server" ID="PageHeader" />

    <div class="cartConfirm">
        <div class="cartConfirmInfo" style="width: 100%;">
            <div class="success">
                <div class="success-b">
                    <h3>您已成功下单</h3>
                </div>
            </div>
            <div style="clear: both; margin-left: 200px;">
                <a href="./OrderView.aspx?id=<%=orderId %>">查看订单</a>
                &nbsp;&nbsp;&nbsp;
                <a href="../Index.aspx">继续购物</a>
            </div>
        </div>

    </div>

    <uc1:PageFooter runat="server" ID="PageFooter" />
</body>
</html>
