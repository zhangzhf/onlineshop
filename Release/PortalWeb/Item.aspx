﻿<%@ page language="C#" autoeventwireup="true" inherits="Item, PortalWeb" enableviewstate="false" theme="Default" %>

<%@ Register Src="Common/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="Common/PageFooter.ascx" TagName="PageFooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="css/layout.css" rel="stylesheet" />
    <link href="css/control.css" rel="stylesheet" />
    <link href="css/gallery.css" rel="stylesheet" />

    <script type="text/javascript">
        var globalSetting = {
            base: '<%=Common.Utils.ApplicationBase() %>'
        };

    </script>

    <script src="js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="js/gallery.js"></script>
    <script src="js/Common.js"></script>
    <script type="text/javascript">

        $(document).ready(function ()
        {
            InitPopMenu(false);
            tabs("divTabs");

            $("#btnAddtoCart").click(function ()
            {
                // 加入购物车
                var url = "./space/Add2Cart.aspx?itemId=<%=item.ItemId%>&count=" + $("#pCnt").val();
                window.location.href = url;
            });

            $("#btndes").click(function ()
            {
                SetNum(-1);
            });

            $("#btnplug").click(function ()
            {
                SetNum(1);
            });
        });

        function SetNum(delta)
        {
            var n = parseInt($("#pCnt").val());
            n = n + delta;
            if (n < 1)
            {
                n = 1;
            }
            $("#pCnt").val(n);
        }

    </script>
</head>
<body>
    <uc1:PageHeader ID="PageHeader1" runat="server" />

    <div class="breadcrumb">
        <h2>
            <asp:Literal ID="ltParent" runat="server"></asp:Literal></h2>
        &gt;
        <h4>
            <asp:Literal ID="ltBoard" runat="server"></asp:Literal></h4>
        &gt;
        <h4>
            <asp:Literal ID="ltCurrent" runat="server"></asp:Literal></h4>
    </div>

    <div class="pageItem">
        <div class="preview">

            <div id="gallery" class="pic">
                <div id="slides">
                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                        <ItemTemplate>
                            <div class="slide">
                                <asp:Image ID="Image1" runat="server" AlternateText="side" Width="350" Height="350" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <div id="menu">
                    <ul>
                        <asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
                            <ItemTemplate>
                                <li class="menuItem"><a href="###">
                                    <asp:Image ID="Image1" runat="server" AlternateText="thumbnail" />
                                </a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>

            <div class="info">

                <div class="dtitle">
                    <asp:Literal ID="ltDisplayTitle" runat="server"></asp:Literal>
                </div>
                <div class="stitle">
                    <asp:Literal ID="ltSubTitle" runat="server"></asp:Literal>
                </div>
                <hr />
                <table class="info_b">
                    <tr>
                        <td>商品编号：</td>
                        <td>
                            <asp:Literal ID="ltItemNo2" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>市&nbsp;场&nbsp;价：</td>
                        <td class="mprice">￥<asp:Literal ID="ltMarketPrice" runat="server"></asp:Literal>元</td>
                    </tr>
                    <tr>
                        <td>商&nbsp;城&nbsp;价：</td>
                        <td class="rprice">￥<asp:Literal ID="ltRetailPrice" runat="server"></asp:Literal>元</td>
                    </tr>

                    <tr>
                        <td>库存状态：</td>
                        <td>有货</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>购买数量：</td>
                        <td class="num">
                            <div class="des" id="btndes"></div>
                            <div class="in">
                                <input type="text" class="text" value="1" id="pCnt" />
                            </div>
                            <div class="plug" id="btnplug"></div>
                        </td>
                    </tr>
                </table>
                <div class="buttonBar">
                    <div id="btnAddtoCart" class="AddtoCart" />
                </div>
            </div>
        </div>

        <div class="info_d">
            <div class="divTabs">
                <ul>
                    <li refer="tab1" class="selected">
                        <div>商品介绍</div>
                    </li>
                    <li refer="tab2">
                        <div>规格参数</div>
                    </li>
                    <li refer="tab3">
                        <div>包装清单</div>
                    </li>
                    <li refer="tab4">
                        <div>售后保障</div>
                    </li>
                </ul>
            </div>
            <div class="tabs tab1">
                <table class="tableInfo">
                    <tr>
                        <td>产品名称：</td>
                        <td>
                            <asp:Literal ID="ltItemName" runat="server"></asp:Literal>
                        </td>
                        <td>商品编号：</td>
                        <td>
                            <asp:Literal ID="ltItemId" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>品牌：</td>
                        <td>
                            <asp:Literal ID="ltBrandName" runat="server"></asp:Literal>
                        </td>
                        <td>型号：</td>
                        <td>
                            <asp:Literal ID="ltItemModel" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>

                <div>
                    <asp:Literal ID="ltContent" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="tabs tab2">

                <asp:Table ID="ItemBasicInfo" runat="server" CssClass="tableInfo">
                </asp:Table>

                <asp:Table ID="ItemSpecInfo" runat="server" CssClass="tableInfo">
                </asp:Table>
            </div>
            <div class="tabs tab3">
                <asp:Table ID="ItemPackage" runat="server" CssClass="tableInfo">
                </asp:Table>
            </div>
            <div class="tabs tab4">
                <asp:Table ID="ItemServices" runat="server" CssClass="tableInfo">
                </asp:Table>
            </div>
        </div>

    </div>

    <uc2:PageFooter ID="PageFooter1" runat="server" />
</body>
</html>
