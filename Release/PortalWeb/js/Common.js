﻿function CommonUploadFile(title, callback, bizModule, bizKey)
{
    document.getElementById("dialogContent").src = "../Common/UploadFile.aspx?callback=" + callback + "&BizModule=" + bizModule + "&BizKey=" + bizKey;

    $("#dialog").dialog({ title: title, resizable: false, width: 650, height: 300, modal: true });
    $('#dialog').dialog('open');
}

function InitPopMenu(pin)
{
    $.ajax({
        type: "GET",
        url: globalSetting.base + "/api/NavMenu/GetMenuItem",
        dataType: "json",
        data: {},
        success: function (data)
        {
            if (pin)
            {
                BuildProductNav(data);
            }

            if (!pin)
            {
                $("#popNavMenu").hide();

                $("#showPopMenu").mouseenter(function ()
                {
                    $("#popNavMenu").show();
                });

                $("#popNavMenu").mouseleave(function ()
                {
                    $("#popNavMenu").hide();
                });
            }
            else
            {
                $("#popNavMenu").show();
            }

            BuildPopMenu(data);

        },
        error: function (data)
        {
            alert("对不起，服务器端出现错误，请稍候再试。");
        }
    });

    $("#showPopMenu").click(function ()
    {
        window.location.href = globalSetting.base;
    });
}
/* 根据返回数据构建弹出菜单 */
function BuildPopMenu(data)
{
    $.each(data, function (index, item)
    {
        var $ul = $("<ul></ul>");
        $("#popNavMenu div.container").append($ul);

        var $li1 = $("<li>" + item.MenuName + "</li>");
        $ul.append($li1);

        var $li2 = $("<li></li>");
        var $ul2 = $("<ul></ul>");

        $ul.append($li2);
        $li2.append($ul2);

        $.each(item.subMenu, function (index2, subitem)
        {
            var $a = $('<li><a href="' + globalSetting.base + '/Board.aspx?id=' + subitem.MenuId + '">' + subitem.MenuName + '</a></li>');
            $ul2.append($a);
        });
    });
}

function BuildProductNav(data)
{
    $container = $('div#productsList ul.floor');

    $.each(data, function (index, item)
    {
        /* build the menu and title */
        var $li = $('<li class="category"></li>');
        $container.append($li);

        $title = $('<div class="title"><div class="f">' + (index + 1) + 'F</div><div class="name"><h2>' + item.MenuName + '</h2></div><div class="more"><h3>更多>></h3></div></div>');

        $li.append($title);

        /* start to build the sub menu items in left */

        var $content = $('<div class="content"></div>');
        var $subMenu = $('<div class="subMenu"></div>');
        var $subUl = $('<ul class="c"></ul>');

        $li.append($content);
        $content.append($subMenu);
        $subMenu.append($subUl);

        var cIds = "";

        $.each(item.subMenu, function (index2, subItem)
        {
            var $a = $('<li><a href="./Board.aspx?id=' + subItem.MenuId + '">' + subItem.MenuName + '</a></li>');
            $subUl.append($a);
            cIds += subItem.MenuId + ',';
        });

        /* start build the product item list in right */

        var $product = $('<div class="products"></div>');
        var $p = $('<ul class="p"></ul>');
        $content.append($product);
        $product.append($p);

        if (cIds != '')
        {
            BuildProductList(cIds, $p);
        }
    });
}

function BuildProductList(ids, pContainer)
{
    $.ajax({
        type: "GET",
        url: globalSetting.base + "/api/NavMenu/GetProductItems",
        dataType: "json",
        data: {
            ids: ids,
            top: '5'
        },
        success: function (data)
        {
            $.each(data, function (index, item)
            {
                $i = $('<li><div class="thumbc"><a href="./Item.aspx?id=' + item.ItemId + '"><img src="' + item.url + '" class="thumb" /></a></div><div class="info"><span class="pname"><a href="./Item.aspx?id=' + item.ItemId + '">' + item.DisplayTitle + '</a></span><span class="price">￥' + item.RetailPrice + '</span></div></li>');
                pContainer.append($i);
            });
        },
        error: function (data)
        {
            alert("对不起，服务器端出现错误，请稍候再试。");
        }
    });
}


/***  多个tab标签的样式 ***/
function tabs(className)
{
    var tabs = $("." + className + " li");

    for (var i = 0; i < tabs.length; i++)
    {

        $(tabs[i]).click(function ()
        {

            for (var j = 0; j < tabs.length; j++)
            {
                handleShowHide(tabs[j], false);
            }

            handleShowHide(this, true);
        });
    }

    $(tabs[0]).click();
}

function handleShowHide(tab, show)
{
    var refer = $(tab).attr("refer");
    if (show)
    {
        $("div." + refer).show();
        $(tab).addClass("selected");
    }
    else
    {
        $("div." + refer).hide();
        $(tab).removeClass("selected");
    }
}