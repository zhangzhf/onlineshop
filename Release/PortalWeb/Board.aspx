﻿<%@ page language="C#" autoeventwireup="true" inherits="Board, PortalWeb" enableviewstate="false" theme="Default" %>

<%@ Register Src="Common/PageHeader.ascx" TagName="PageHeader" TagPrefix="uc1" %>
<%@ Register Src="Common/PageFooter.ascx" TagName="PageFooter" TagPrefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="css/layout.css" rel="stylesheet" />
    <link href="css/control.css" rel="stylesheet" />

    <script type="text/javascript">
        var globalSetting = {
            base: '<%=Common.Utils.ApplicationBase() %>'
        };

    </script>

    <script src="./js/jQuery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="js/Common.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            InitPopMenu(false);
        });

    </script>
</head>
<body>

    <uc1:PageHeader ID="PageHeader1" runat="server" />

    <div class="breadcrumb">
        <h2>
            <asp:Literal ID="ltParent" runat="server"></asp:Literal></h2>
        &gt;
        <h4><asp:Literal ID="ltCurrent" runat="server"></asp:Literal></h4>
    </div>

    <div class="page_list">
        <div class="left">
            <ul class="subMenu">
                <asp:Repeater ID="Repeater2" runat="server">
                    <ItemTemplate>
                        <li><a href="Board.aspx?id=<%# Eval("CategoryId") %>"><%# Eval("CategoryName") %></a></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <div class="right">
            <ul>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <li>
                            <div class="thumbc">
                                <a href="Item.aspx?id=<%# Eval("ItemId") %>">
                                    <img width="220" height="220" src="<%# Common.Utils.ResolveUrl(model.res.FileInfoImpl.GetPictureThumb(model.res.FileInfoImpl.GetModelByKey(Convert.ToInt32(Eval("SnapshotPicId").ToString())), model.res.ThumbLevel.M)) %>" class="thumb" />
                                </a>
                            </div>
                            <div class="info">
                                <div class="pname"><a href="Item.aspx?id=<%# Eval("ItemId") %>"><%# Eval("DisplayTitle") %></a></div>
                                <div class="act">
                                    <div class="price">￥<%# Eval("RetailPrice") %></div>
                                    <div class="pbutton"><a class="btn">加入购物车</a> </div>
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>

    <uc2:PageFooter ID="PageFooter1" runat="server" />

</body>
</html>
