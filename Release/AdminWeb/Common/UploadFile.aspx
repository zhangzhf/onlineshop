﻿<%@ page title="" language="C#" masterpagefile="~/master/PopupPage.master" autoeventwireup="true" inherits="Common_UploadFile, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ToolBar" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Table ID="Table1" runat="server" CssClass="tableInfo">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>上传文件信息</asp:TableHeaderCell>
            <asp:TableHeaderCell></asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell>文件标题：</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                选择文件：
            </asp:TableCell>
            <asp:TableCell>
                <asp:FileUpload ID="FileUpload1" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                说明：
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtMemo" runat="server" TextMode="MultiLine"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div class="divFooter">
        <asp:Button ID="btnSubmit" runat="server" Text="提交" CssClass="blue" OnClick="btnSubmit_Click" />
        <input id="btnCancel" type="button" value="取消" />
        <br />
    </div>
    <asp:Label ID="txtMsg" runat="server" Text=""></asp:Label>
</asp:Content>

