﻿<%@ page title="" language="C#" masterpagefile="~/master/MasterPage.master" autoeventwireup="true" inherits="Member_OrgInfo, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <script type="text/javascript">

        $(document).ready(function () {

            $("#btnEdit").click(function () {

                document.getElementById("dialogContent").src = "./OrgEdit.aspx?OrgId=<%= strOrgId %>";

                $("#dialog").dialog({ title: '编辑单位', resizable: false, width: 600, height: 300, modal: true });
                $('#dialog').dialog('open');

            });

        });

        function CloseDialog() {
            $('#dialog').dialog('close');
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" Runat="Server">
    查看单位详细信息
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" Runat="Server">
    <input id="btnEdit" type="button" value="编辑单位" />
    <asp:Button ID="btnDelete" runat="server" Text="删除单位" OnClientClick="javascipt:return confirm('确认删除当前单位？\r\n\r\n注意：单位删除后将不会再显示在系统里。但该单位相关的数据将会保留。')" OnClick="btnDelete_Click" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src=""></iframe>
    </div>
</asp:Content>

