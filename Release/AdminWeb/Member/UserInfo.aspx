﻿<%@ page title="" language="C#" masterpagefile="~/master/MasterPage.master" autoeventwireup="true" inherits="Member_UserInfo, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">

        $(document).ready(function () {

            $("#btnEdit").click(function () {

                document.getElementById("dialogContent").src = "./UserEdit.aspx?UserId=<%= strUserId %>";

                $("#dialog").dialog({ title: '编辑用户', resizable: false, width: 600, height: 600, modal: true });
                $('#dialog').dialog('open');

            });

        });

        function CloseDialog() {
            $('#dialog').dialog('close');
        }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
    查看用户详细信息
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
    <input id="btnEdit" type="button" value="编辑用户" />
    <asp:Button ID="btnDelete" runat="server" Text="删除用户" OnClientClick="javascipt:return confirm('确认删除当前用户？\r\n\r\n注意：用户删除后将不会再登录系统。但该用户相关的数据将会保留。')" OnClick="btnDelete_Click" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src=""></iframe>
    </div>
</asp:Content>

