﻿<%@ page title="" language="C#" masterpagefile="~/master/MasterPage.master" autoeventwireup="true" inherits="Member_OrgAdmin, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {

            $("#btnAddOrg").click(function () {

                document.getElementById("dialogContent").src = "./OrgEdit.aspx";

                $("#dialog").dialog({ title: '添加单位', resizable: false, width: 600, height: 300, modal: true });
                $('#dialog').dialog('open');

            });

        });

        function CloseDialog() {
            $('#dialog').dialog('close');
        }

        function EditOrg(orgId) {
            document.getElementById("dialogContent").src = "./OrgEdit.aspx?OrgId=" + orgId;

            $("#dialog").dialog({ title: '编辑单位', resizable: false, width: 600, height: 300, modal: true });
            $('#dialog').dialog('open');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
    用户单位管理
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
    <input id="btnAddOrg" type="button" value="添加单位" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField HeaderText="单位序号" DataField="OrgId" />

            <asp:HyperLinkField HeaderText="单位名称" DataTextField="OrgName" DataNavigateUrlFields="OrgId" DataNavigateUrlFormatString="./OrgInfo.aspx?OrgId={0}" />

            <asp:BoundField HeaderText="地址" DataField="Address" />
            <asp:BoundField HeaderText="邮编" DataField="ZIP" />

            <asp:BoundField HeaderText="创建时间" DataField="Created" DataFormatString="{0:yyy-MM-dd HH:mm}" />
            <asp:TemplateField HeaderText="当前状态">
                <ItemTemplate>
                    <%# EnumUtils.GetDescription((model.Status)Convert.ToInt32( Eval("Status").ToString()) )%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="操作">
                <ItemTemplate>
                    <asp:HyperLink ID="hyEditOrg" runat="server">编辑</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src=""></iframe>
    </div>
</asp:Content>

