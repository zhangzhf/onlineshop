﻿<%@ page title="" language="C#" masterpagefile="~/master/PopupPage.master" autoeventwireup="true" inherits="Member_UserEdit, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            $("#btnCancel").click(function () {
                parent.CloseDialog();
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ToolBar" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table class="tableInfo">
        <thead>
            <tr>
                <th colspan="2">请填写用户信息
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>帐号：
                </td>
                <td>
                    <asp:TextBox ID="txtUserAccount" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="请输入帐号"
                        ControlToValidate="txtUserAccount" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>密码：
                </td>
                <td>
                    <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="请输入密码"
                        ControlToValidate="txtPassword1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>确认密码：
                </td>
                <td>
                    <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password"></asp:TextBox>
                    <span class="MustInput">*</span><asp:CompareValidator ID="CompareValidator1" runat="server"
                        ControlToValidate="txtPassword1" ControlToCompare="txtPassword2" ErrorMessage="两次密码输入不一致"
                        SetFocusOnError="true"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>用户显示昵称：
                </td>
                <td>
                    <asp:TextBox ID="txtNickName" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请输入昵称"
                        ControlToValidate="txtNickName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td>用户真实姓名：
                </td>
                <td>
                    <asp:TextBox ID="txtRealName" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="请输入姓名"
                        ControlToValidate="txtRealName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>所在单位：
                </td>
                <td>
                    <asp:DropDownList ID="ddpOrgInfo" runat="server" DataTextField="OrgName" DataValueField="OrgId">
                    </asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td>手机：
                </td>
                <td>
                    <asp:TextBox ID="txtCellPhone" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>座机电话：
                </td>
                <td>
                    <asp:TextBox ID="txtLandlinePhone" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>传真：
                </td>
                <td>
                    <asp:TextBox ID="txtFax" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>邮箱：
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>QQ号：
                </td>
                <td>
                    <asp:TextBox ID="txtQQ" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>微信号：
                </td>
                <td>
                    <asp:TextBox ID="txtWeChat" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>地址：
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>邮编：
                </td>
                <td>
                    <asp:TextBox ID="txtZIP" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>备注：
                </td>
                <td>
                    <asp:TextBox ID="txtMemo" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>用户状态：
                </td>
                <td>
                    <asp:CheckBox ID="chkStatus" runat="server" Checked="true" />
                    启用
                </td>
            </tr>
        </tbody>
    </table>
    <div class="divFooter">
        <asp:Button ID="btnSubmit" runat="server" Text="保存" OnClick="btnSubmit_Click" />
        <input id="btnCancel" type="button" value="取消" />
        <br />
    </div>
    <asp:Label ID="txtMsg" runat="server" Text=""></asp:Label>
</asp:Content>
