﻿<%@ page title="" language="C#" masterpagefile="~/master/PopupPage.master" autoeventwireup="true" inherits="Goods_ItemEditPriceAdd, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(document).ready(function ()
        {
            $("#btnCancel").click(function ()
            {
                parent.CloseDialog();
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ToolBar" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table class="tableInfo">
        <thead>
            <tr>
                <th colspan="2">请填写产品价格信息
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>市场价：
                </td>
                <td>
                    <asp:TextBox ID="txtPriceMarket" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="请输入产品市场价格"
                        ControlToValidate="txtPriceMarket" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>零售价：
                </td>
                <td>
                    <asp:TextBox ID="txtPriceRetail" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="请输入产品销售价格"
                        ControlToValidate="txtPriceRetail" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>团购价：
                </td>
                <td>
                    <asp:TextBox ID="txtPriceGroupOn" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请输入产品团购价格"
                        ControlToValidate="txtPriceGroupOn" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="divFooter">
        <asp:Button ID="btnSubmit" runat="server" Text="保存" OnClick="btnSubmit_Click" />
        <input id="btnCancel" type="button" value="取消" />
        <br />
    </div>
    <asp:Label ID="txtMsg" runat="server" Text=""></asp:Label>
</asp:Content>

