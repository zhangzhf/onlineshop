﻿<%@ page title="" language="C#" masterpagefile="~/master/PopupPage.master" autoeventwireup="true" inherits="Goods_CategoryEdit, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            $("#btnCancel").click(function () {
                parent.CloseDialog();
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ToolBar" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table class="tableInfo">
        <thead>
            <tr>
                <th colspan="2">请填写产品分类信息
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>产品分类名称：
                </td>
                <td>
                    <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="请输入产品分类名称"
                        ControlToValidate="txtCategoryName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>父分类：
                </td>
                <td>
                    <asp:DropDownList ID="ddpParentCate" runat="server" DataTextField="CategoryName" DataValueField="CategoryId"
                        AutoPostBack="true" OnSelectedIndexChanged="ddpParentCate_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>顺序：
                </td>
                <td>
                    <asp:TextBox ID="txtOrderId" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="请输入显示顺序"
                        ControlToValidate="txtOrderId" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>状态：
                </td>
                <td>
                    <asp:CheckBox ID="chkStatus" runat="server" Checked="true" />
                    启用
                </td>
            </tr>
        </tbody>
    </table>
    <div class="divFooter">
        <asp:Button ID="btnSubmit" runat="server" Text="保存" OnClick="btnSubmit_Click" />
        <input id="btnCancel" type="button" value="取消" />
        <br />
    </div>
    <asp:Label ID="txtMsg" runat="server" Text=""></asp:Label>
</asp:Content>

