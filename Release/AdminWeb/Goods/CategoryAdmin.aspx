﻿<%@ page title="" language="C#" masterpagefile="~/master/MasterPage.master" autoeventwireup="true" inherits="Goods_CategoryAdmin, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {

            $("#btnAddCate").click(function () {

                document.getElementById("dialogContent").src = "./CategoryEdit.aspx";

                $("#dialog").dialog({ title: '添加产品分类', resizable: false, width: 600, height: 340, modal: true });
                $('#dialog').dialog('open');

            });
        });

        function CloseDialog() {
            $('#dialog').dialog('close');
        }

        function EditCate(cateId) {
            document.getElementById("dialogContent").src = "./CategoryEdit.aspx?CategoryId=" + cateId;

            $("#dialog").dialog({ title: '编辑产品分类', resizable: false, width: 600, height: 340, modal: true });
            $('#dialog').dialog('open');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
    商品分类管理
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
    <input id="btnAddCate" type="button" value="添加产品分类" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound">
        <Columns>
            <asp:BoundField HeaderText="顺序" DataField="OrderId" />
            <asp:HyperLinkField HeaderText="分类名称" DataTextField="CategoryName" DataNavigateUrlFields="CategoryId" DataNavigateUrlFormatString="./CategoryInfo.aspx?CategoryId={0}" />

            <asp:BoundField HeaderText="创建时间" DataField="Created" DataFormatString="{0:yyy-MM-dd HH:mm}" />
            <asp:TemplateField HeaderText="当前状态">
                <ItemTemplate>
                    <%# EnumUtils.GetDescription((model.Status)Convert.ToInt32( Eval("Status").ToString()) )%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="操作">
                <ItemTemplate>
                    <asp:HyperLink ID="hyEditCate" runat="server">编辑</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src="#"></iframe>
    </div>
</asp:Content>

