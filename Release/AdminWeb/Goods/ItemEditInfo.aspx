﻿<%@ page title="" language="C#" masterpagefile="~/master/MasterPage.master" autoeventwireup="true" inherits="Goods_ItemEdit, AdminWeb" validaterequest="false" theme="_Default" %>

<%@ Register Src="../Common/InnerTabs.ascx" TagName="InnerTabs" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function ()
        {
            var editor = CKEDITOR.replace('<%=txtDescript.ClientID%>');
            CKFinder.setupCKEditor(editor, '../ckfinder/');

            $(".write").hide();

            $("#btnEdit").click(function ()
            {
                $(".read").hide();
                $(".write").show();
                $("#<%=txtDescript.ClientID%>").hide();
            });

            $("#btnCancel").click(function ()
            {
                $(".write").hide();
                $(".read").show();
                $("#<%=txtDescript.ClientID%>").hide();
            });
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
    商品基本信息编辑
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <uc1:InnerTabs ID="InnerTabs1" runat="server" />

    <table class="tableInfo width-p100">
        <tbody>
            <tr>
                <th>商品基本信息</th>
                <th align="right">
                    <input type="button" id="btnEdit" value="编辑" class="read" />

                    <asp:Button ID="btnSubmit" runat="server" Text="提交" CssClass="write" OnClick="btnSubmit_Click" />
                    <input type="button" id="btnCancel" value="取消" class="write" />
                </th>
            </tr>
            <tr>
                <td>商品名称</td>
                <td>
                    <asp:Label ID="lblItemName" runat="server" Text="" CssClass="read"></asp:Label>
                    <asp:TextBox ID="txtItemName" runat="server" CssClass="write"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>显示标题</td>
                <td>
                    <asp:Label ID="lblDisplayTitle" runat="server" Text="" CssClass="read"></asp:Label>
                    <asp:TextBox ID="txtDisplayTitle" runat="server" CssClass="write"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>子标题</td>
                <td>
                    <asp:Label ID="lblSubTitle" runat="server" Text="" CssClass="read"></asp:Label>
                    <asp:TextBox ID="txtSubTitle" runat="server" CssClass="write"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>厂家</td>
                <td>
                    <asp:Label ID="lblMFR" runat="server" Text="" CssClass="read"></asp:Label>
                    <asp:TextBox ID="txtMFR" runat="server" CssClass="write"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>品牌</td>
                <td>
                    <asp:Label ID="lblBrandName" runat="server" Text="" CssClass="read"></asp:Label>
                    <asp:TextBox ID="txtBrandName" runat="server" CssClass="write"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>规格/型号</td>
                <td>
                    <asp:Label ID="lblModel" runat="server" Text="" CssClass="read"></asp:Label>
                    <asp:TextBox ID="txtModel" runat="server" CssClass="write"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>市场价格</td>
                <td>
                    <asp:Label ID="lblMarketPrice" runat="server" Text="" CssClass="read"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>零售价格</td>
                <td>
                    <asp:Label ID="lblRetailPrice" runat="server" Text="" CssClass="read"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>团购价格</td>
                <td>
                    <asp:Label ID="lblGroupOnPrice" runat="server" Text="" CssClass="read"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>状态
                </td>
                <td>
                    <asp:Label ID="lblStatus" runat="server" Text="" CssClass="read"></asp:Label>
                    <asp:CheckBox ID="chkStatus" runat="server" CssClass="write" />
                    <asp:Label ID="lblStatusText" runat="server" Text="启用" CssClass="write"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>备注
                </td>
                <td>
                    <asp:Label ID="lblRMK" runat="server" Text="" CssClass="read"></asp:Label>
                    <asp:TextBox ID="txtRMK" runat="server" CssClass="write width-300"></asp:TextBox>
                </td>
            </tr>
            <tr class="richedit">
                <td>产品描述
                </td>
                <td>
                    <asp:Label ID="lblDescript" runat="server" Text="" CssClass="read"></asp:Label>
                    <div class="write">
                        <asp:TextBox ID="txtDescript" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</asp:Content>

