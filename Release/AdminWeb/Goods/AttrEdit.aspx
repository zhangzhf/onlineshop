﻿<%@ page title="" language="C#" masterpagefile="~/master/PopupPage.master" autoeventwireup="true" inherits="Goods_AttrEdit, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(document).ready(function ()
        {
            $("#btnCancel").click(function ()
            {
                parent.CloseDialog();
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ToolBar" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table class="tableInfo">
        <thead>
            <tr>
                <th colspan="2">请填写属性信息
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>属性信息编号：
                </td>
                <td>
                    <asp:TextBox ID="txtInfoKey" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="请输入属性编号"
                        ControlToValidate="txtInfoKey" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>属性信息名称：
                </td>
                <td>
                    <asp:TextBox ID="txtKeyName" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="请输入属性名称"
                        ControlToValidate="txtKeyName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>顺序：
                </td>
                <td>
                    <asp:TextBox ID="txtOrderId" runat="server"></asp:TextBox>
                    <span class="MustInput">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="请输入顺序"
                        ControlToValidate="txtOrderId" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>状态：
                </td>
                <td>
                    <asp:CheckBox ID="chkStatus" runat="server" Checked="true" />
                    启用
                </td>
            </tr>
        </tbody>
    </table>
    <div class="divFooter">
        <asp:Button ID="btnSubmit" runat="server" Text="保存" OnClick="btnSubmit_Click" />
        <input id="btnCancel" type="button" value="取消" />
        <br />
    </div>
    <asp:Label ID="txtMsg" runat="server" Text=""></asp:Label>

</asp:Content>

