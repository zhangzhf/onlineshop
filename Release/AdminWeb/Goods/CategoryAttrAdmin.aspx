﻿<%@ page title="" language="C#" masterpagefile="~/master/MasterPage.master" autoeventwireup="true" inherits="Goods_CategoryAttrAdmin, AdminWeb" theme="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script language="javascript" type="text/javascript">

        $(document).ready(function ()
        {
            $("#btnAddAttr").click(function ()
            {

                var type = $("#<%=ddpInfoType.ClientID%> option:selected").val();

                document.getElementById("dialogContent").src = "./AttrEdit.aspx?categoryId=<%=strCategoryId%>&infoType=" + type;

                $("#dialog").dialog({ title: '添加属性', resizable: false, width: 600, height: 300, modal: true });
                $('#dialog').dialog('open');
            });
        });

        function CloseDialog()
        {
            $('#dialog').dialog('close');
        }

        function EditAttr(p1, p2, p3)
        {
            document.getElementById("dialogContent").src = "./AttrEdit.aspx?categoryId=" + p1 + "&infoType=" + p2 + "&infoKey=" + p3;

            $("#dialog").dialog({ title: '编辑属性', resizable: false, width: 600, height: 300, modal: true });
            $('#dialog').dialog('open');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CurrentPosition" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SearchBar" runat="Server">
    信息属性分类：
    <asp:DropDownList ID="ddpInfoType" runat="server" AutoPostBack="true"
        OnSelectedIndexChanged="ddpInfoType_SelectedIndexChanged">
    </asp:DropDownList>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ToolBar" runat="Server">
    <input id="btnAddAttr" type="button" value="添加属性" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand">
        <Columns>
            <asp:BoundField HeaderText="分类编号" DataField="InfoKey" />
            <asp:BoundField HeaderText="分类名称" DataField="KeyName" />
            <asp:BoundField HeaderText="显示顺序" DataField="OrderId" />
            <asp:TemplateField HeaderText="状态">
                <ItemTemplate>
                    <%# EnumUtils.GetDescription((model.Status)Convert.ToInt32( Eval("Status").ToString()) )%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="操作">
                <ItemTemplate>
                    <asp:HyperLink ID="hyEdit" runat="server">编辑</asp:HyperLink>
                    <asp:LinkButton ID="btnDelete" runat="server" OnClientClick="javascript:return confirm('确认删除所选属性？')">删除</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <div id="dialog" style="display: none;">
        <iframe frameborder="0" id="dialogContent" src="#"></iframe>
    </div>
</asp:Content>

