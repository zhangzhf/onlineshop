﻿jQuery(function ($)
{
    $.datepicker.regional['zh-CN'] = {
        closeText: '关闭',
        prevText: '&#x3c;上月',
        nextText: '下月&#x3e;',
        currentText: '今天',
        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
		'七月', '八月', '九月', '十月', '十一月', '十二月'],
        monthNamesShort: ['一', '二', '三', '四', '五', '六',
		'七', '八', '九', '十', '十一', '十二'],
        dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
        weekHeader: '周',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: true,
        changeYear: true,
        yearRange: "2014:2049",
        showButtonPanel: true
    };
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
});

function CommonUploadFile(title, callback, bizModule, bizKey)
{
    document.getElementById("dialogContent").src = "../Common/UploadFile.aspx?callback=" + callback + "&BizModule=" + bizModule + "&BizKey=" + bizKey;

    $("#dialog").dialog({ title: title, resizable: false, width: 650, height: 300, modal: true });
    $('#dialog').dialog('open');
}
